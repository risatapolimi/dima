package com.risata.parkshare.patter_observer.wear_messages;

import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.risata.parkshare.Model.Car;
import com.risata.parkshare.Model.ParkingSlot;
import com.risata.parkshare.Model.Position;
import com.risata.parkshare.R;
import com.risata.parkshare.presenter.WearPresenter;

import org.json.JSONObject;

public class SignalParkMessage implements MyMessage {


    private boolean isPayant;
    private boolean isRestricted;

    public SignalParkMessage(boolean isPayant, boolean isRestricted) {
        this.isPayant = isPayant;
        this.isRestricted = isRestricted;
    }

    @Override
    public void perform(final WearPresenter wearPresenter) {
        String userID = wearPresenter.getMiniModel().getUserId();
        Position userPosition = wearPresenter.getMiniModel().getUserPosition();
        Car inUseCar = wearPresenter.getMiniModel().getInUseCar();

        ParkingSlot parkingSlot = new ParkingSlot(userPosition,isPayant,isRestricted,1);
        if(userPosition!=null) {
            wearPresenter.getMiniConnectionHandler().postParkingSlot(userID, inUseCar, parkingSlot, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(wearPresenter.getMapsActivity(),
                            wearPresenter.getMapsActivity().getString(R.string.parking_added),
                            Toast.LENGTH_LONG).show();
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(wearPresenter.getMapsActivity(),
                            wearPresenter.getMapsActivity().getString(R.string.parking_added_error),
                            Toast.LENGTH_LONG).show();
                }
            });
        }


    }
}
