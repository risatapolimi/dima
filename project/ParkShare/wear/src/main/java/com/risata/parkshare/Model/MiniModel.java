package com.risata.parkshare.Model;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

public class MiniModel {
    private String userId;
    private Ranking ranking;
    private List<Car> carList;
    private Position userPosition;
    private List<ParkingSlot> parkingSlotList;


    public MiniModel(String userId) {
        this.userId = userId;
        this.parkingSlotList = new ArrayList<>();
        this.carList = new ArrayList<>();
        this.ranking = new Ranking();
    }

    public Position getUserPosition() {
        return userPosition;
    }

    public void setUserPosition(Position userPosition) {
        this.userPosition = userPosition;
    }

    public List<ParkingSlot> getParkingSlotList() {
        return parkingSlotList;
    }

    public String getUserId() {
        return userId;
    }

    public Ranking getRanking() {
        return ranking;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public Car getInUseCar(){
        for(Car c: carList){
            if (c.isFavorite())
                return c;
        }
        return null;
    }
}
