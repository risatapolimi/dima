package com.risata.parkshare.patter_observer;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class ActivityObservable<T> extends Activity {

    private List<MyObserver<T>> observers;

    public ActivityObservable(){
        observers=new ArrayList<>();
    }

    public void registerObserver(MyObserver myObserver){
        this.observers.add(myObserver);
    }

    public void removeObserver(MyObserver myObserver){
        this.observers.remove(myObserver);
    }

    public void notifyObserver(T message){
        for(MyObserver<T> M:observers)
            M.update(message);
    }

    public List<MyObserver<T>> getObservers() {
        return observers;
    }

    public void setObservers(List<MyObserver<T>> observers) {
        this.observers = observers;
    }

}
