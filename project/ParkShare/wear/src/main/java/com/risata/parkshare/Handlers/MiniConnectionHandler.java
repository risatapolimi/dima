package com.risata.parkshare.Handlers;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.risata.parkshare.Model.Car;
import com.risata.parkshare.Model.ParkingSlot;
import com.risata.parkshare.Model.Position;
import com.risata.parkshare.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MiniConnectionHandler {

    private String userId;
    private String tokenAuth;
    private  RequestQueue requestQueue;
    private Context context;
    private String baseUrl;

    public MiniConnectionHandler(String userId, String tokenAuth, Context context) {
        this.userId = userId;
        this.tokenAuth = tokenAuth;
        this.requestQueue = Volley.newRequestQueue(context);
        this.context = context;
        this.baseUrl = context.getString(R.string.url_server);
    }

    public  void getRanking(String userId, Response.Listener<JSONObject> jsonResponse,Response.ErrorListener jsonError){
        String url = context.getString(R.string.url_server) +"entities.ranking/"+userId;
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                url, null, jsonResponse,jsonError);

        requestQueue.add(req);
    }

    public void getCarList(String userId,Response.Listener<JSONArray> jsonListener, Response.ErrorListener jsonError ){
        String url = baseUrl+"entities.car/all/"+userId;
        JsonArrayRequest req = new JsonArrayRequest(url,jsonListener,jsonError);

        requestQueue.add(req);

    }

    public static void login(Context context, String email, String password, Response.Listener<JSONObject> jsonResponse, Response.ErrorListener jsonError){
        String url = context.getString(R.string.url_server) +"entities.user/login/"+email+"/"+password;
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                url, null, jsonResponse,jsonError);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(req);

    }


    public void getParkingSlots(Location location,Response.Listener<JSONArray> jsonListener){
        String url = baseUrl+"entities.parkingslot/list/"+ location.getLatitude()+"/"+location.getLongitude();
        JsonArrayRequest req = new JsonArrayRequest(url,jsonListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,
                        context.getString(R.string.download_parking_slot_error),
                        Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> headers = new HashMap<String,String>();
                headers.put("Authorization",tokenAuth);
                return headers;
            }
        };
        // Adding request to request queue
        requestQueue.add(req);
    }

    public void postParkingSlot(String userId, Car inUseCar, ParkingSlot parkingSlot,Response.Listener jsonResponse, Response.ErrorListener jsonError){
        String url = baseUrl +"entities.parkingslot";
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id", 44);
            jsonBody.put("width", inUseCar.getWidth());
            jsonBody.put("length", inUseCar.getLength());
            jsonBody.put("ispayant", parkingSlot.isPayant());
            jsonBody.put("isrestricted", parkingSlot.isRestricted());
            jsonBody.put("isavailable", true);
            jsonBody.put("address","not_defined"); // indirizzo da wear
            jsonBody.put("latitude",parkingSlot.getPosition().getLatitude());
            jsonBody.put("longitude",parkingSlot.getPosition().getLongitude());
            jsonBody.put("advicedDimension",1);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String requestBody = jsonBody.toString();


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,url,jsonBody,jsonResponse,jsonError){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> headers = new HashMap<String,String>();
                headers.put("Authorization",tokenAuth);
                return headers;
            }/*
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
                public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }*/
        };
        requestQueue.add(req);


    }

}
