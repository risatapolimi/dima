package com.risata.parkshare;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.wear.ambient.AmbientMode;
import android.support.wearable.view.ConfirmationOverlay;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.google.android.wearable.intent.RemoteIntent;
import com.risata.parkshare.Handlers.MiniConnectionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Set;

import static android.support.constraint.Constraints.TAG;

public class LoginActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        CapabilityApi.CapabilityListener {

    private static final String CAPABILITY_PHONE_APP = "parkShareMobileApplication";


    private Node mAndroidPhoneNodeWithApp;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleApiClient mGoogleApiClient;
    private SignInButton signInButton;

    private static final int RC_SIGN_IN = 9000;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

         mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        this.signInButton = findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                @SuppressLint("RestrictedApi") Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });


    }

    private void startMapsActivity(){
        Intent intent = new Intent(this,MapsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if ((mGoogleApiClient != null) && mGoogleApiClient.isConnected()) {
            Wearable.CapabilityApi.removeCapabilityListener(
                    mGoogleApiClient,
                    this,
                    CAPABILITY_PHONE_APP);

            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...)
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult signInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (signInResult.isSuccess()) {
                GoogleSignInAccount account = signInResult.getSignInAccount();

                // Get account information
                String id = account.getId();
                id = id.substring(0, Math.min(id.length(), 17)); //TODO Tronco altrimenti bad request dal server, idee?
                String email =  "goo_" + account.getEmail();
                String surname = account.getFamilyName();
                String name = account.getGivenName();

                GoogleLogin gl = new GoogleLogin(id, name, surname, email);
                gl.execute((Void) null);
            }
        }
    }


    public class GoogleLogin extends AsyncTask<Void, Void, Boolean>{

        private String id;
        private String name;
        private String surname;
        private String email;

        public GoogleLogin(String id, String name, String surname, String email) {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.email = email;
        }

        //executed once the connection to facebook has been performed in order to access the app's server
        @Override
        protected Boolean doInBackground(Void... voids) {
            registerLoginSocial(name, surname, email, id);
            return null;
        }
    }


    public void registerLoginSocial(String name, String surname, String email, String id){

        MiniConnectionHandler.login(this,email,id, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("TAG", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    String token = response.getString("token");
                    String userID = response.getString("userid");

                    Bundle bundle = new Bundle();
                    bundle.putString("userId",userID);
                    bundle.putString("token",token);
                    Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(getApplicationContext(),
                            getString(R.string.connection_error),
                            Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
            }
        });


        }


    //In teoria tutto da togliere qua sotto





    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Set up listeners for capability changes (install/uninstall of remote app).
        Wearable.CapabilityApi.addCapabilityListener(
                mGoogleApiClient,
                this,
                CAPABILITY_PHONE_APP);

        //checkIfPhoneHasApp();
    }

    private void checkIfPhoneHasApp() {
        PendingResult<CapabilityApi.GetCapabilityResult> pendingResult =
                Wearable.CapabilityApi.getCapability(
                        mGoogleApiClient,
                        CAPABILITY_PHONE_APP,
                        CapabilityApi.FILTER_REACHABLE);

        pendingResult.setResultCallback(new ResultCallback<CapabilityApi.GetCapabilityResult>() {
            @Override
            public void onResult(@NonNull CapabilityApi.GetCapabilityResult getCapabilityResult) {
                if (getCapabilityResult.getStatus().isSuccess()) {
                    CapabilityInfo capabilityInfo = getCapabilityResult.getCapability();
                    mAndroidPhoneNodeWithApp = pickBestNodeId(capabilityInfo.getNodes());
                    Log.d("capabilityInfo",capabilityInfo.getNodes()+"");
                    verifyNodeAndUpdateUI();
                } else {
                }
            }
        });
    }

    private void verifyNodeAndUpdateUI() {
        if (mAndroidPhoneNodeWithApp != null) {
            //loginButton.setVisibility(View.VISIBLE);
            //textView.setText("ohoh, ma hai la nostra app! Che bravo bambino");
            Log.d("capabilityYES", ""+ mAndroidPhoneNodeWithApp);

        } else {
            //loginButton.setVisibility(View.INVISIBLE);
            //textView.setText("Sembra che tu non abbia la nostra app, cattivooo");
            Log.d("capabilityNO", ""+mAndroidPhoneNodeWithApp);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mAndroidPhoneNodeWithApp = null;
        verifyNodeAndUpdateUI();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mAndroidPhoneNodeWithApp = null;
        verifyNodeAndUpdateUI();
    }

    /*
     * Updates UI when capabilities change (install/uninstall phone app).
     */

    public void onCapabilityChanged(CapabilityInfo capabilityInfo) {
        mAndroidPhoneNodeWithApp = pickBestNodeId(capabilityInfo.getNodes());
        Log.d("capability", "onCapabilityChanged(): " + capabilityInfo);

        verifyNodeAndUpdateUI();
    }

    private Node pickBestNodeId(Set<Node> nodes) {
        Node bestNodeId = null;
        // Find a nearby node/phone or pick one arbitrarily. Realistically, there is only one phone.
        for (Node node : nodes) {
            if(node.isNearby())
                bestNodeId = node;
        }
        return bestNodeId;
    }


    private final ResultReceiver mResultReceiver = new ResultReceiver(new Handler()) {
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultCode == RemoteIntent.RESULT_OK) {
                new ConfirmationOverlay().showOn(LoginActivity.this);

            } else if (resultCode == RemoteIntent.RESULT_FAILED) {
                new ConfirmationOverlay()
                        .setType(ConfirmationOverlay.FAILURE_ANIMATION)
                        .showOn(LoginActivity.this);

            } else {
                throw new IllegalStateException("Unexpected result " + resultCode);
            }
        }
    };
}
