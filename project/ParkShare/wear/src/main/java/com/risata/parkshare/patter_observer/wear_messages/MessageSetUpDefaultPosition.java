package com.risata.parkshare.patter_observer.wear_messages;

import com.risata.parkshare.presenter.WearPresenter;

public class MessageSetUpDefaultPosition implements MyMessage {
    @Override
    public void perform(WearPresenter wearPresenter) {
        wearPresenter.getMiniLocationHandler().setDefaultPosition();
        }
    }
