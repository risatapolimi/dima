package com.risata.parkshare.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.risata.parkshare.Handlers.MiniConnectionHandler;
import com.risata.parkshare.Handlers.MiniLocationHandler;
import com.risata.parkshare.MapsActivity;
import com.risata.parkshare.Model.Car;
import com.risata.parkshare.Model.MiniModel;
import com.risata.parkshare.Model.ParkingSlot;
import com.risata.parkshare.Model.Position;
import com.risata.parkshare.Model.Ranking;
import com.risata.parkshare.R;
import com.risata.parkshare.patter_observer.MyObserver;
import com.risata.parkshare.patter_observer.wear_messages.MyMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WearPresenter implements MyObserver<MyMessage>{

    private MiniLocationHandler miniLocationHandler;
    private MiniConnectionHandler miniConnectionHandler;
    private MiniModel miniModel;
    private MapsActivity mapsActivity;

    public WearPresenter(MapsActivity activity, String userId, String token) {
        this.miniConnectionHandler = new MiniConnectionHandler(userId,token,activity);
        this.miniModel = new MiniModel(userId);

        miniLocationHandler = new MiniLocationHandler((LocationManager) activity.getSystemService(Context.LOCATION_SERVICE),activity, miniModel);
        miniLocationHandler.registerObserver(this);
        miniLocationHandler.checkPermissions();
        this.mapsActivity = activity;


        miniConnectionHandler.getRanking(userId,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("TAG", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    int level = response.getInt("level");
                    int exp = response.getInt("exp");

                    miniModel.getRanking().setExp(exp);
                    miniModel.getRanking().setLevel(level);

                } catch (JSONException e) {
                    Toast.makeText(mapsActivity,
                            mapsActivity.getString(R.string.download_ranking_error), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mapsActivity,
                        mapsActivity.getString(R.string.download_ranking_error), Toast.LENGTH_SHORT).show();
            }
        });


        //GetCarList
        miniConnectionHandler.getCarList(userId,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                List<Car> carList = miniModel.getCarList();
                try {
                    // Parsing json array response
                    // loop through each json object
                    for (int i = 0; i < response.length(); i++) {

                        JSONObject carJson = (JSONObject) response
                                .get(i);
                        String manufacturer = carJson.getString("manufacturer");
                        String model = carJson.getString("model");
                        double width = carJson.getDouble("width");
                        double length = carJson.getDouble("length");
                        boolean isFavourite = carJson.getBoolean("isfavourite");
                        String category = carJson.getString("category");
                        String carID = carJson.getString("carid");

                        carList.add(new Car(manufacturer,model,width,length,isFavourite,category,carID));
                        Log.d("cars",carList.toString());

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mapsActivity,mapsActivity.getString(R.string.download_cars_error)
                            ,
                            Toast.LENGTH_LONG).show();
                }
                Car inUseCar = getMiniModel().getInUseCar();
                mapsActivity.getMenu().findItem(R.id.car_name_menu).setTitle(mapsActivity.getString(R.string.your_selected_car)+"\n"+inUseCar.getManufacturer()+" "+inUseCar.getModel()+"\n");


            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mapsActivity,
                        mapsActivity.getString(R.string.download_cars_error),
                        Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void update(MyMessage message) {
        message.perform(this);
    }


    public MiniLocationHandler getMiniLocationHandler() {
        return miniLocationHandler;
    }

    public MiniModel getMiniModel() {
        return miniModel;
    }

    public MapsActivity getMapsActivity() {
        return mapsActivity;
    }

    public MiniConnectionHandler getMiniConnectionHandler() {
        return miniConnectionHandler;
    }
}
