package com.risata.parkshare.patter_observer.wear_messages;

import android.location.Location;

import com.risata.parkshare.Handlers.MiniLocationHandler;
import com.risata.parkshare.presenter.WearPresenter;

public class MessageCheckLocationPermission implements MyMessage {


        @Override
        public void perform(WearPresenter wearPresenter) {
            MiniLocationHandler miniLocationHandler = wearPresenter.getMiniLocationHandler();
            if(miniLocationHandler.checkPermissions()){
                Location position = wearPresenter.getMiniModel().getUserPosition();
                miniLocationHandler.setUpHandler();
                if (position != null){
                    wearPresenter.getMapsActivity().setUpMapReady(position);
                }

            }

        }

    }

