package com.risata.parkshare.patter_observer.wear_messages;

import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.risata.parkshare.MapsActivity;
import com.risata.parkshare.Model.MiniModel;
import com.risata.parkshare.Model.ParkingSlot;
import com.risata.parkshare.Model.Position;
import com.risata.parkshare.Model.Ranking;
import com.risata.parkshare.R;
import com.risata.parkshare.presenter.WearPresenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UpdateParking implements MyMessage {

    protected Location loc;

    public UpdateParking(Location loc) {
        this.loc = loc;
    }

    @Override
    public void perform(final WearPresenter wearPresenter) {
        wearPresenter.getMiniConnectionHandler().getParkingSlots(loc,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("banana", response.toString());
                        MiniModel miniModel = wearPresenter.getMiniModel();
                        List<ParkingSlot> parkingSlotListModel = miniModel.getParkingSlotList();
                        List<ParkingSlot> parkingSlotListReceived = new ArrayList<>();
                        try {
                            // Parsing json array response
                            // loop through each json object
                            for (int i = 0; i < response.length(); i++) {

                                JSONObject parkingSlot = (JSONObject) response
                                        .get(i);
                                int id = parkingSlot.getInt("id");
                                boolean isRestricted = parkingSlot.getBoolean("isrestricted");
                                boolean isPayant = parkingSlot.getBoolean("ispayant");
                                double latitude = parkingSlot.getDouble("latitude");
                                double longitude = parkingSlot.getDouble("longitude");
                                Location loc = new Location("");
                                loc.setLatitude(latitude);
                                loc.setLongitude(longitude);

                                parkingSlotListReceived.add(new ParkingSlot(new Position(loc),isPayant,isRestricted,id));

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(wearPresenter.getMapsActivity(),
                                    wearPresenter.getMapsActivity().getString(R.string.download_parking_slot_error),
                                    Toast.LENGTH_LONG).show();
                        }


                        List<ParkingSlot> listToBeDeleted = new ArrayList<>();
                        if(parkingSlotListModel!=null) {
                            for(ParkingSlot pCheck: parkingSlotListModel){
                                if(!parkingSlotListReceived.contains(pCheck)){
                                    listToBeDeleted.add(pCheck);
                                }
                            }
                            //rimuove dalla lista nel modello i parcheggi diversi da quelli ricevuti
                            parkingSlotListModel.removeAll(listToBeDeleted);

                            parkingSlotListReceived.removeAll(parkingSlotListModel);

                            wearPresenter.getMiniModel().getParkingSlotList().addAll(parkingSlotListReceived);

                        }

                        Ranking ranking = miniModel.getRanking();
                        if(ranking==null)
                            ranking = new Ranking("pippo",1,1);

                        setUpParking(loc, miniModel.getParkingSlotList(), wearPresenter.getMapsActivity(),
                                listToBeDeleted, ranking);



                }});

    }

    public void setUpParking(Location loc, List<ParkingSlot> parkingSlotList, MapsActivity mapsActivity, List<ParkingSlot> listToBeDeleted, Ranking ranking){

        if (loc != null && mapsActivity.getmMap() != null) {
            mapsActivity.removeMarkerParking(listToBeDeleted);
            mapsActivity.drawRankingCircle(loc,ranking);

            double distance = 0;
            Position userPosition = new Position(loc);

            for(ParkingSlot p: parkingSlotList){
                distance = p.getPosition().differenceKm(userPosition);
                boolean visible = p.isInternalRanking(userPosition, ranking);
                mapsActivity.markParking(p,visible);

            }


        }

    }

}
