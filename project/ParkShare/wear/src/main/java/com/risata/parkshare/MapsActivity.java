package com.risata.parkshare;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.risata.parkshare.Model.ParkingSlot;
import com.risata.parkshare.Model.Ranking;
import com.risata.parkshare.patter_observer.ActivityObservable;
import com.risata.parkshare.patter_observer.wear_messages.MessageCheckLocationPermission;
import com.risata.parkshare.patter_observer.wear_messages.MessageSetUpDefaultPosition;
import com.risata.parkshare.patter_observer.wear_messages.MyMessage;
import com.risata.parkshare.patter_observer.wear_messages.SignalParkMessage;
import com.risata.parkshare.patter_observer.wear_messages.StartLocationUpdateMessage;
import com.risata.parkshare.patter_observer.wear_messages.StopLocationUpdateMessage;
import com.risata.parkshare.presenter.WearPresenter;
//import com.risata.parkshare.wearable_service.WearService;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.wear.widget.drawer.WearableActionDrawerView;
import android.support.wearable.view.DismissOverlayView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowInsets;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MapsActivity extends ActivityObservable<MyMessage> implements OnMapReadyCallback,
        GoogleMap.OnMapLongClickListener, MenuItem.OnMenuItemClickListener, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnMyLocationButtonClickListener {

    public static final int MY_PERMISSIONS_REQUEST_READ_FINE_LOCATION = 100;
    public static final int REQUEST_CODE = 42;

    private Menu menu;

    private boolean isLookingAround;
    private List<Marker> markerList;
    private Bitmap bitmap,bitmapHandy;
    private Circle circle;


    private WearableActionDrawerView actionDrawerView;
    /**
     * Overlay that shows a short help text when first launched. It also provides an option to
     * exit the app.
     */
    private DismissOverlayView mDismissOverlay;
    private GoogleMap mMap;

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);

        Bundle bundle = getIntent().getExtras();
        String userid=bundle.getString("userId");
        String token=bundle.getString("token");

        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.ic_parking_marker);
        bitmap = this.getBitmapFromVectorDrawable((VectorDrawable) drawable);
        Drawable drawableHandy = res.getDrawable(R.drawable.ic_parking_marker_handy);
        bitmapHandy = this.getBitmapFromVectorDrawable((VectorDrawable) drawableHandy);

        isLookingAround = false;
        markerList = new ArrayList<>();
        setContentView(R.layout.activity_maps);

        //Bottom drawer
        actionDrawerView =
                (WearableActionDrawerView) findViewById(R.id.bottom_action_drawer);
        //Alza il menù
        actionDrawerView.getController().peekDrawer();
        actionDrawerView.setOnMenuItemClickListener(this);

        this.menu = actionDrawerView.getMenu();

        final FrameLayout topFrameLayout = (FrameLayout) findViewById(R.id.root_container);
        final FrameLayout mapFrameLayout = (FrameLayout) findViewById(R.id.map_container);

        // Set the system view insets on the containers when they become available.
        topFrameLayout.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
            @Override
            public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
                // Call through to super implementation and apply insets
                insets = topFrameLayout.onApplyWindowInsets(insets);

                FrameLayout.LayoutParams params =
                        (FrameLayout.LayoutParams) mapFrameLayout.getLayoutParams();

                // Add Wearable insets to FrameLayout container holding map as margins
                params.setMargins(
                        insets.getSystemWindowInsetLeft(),
                        insets.getSystemWindowInsetTop(),
                        insets.getSystemWindowInsetRight(),
                        insets.getSystemWindowInsetBottom());
                mapFrameLayout.setLayoutParams(params);

                return insets;
            }
        });

        // Obtain the DismissOverlayView and display the introductory help text.
        mDismissOverlay = (DismissOverlayView) findViewById(R.id.dismiss_overlay);
        mDismissOverlay.setIntroText(R.string.intro_text);
        mDismissOverlay.showIntroIfNecessary();

         //Obtain the MapFragment and set the async listener to be notified when the map is ready.
        MapFragment mapFragment =
                (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        WearPresenter wearPresenter = new WearPresenter(this, userid, token);
        registerObserver(wearPresenter);

    }



    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Map is ready to be used.
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnMyLocationButtonClickListener(this);


        // Set the long click listener as a way to exit the map.
        mMap.setOnMapLongClickListener(this);

        notifyObserver(new MessageCheckLocationPermission());
    }

    public void setUpMapReady(Location userLocation){
        focusMap(new LatLng(userLocation.getLatitude(), userLocation.getLongitude()));


    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        // Display the dismiss overlay with a button to exit this activity.
        mDismissOverlay.show();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    notifyObserver(new MessageCheckLocationPermission());
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Just","to be sure");
                        mMap.setMyLocationEnabled(true);
                        notifyObserver(new MessageSetUpDefaultPosition());
                    }

                } else {

                }
                return;
            }
        }
    }





    public void focusMap(LatLng location) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 17), 30, null);
    }

    @Override
    public void onCameraMoveStarted(int i) {
        if(i==1){
            isLookingAround=true;
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        isLookingAround=false;
        return false;
    }

    public boolean isLookingAround() {
        return isLookingAround;
    }

    public GoogleMap getmMap() {
        return mMap;
    }


    public void removeMarkerParking(List<ParkingSlot> listToBeDeleted){
        for (Iterator<Marker> iterator = markerList.iterator(); iterator.hasNext();) {
            Marker marker = iterator.next();
            ParkingSlot p = (ParkingSlot) marker.getTag();
            if (listToBeDeleted.contains(p)) {
                marker.remove();
                iterator.remove();
            }
        }
    }

    public void drawRankingCircle(Location userLocation, Ranking userRanking) {
        LatLng pos = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
        if (circle != null) {
            circle.remove();
        }
        circle = this.mMap.addCircle(new CircleOptions()
                .center(pos)
                .radius(userRanking.getLevel() * 150)
                .strokeColor(Color.YELLOW) //circonferenza
                .fillColor(Color.argb(50, 246, 252, 48)));
    }

    public void markParking(ParkingSlot ps, boolean visible) {
        Marker markerAlreadyPresent = this.checkAlreadyPresent(markerList.iterator(), ps);
        if (markerAlreadyPresent == null) {
            String stringTitle;
            if(ps.isPayant()){
                stringTitle = getString(R.string.toll_parking);
            }else{
                stringTitle = getString(R.string.toll_free);
            }

            Marker marker = this.mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(ps.getPosition().getLatitude(), ps.getPosition().getLongitude()))
                    .title(stringTitle)
                    .icon(BitmapDescriptorFactory.fromBitmap(ps.isRestricted() ? bitmapHandy : bitmap)) //cambia icona se per disabili
                    .visible(visible));
            marker.setTag(ps);
            markerList.add(marker);
            markerAlreadyPresent = marker;
        } else {
            markerAlreadyPresent.setVisible(visible);
            markerAlreadyPresent.setTag(ps);
        }
    }

    public Marker checkAlreadyPresent(Iterator<Marker> iter, ParkingSlot p) {
        if (iter.hasNext()) {
            Marker m = iter.next();
            if (p.hasSameLocation(m.getPosition()))
                return m;
            else return this.checkAlreadyPresent(iter, p);
        } else return null;
    }


    private Bitmap getBitmapFromVectorDrawable(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id){
            case R.id.model:
                Intent intent = new Intent(getApplicationContext(), SignalParkActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent,REQUEST_CODE);
                actionDrawerView.getController().peekDrawer();
                break;
        }

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == SignalParkActivity.RESULT_OK) {
                boolean isPayant = data.getBooleanExtra("isPayant",false);
                boolean isRestricted = data.getBooleanExtra("isRestricted",false);
                notifyObserver(new SignalParkMessage(isPayant,isRestricted));

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public Menu getMenu() {
        return menu;
    }

    @Override
    protected void onPause() {
        notifyObserver(new StopLocationUpdateMessage());
        super.onPause();
    }

    @Override
    protected void onResume() {
        notifyObserver(new StartLocationUpdateMessage());
        super.onResume();
    }
}
