package com.risata.parkshare.patter_observer.wear_messages;

import com.risata.parkshare.presenter.WearPresenter;

public interface MyMessage {

    void perform(WearPresenter wearPresenter);

}
