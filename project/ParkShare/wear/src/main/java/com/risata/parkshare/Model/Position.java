package com.risata.parkshare.Model;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.risata.parkshare.patter_observer.wear_messages.MyMessage;
import com.risata.parkshare.presenter.WearPresenter;

public class Position extends Location implements MyMessage{

    public Position(String provider) {
        super(provider);
    }

    public Position(Location l) {
        super(l);
    }

    @Override
    public void perform(WearPresenter wearPresenter) {
        MiniModel model = wearPresenter.getMiniModel();
        model.setUserPosition(this);
        if(!wearPresenter.getMapsActivity().isLookingAround()){
            wearPresenter.getMapsActivity().focusMap(new LatLng(this.getLatitude(),this.getLongitude()));

        }
    }

    public double differenceKm(Position pos2){
        double pigreco = 3.1415927;
        double lat1= pigreco * this.getLatitude() /180;
        double long1=pigreco * this.getLongitude() /180;
        double lat2= pigreco * pos2.getLatitude() /180;
        double long2=pigreco * pos2.getLongitude() /180;
        double deltalat=lat1-lat2;
        double deltalong=long1-long2;
        double distRadianti=Math.asin(
                (
                        Math.sqrt(
                                Math.pow(Math.cos(lat2)*Math.sin(deltalong),2)
                                        + Math.pow(Math.cos(lat1)*Math.sin(lat2)-Math.sin(lat1)*Math.cos(lat2)*Math.cos(deltalong),2)
                        )
                )
                        /
                        (Math.sin(lat1)*Math.sin(lat2)+Math.cos(lat1)*Math.cos(lat2)*Math.cos(deltalong))
        );
        return distRadianti*6371;
    }
}
