package com.risata.parkshare.Model;

import com.google.android.gms.maps.model.LatLng;

import java.text.DecimalFormat;

public class ParkingSlot {

    private Position position;
    private boolean isPayant;
    private boolean isRestricted;
    private int id;

    public ParkingSlot(Position position, boolean isPayant, boolean isRestricted, int id) {
        this.position = position;
        this.isPayant = isPayant;
        this.isRestricted = isRestricted;
        this.id = id;
    }

    public Position getPosition() {
        return position;
    }

    public boolean isPayant() {
        return isPayant;
    }

    public boolean isRestricted() {
        return isRestricted;
    }

    public int getId() {
        return id;
    }

    public boolean isInternalRanking(Position userPosition, Ranking userRanking) {
        double distance=userPosition.differenceKm(this.getPosition());
        return distance<=(userRanking.getLevel()*0.150);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingSlot that = (ParkingSlot) o;
        return id == that.id;
    }

    public boolean hasSameLocation(LatLng val){
        return this.position.getLatitude()==val.latitude && this.position.getLongitude()==val.longitude;
    }



}
