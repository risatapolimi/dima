package com.risata.parkshare.Model;

public class Ranking {

    private int level;
    private int exp;
    private String userID;

    public Ranking(){
        exp=0;
        level=1;
    }

    public Ranking(String userId){
        this();
        this.userID = userId;
    }

    public Ranking(String userId,int lev, int experience){
        this.userID = userId;
        this.level=lev;
        this.exp=experience;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
