package com.risata.parkshare.patter_observer;

public interface MyObserver<T> {

    public void update(T object);

}
