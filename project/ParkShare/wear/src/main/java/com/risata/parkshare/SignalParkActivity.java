package com.risata.parkshare;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

public class SignalParkActivity extends Activity {

    public static final int RESULT_OK = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signal_park);

        final CheckBox checkBoxPayant = findViewById(R.id.payant_park);
        final CheckBox checkBoxRestricted = findViewById(R.id.handicap_park);
        Intent data = new Intent();


        Button confirmButton = findViewById(R.id.button_signal);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isPayant = checkBoxPayant.isChecked();
                boolean isRestricted = checkBoxRestricted.isChecked();

                Intent data = new Intent();
                data.putExtra("isPayant",isPayant);
                data.putExtra("isRestricted",isRestricted);
                setResult(RESULT_OK,data);
                finish();



            }
        });

    }
}
