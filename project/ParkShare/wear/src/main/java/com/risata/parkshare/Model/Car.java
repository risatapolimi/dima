package com.risata.parkshare.Model;

public class Car {

    private String manufacturer;

    private String model;

    private double width;

    private double length;

    private boolean isFavorite;

    private String category;

    private String carId;

    public Car(String manufacturer, String model, double width, double length, boolean isFavorite, String category, String carId) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.width = width;
        this.length = length;
        this.isFavorite = isFavorite;
        this.category = category;
        this.carId = carId;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public String getCategory() {
        return category;
    }

    public String getCarId() {
        return carId;
    }
}
