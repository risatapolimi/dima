package com.risata.parkshare.Handlers;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.risata.parkshare.MapsActivity;
import com.risata.parkshare.Model.MiniModel;
import com.risata.parkshare.Model.Position;
import com.risata.parkshare.patter_observer.MyObservable;
import com.risata.parkshare.patter_observer.wear_messages.MyMessage;
import com.risata.parkshare.patter_observer.wear_messages.UpdateParking;

public class MiniLocationHandler extends MyObservable<MyMessage> {

    private Activity activity;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private LocationManager locationManager;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    protected LocationRequest mLocationRequest;


    private MiniModel miniModel;


    public MiniLocationHandler(LocationManager locationManager, Activity activity, MiniModel miniModel) throws SecurityException {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        this.activity = activity;
        this.locationManager = locationManager;
        this.miniModel = miniModel;
        setUpHandler();
    }

    public void removeUpdate(){
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }


    public void setUpHandler() throws SecurityException {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(200);
        mLocationRequest.setFastestInterval(200);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    Log.d("position","error");
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    // ...
                    Log.d("Positionbanana",location.toString());
                    Position position = new Position(location);
                    miniModel.setUserPosition(new Position(location));
                    notifyObserver(position);
                    notifyObserver(new UpdateParking(location));
                }
            }
        };
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }

    public boolean checkPermissions(){
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission( activity.getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){

                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MapsActivity.MY_PERMISSIONS_REQUEST_READ_FINE_LOCATION);
                Log.d("POSITION","PERMESSI");
                return false;
            }
            return true;
        }



    public void getDeviceLocation(final GoogleMap mMap) {
        try {
            Task<Location> locationResult = mFusedLocationClient.getLastLocation();
            locationResult.addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        Location location = task.getResult();
                        LatLng currentLatLng = new LatLng(location.getLatitude(),
                                location.getLongitude());
                        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(currentLatLng,
                                15);
                        mMap.moveCamera(update);
                    }
                }
            });

        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    public void setDefaultPosition(){
        //Set predefined position
        Location location=new Location("");
        location.setLatitude(45.464180);
        location.setLongitude(9.190006);
        this.notifyObserver(new Position(location));
    }



}
