package com.risata.parkshare.pattern_observer.messages;

import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.widget.PopupMenu;

import com.risata.parkshare.R;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Filter;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.model.User;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.presenters.MapHomePresenter;
import com.risata.parkshare.webservice.Status;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReadyMapHomeFragment implements MyMessage {

    @Override
    public void perform(MainPresenter mainPresenter) {
        Model model = mainPresenter.getModel();
        MapHomePresenter mapHomePresenter = mainPresenter.getMapHomePresenter();
        MapHomeFragment mapHomeFragment = mapHomePresenter.getMapHomeFragment();
        setUpStatusInfo(mapHomeFragment,model.getUser(), model);

        mapHomePresenter.manageFilter(model.getFilter());


    }

    private void setUpStatusInfo( MapHomeFragment mapHomeFragment, User user, Model model){

        Iterator<Car> iterator=user.getCarList().iterator();
        while(iterator.hasNext()){
            Car next=iterator.next();
            if(next.isFavorite()) {
                mapHomeFragment.setCarSelected(next.getManufacturer() + " " + next.getModel());
                mapHomeFragment.getParkingSlotAdapter().setInUseCar(next);
                break;
            }
        }
        mapHomeFragment.setUserLevel(user.getStringRanking());

    }

}
