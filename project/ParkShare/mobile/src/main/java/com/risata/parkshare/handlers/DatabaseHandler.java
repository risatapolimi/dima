package com.risata.parkshare.handlers;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.Transaction;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.model.User;
import com.risata.parkshare.repository.CarDao;
import com.risata.parkshare.repository.MyDatabase;
import com.risata.parkshare.repository.RankingDao;
import com.risata.parkshare.repository.RememberMe;
import com.risata.parkshare.repository.RememberMeDao;
import com.risata.parkshare.repository.UserDao;
import com.risata.parkshare.views.MainActivity_Drawer;
import com.risata.parkshare.webservice.RESTInterface;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Babbo natale on 04/05/2018.
 */

public class DatabaseHandler {

    private UserDao userDao;
    private CarDao carDao;
    private RankingDao rankingDao;
    static private RememberMeDao rememberMeDao;
    private Executor executor;
    private String userId;


    public DatabaseHandler(Context context, Executor executor, String userId) {
        this.userId = userId;
        MyDatabase db = Room.databaseBuilder(context, MyDatabase.class, "parkshare.db").fallbackToDestructiveMigration().build();
        this.userDao = db.userDao();
        this.carDao = db.carDao();
        this.rankingDao = db.rankingDao();
        this.rememberMeDao=db.rememberMeDao();
        this.executor = Executors.newSingleThreadExecutor();
    }


    public void initUserData(User user){
        userDao.insert(user);
        rankingDao.insert(user.getMyRanking());
        for(Car c:user.getCarList())
            carDao.insert(c);
    }


    public User initUser(String userId) {
        ExecutorService es = Executors.newSingleThreadExecutor();
        Future<User> result = es.submit(() -> {
            User user = userDao.load(userId);
            user.setCarList(carDao.loadCars(userId));
            user.setMyRanking(rankingDao.load(userId));
            return user;
        });

        User user = null;
        try {
             user = result.get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return  user;
    }


    public void addNewCar(Car newCar) {
        executor.execute(() ->
            carDao.insert(newCar)
        );
    }

    public  void deleteCar(Car deletingCar){
        executor.execute(() ->
            carDao.deleteCar(deletingCar.getCarId(), userId)
        );
    }

    public void editCar(Car editingCar){
        executor.execute(() ->
                carDao.updateEditedCar(editingCar.getManufacturer(), editingCar.getModel(), editingCar.getLength(), editingCar.getWidth(), editingCar.getCategory(), editingCar.getCarId(), userId)
        );
    }

    public List<Car> getCarList() {

        ExecutorService es = Executors.newSingleThreadExecutor();
        Future<List<Car>> result = es.submit(() -> {
            List<Car> carList = carDao.loadCars(userId);
            return carList;
        });

        List<Car> carList = null;
        try {
            carList = result.get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return  carList;
    }

    public void setInUseCar(Car inUseCar){
        executor.execute(() ->
           updateInUseCar(inUseCar.getCarId(), userId)
        );
    }

     @Transaction
    void updateInUseCar(String carId, String userId){
        carDao.clearInUseCar(userId);
        carDao.setInUseCar(carId, userId);
     }

    @Transaction
    public void clear(){
        executor.execute(()->clearTables());

    }

    public void clearTables(){
        userDao.deleteAllUsers();
        carDao.deleteAllCars();
        rankingDao.deleteAllRankings();
    }

    public void clearRememberme(){
        executor.execute(()->
            rememberMeDao.deleteAll());
    }

    @Transaction
    public void updateRanking(String userId,int level,int exp){
        executor.execute(()->
                rankingDao.update(level, exp, userId));
    }

    @Transaction
    public static void insertRememberMe(RememberMe rememberMe,Context context){
        MyDatabase db = Room.databaseBuilder(context, MyDatabase.class, "parkshare.db").fallbackToDestructiveMigration().build();
        Executor ex=Executors.newSingleThreadExecutor();
        rememberMeDao=db.rememberMeDao();
        ex.execute(()-> rememberMeDao.insert(rememberMe));
    }

    @Transaction
    public static RememberMe getRememberMe(Context context){
        ExecutorService es = Executors.newSingleThreadExecutor();
        MyDatabase db = Room.databaseBuilder(context, MyDatabase.class, "parkshare.db").fallbackToDestructiveMigration().build();
        rememberMeDao=db.rememberMeDao();
        Future<RememberMe> result = es.submit(() -> {
            RememberMe rem = rememberMeDao.load();
            return rem;
        });

        RememberMe rem = null;
        try {
            rem = result.get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return  rem;
    }

    public static void removeRememberme(Context context,String newRemember) {
        MyDatabase db = Room.databaseBuilder(context, MyDatabase.class, "parkshare.db").fallbackToDestructiveMigration().build();
        Executor ex=Executors.newSingleThreadExecutor();
        rememberMeDao=db.rememberMeDao();
        ex.execute(()-> rememberMeDao.deleteRememberme(newRemember));
    }
}



