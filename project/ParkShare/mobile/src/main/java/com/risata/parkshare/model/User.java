package com.risata.parkshare.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Relation;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniele on 08/03/2018.
 */
@Entity
public class User {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("surname")
    @Expose
    private String surname;

    @SerializedName("userid")
    @Expose
    @PrimaryKey
    @NonNull
    private String userId;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("pwdHash")
    @Expose
    private String pwdHash;

    @Ignore
    private Ranking myRanking;

    @Ignore
    private List<Car> carList;


    public User(String userId,String name, String surname, String email, String pwdHash) {
        this.userId=userId;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.pwdHash = pwdHash;
        this.carList = new ArrayList<>();
        //this.userId = email; //Integer.toString(hashCode());
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwdHash() {
        return pwdHash;
    }

    public void setPwdHash(String pwdHash) {
        this.pwdHash = pwdHash;
    }

    public Ranking getMyRanking() {
        return myRanking;
    }

    public String getStringRanking(){
        return myRanking.getValue();
    }

    public void setMyRanking(Ranking myRanking) {
        this.myRanking = myRanking;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList.clear();
        this.carList.addAll(carList);
    }

    public Car getInUseCar(){
        for(Car c: carList){
            if(c.isFavorite()){
                return c;
            }
        }
        return null;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (!getName().equals(user.getName())) return false;
        if (!getSurname().equals(user.getSurname())) return false;
        return getEmail().equals(user.getEmail());
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getSurname().hashCode();
        result = 31 * result + getEmail().hashCode();
        return result;
    }

}
