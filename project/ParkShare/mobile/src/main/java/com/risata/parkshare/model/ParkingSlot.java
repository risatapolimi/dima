package com.risata.parkshare.model;

import com.google.android.gms.maps.model.LatLng;
import com.risata.parkshare.pattern_observer.MyObservable;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.pattern_observer.messages.NotifyUpdatedAddress;

import java.text.DecimalFormat;
import java.util.Objects;

/**
 * Created by Daniele on 08/03/2018.
 */

public class ParkingSlot extends MyObservable<MyMessage>{

    private double width;
    private double length;
    private Position position;
    private boolean isAvailable;
    private boolean isPayant;
    private boolean isRestricted;
    private String address;
    private int id;


    public ParkingSlot(double width, double length, Position position, boolean isAvailable, boolean isPayant, boolean isRestricted) {
        this.width = width;
        this.length = length;
        this.position = position;
        this.isAvailable = isAvailable;
        this.isPayant = isPayant;
        this.isRestricted = isRestricted;
        id=0;
        address="";

    }

    public ParkingSlot(double width, double length, Position position, boolean isAvailable, boolean isPayant, boolean isRestricted, int id) {
        this.width = width;
        this.length = length;
        this.position = position;
        this.isAvailable = isAvailable;
        this.isPayant = isPayant;
        this.isRestricted = isRestricted;
        this.id = id;
        address="";
    }

    public ParkingSlot(double width, double length, Position position, boolean isAvailable, boolean isPayant, boolean isRestricted, int id,String addr) {
        this.width = width;
        this.length = length;
        this.position = position;
        this.isAvailable = isAvailable;
        this.isPayant = isPayant;
        this.isRestricted = isRestricted;
        this.id = id;
        address=addr;
    }


    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isPayant() {
        return isPayant;
    }

    public void setPayant(boolean payant) {
        isPayant = payant;
    }

    public boolean isRestricted() {
        return isRestricted;
    }

    public void setRestricted(boolean restricted) {
        isRestricted = restricted;
    }

    public int getId() {
        return id;
    }

    public boolean isInternalRanking(Position userPosition, Ranking userRanking) {
        double distance=userPosition.differenceKm(this.getPosition());
        return distance<=(userRanking.getLevel()*0.150);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingSlot that = (ParkingSlot) o;
        return /*Double.compare(that.width, width) == 0 &&
                Double.compare(that.length, length) == 0 &&
                isAvailable == that.isAvailable &&
                isPayant == that.isPayant &&
                isRestricted == that.isRestricted &&*/
                id == that.id /*&&
                hasSameLocation(new LatLng(that.getPosition().getLatitude(),that.getPosition().getLongitude()))*/;
    }


    /*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParkingSlot)) return false;

        ParkingSlot that = (ParkingSlot) o;

        if (Double.compare(that.getWidth(), getWidth()) != 0) return false;
        if (Double.compare(that.getLength(), getLength()) != 0) return false;
        if (isAvailable() != that.isAvailable()) return false;
        if (isPayant() != that.isPayant()) return false;
        if (isRestricted() != that.isRestricted()) return false;
        return getPosition().equals(that.getPosition());
    }*/

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getWidth());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLength());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + getPosition().hashCode();
        result = 31 * result + (isAvailable() ? 1 : 0);
        result = 31 * result + (isPayant() ? 1 : 0);
        result = 31 * result + (isRestricted() ? 1 : 0);
        return result;
    }

    public boolean hasSameLocation(LatLng val){
        return this.position.getLatitude()==val.latitude && this.position.getLongitude()==val.longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        this.notifyObserver(new NotifyUpdatedAddress());
    }

    public double getArea() {
        return width*length;
    }

    @Override
    public String toString() {
        return "ParkingSlot{" +
                "width=" + width +
                ", length=" + length +
                ", position=" + position +
                ", isAvailable=" + isAvailable +
                ", isPayant=" + isPayant +
                ", isRestricted=" + isRestricted +
                ", address='" + address + '\'' +
                ", id=" + id +
                '}';
    }
}
