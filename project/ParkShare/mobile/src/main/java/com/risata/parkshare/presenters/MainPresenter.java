package com.risata.parkshare.presenters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.widget.Toast;

import com.risata.parkshare.R;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.fragments.UserProfileFragment;
import com.risata.parkshare.handlers.ConnectionHandler;
import com.risata.parkshare.handlers.DatabaseHandler;
import com.risata.parkshare.handlers.LocationHandler;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.model.Size;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.pattern_observer.MyObserver;

import retrofit2.Callback;

/**
 * Created by Daniele&Giorgio but not sartini since he was working on icon design on 15/03/2018.
 */

public class MainPresenter implements MyObserver<MyMessage> {
    private MapHomePresenter mapHomePresenter;
    private UserProfilePresenter userProfilePresenter;
    private Model model;
    private DatabaseHandler dbHandler;
    private ConnectionHandler connectionHandler;

    public MainPresenter(MapHomeFragment mapHomeFragment, UserProfileFragment userProfileFragment, AppCompatActivity activity, Model model, DatabaseHandler dbHandler, ConnectionHandler connectionHandler, LocationHandler locationHandler) {
        userProfilePresenter=new UserProfilePresenter(userProfileFragment);
        mapHomePresenter=new MapHomePresenter(locationHandler,mapHomeFragment);
        this.model = model;
        this.dbHandler = dbHandler;
        this.connectionHandler = connectionHandler;
        locationHandler.registerObserver(this);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);//activity.getSharedPreferences("com.risata.parkshare_preferences",Context.MODE_PRIVATE);
        PreferenceManager.getDefaultSharedPreferences(activity).registerOnSharedPreferenceChangeListener(model.getFilter());
        model.getFilter().initFilter(sharedPreferences);

    }

    public void update(MyMessage message){
        try {
            message.perform(this);
        }catch(Exception exp){
            Toast.makeText(getMapHomePresenter().getMapHomeFragment().getContext(),R.string.app_explosion,Toast.LENGTH_SHORT).show();
            getMapHomePresenter().restartApplication(model.getUser().getUserId(),connectionHandler.getTokenAuth(),model);
        }
    }

    public MapHomePresenter getMapHomePresenter() {
        return mapHomePresenter;
    }

    public UserProfilePresenter getUserProfilePresenter() {
        return userProfilePresenter;
    }

    public Model getModel() {
        return model;
    }

    public DatabaseHandler getDbHandler() {
        return dbHandler;
    }

    public ConnectionHandler getConnectionHandler() {
        return connectionHandler;
    }
}
