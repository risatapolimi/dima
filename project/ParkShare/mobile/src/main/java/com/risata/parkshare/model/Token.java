package com.risata.parkshare.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marco Sartini on 08/05/2018.
 */

public class Token {
    @SerializedName("token")
    String token;
    @SerializedName("userid")
    String userid;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
