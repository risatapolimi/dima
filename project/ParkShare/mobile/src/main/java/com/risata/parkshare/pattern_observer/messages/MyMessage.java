package com.risata.parkshare.pattern_observer.messages;

import android.app.Fragment;

import com.risata.parkshare.model.Model;
import com.risata.parkshare.presenters.MainPresenter;

/**
 * Created by Daniele on 16/03/2018.
 */

public interface MyMessage {

    void perform(MainPresenter mainPresenter);
}
