package com.risata.parkshare.pattern_observer.messages;

import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;

import com.risata.parkshare.R;
import com.risata.parkshare.fragments.UserProfileFragment;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.model.User;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.presenters.UserProfilePresenter;
import com.risata.parkshare.webservice.Status;

/**
 * Created by Marco Sartini on 31/03/2018.
 */

public class ReadyProfileFragment implements MyMessage {

    @Override
    public void perform(MainPresenter mainPresenter) {
        UserProfileFragment userProfileFragment = mainPresenter.getUserProfilePresenter().getUserProfileFragment();
        User user = mainPresenter.getModel().getUser();

        setUp(user, userProfileFragment);
        /*
        mainPresenter.getModel().getUser("pluto").observe(userProfileFragment, user -> {
            if( user.status.equals(Status.SUCCESS))
                setUp(user.data, userProfileFragment);
            else{
                Log.d("network", "elseReadyProfile");
            }
        });
*/

       // setUp(user, userProfileFragment);
    }

    private void setUp(User user, UserProfileFragment userProfileFragment){
        Ranking ranking = user.getMyRanking();
        userProfileFragment.setNomeTV(user.getName() + " " + user.getSurname());
        userProfileFragment.setRankingTV(Integer.toString(ranking.getLevel()));

        userProfileFragment.setRadiusTV(userProfileFragment.getString(R.string.user_radius, ranking.getRadiusMeters()));

        userProfileFragment.setupListView(user.getCarList());
        userProfileFragment.setMaxLevelPB(ranking.getNextLevelExperience());
        userProfileFragment.setToNextLevelPB(ranking.getExp()-ranking.getMaxLevelExperience());
        Log.d("PB_ranking", "Exp: "+(ranking.getExp()-ranking.getMaxLevelExperience())+ " su max: "+ ranking.getNextLevelExperience());
        //userProfileFragment.setupListView();
    }
}
