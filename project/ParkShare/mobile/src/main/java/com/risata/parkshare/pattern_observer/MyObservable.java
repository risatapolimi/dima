package com.risata.parkshare.pattern_observer;

import com.risata.parkshare.pattern_observer.messages.MyMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniele on 16/03/2018.
 */

public abstract class MyObservable<T> {

    private List<MyObserver<T>> observers;

    public MyObservable(){
        observers=new ArrayList<>();
    }

    public void registerObserver(MyObserver myObserver){
        this.observers.add(myObserver);
    }

    public void removeObserver(MyObserver myObserver){
        this.observers.remove(myObserver);
    }

    public void notifyObserver(T message){
        for(MyObserver<T> myObserver:observers)
            myObserver.update(message);
    }

}
