package com.risata.parkshare.pattern_observer.messages;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.risata.parkshare.R;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.presenters.MainPresenter;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoveParkingMessage implements MyMessage {

    private Position positionParkToRemove;

    public RemoveParkingMessage(Position destination) {
        positionParkToRemove=destination;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        ParkingSlot parkToRemove=this.getParkingToRemove(mainPresenter.getModel());
        MapHomeFragment mapHomeFragment=mainPresenter.getMapHomePresenter().getMapHomeFragment();
        if(parkToRemove!=null){

            mainPresenter.getConnectionHandler().deleteParking(parkToRemove.getId(), new Callback<ParkingSlotPOJO>() {

                @Override
                public void onResponse(Call<ParkingSlotPOJO> call, Response<ParkingSlotPOJO> response) {
                    if (response.isSuccessful()) {
                        Context context=mapHomeFragment.getContext();
                        if(context!=null) {
                            Toast.makeText(context, "Parking removed", Toast.LENGTH_SHORT).show();
                        }
                        //Update automatico alla prossima ricezione della posizione
                    }else{
                        String error="";
                        try {
                            error =response.errorBody().string();
                        } catch (IOException e) {
                            error=mapHomeFragment.getString(R.string.server_error);
                        }
                        Toast.makeText(mapHomeFragment.getContext(),error,Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Toast.makeText(mapHomeFragment.getContext(), R.string.connection_failure,Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private ParkingSlot getParkingToRemove(Model model) {
        //Position userLocation=model.getUserLocation();
        ParkingSlot oldParking=null;
        ///double distance=positionParkToRemove.differenceKm(userLocation);
        boolean found=false;
        for(ParkingSlot p:model.getParkingSlotList()){
            if(!found && p.hasSameLocation(new LatLng(positionParkToRemove.getLatitude(),positionParkToRemove.getLongitude()))){
                found=true;
                oldParking=p;
            }

        }
        if(found){
            return oldParking;
        }else{
            //elimino il parcheggio più vicino nel raggio di 50 metri
            oldParking=positionParkToRemove.nearestParking(model.getParkingSlotList(),model.getFilter(),
                    model.getUser().getMyRanking().getRadiusMeters()*0.001,positionParkToRemove);
            if(oldParking!=null){
                if(positionParkToRemove.differenceKm(oldParking.getPosition())<0.05){
                    return oldParking;
                }
            }
        }
        return oldParking;
    }
}
