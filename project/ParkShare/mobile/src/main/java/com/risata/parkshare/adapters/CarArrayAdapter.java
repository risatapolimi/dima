package com.risata.parkshare.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.risata.parkshare.R;
import com.risata.parkshare.extendedGraphics.DeleteCarImageButton;
import com.risata.parkshare.extendedGraphics.EditCarImageButton;
import com.risata.parkshare.extendedGraphics.FavoriteCarImageButton;
import com.risata.parkshare.model.Car;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Marco Sartini on 20/03/2018.
 */

public class CarArrayAdapter extends ArrayAdapter<Car> {

    private View.OnClickListener favoriteListener;
    private  View.OnClickListener deleteListener;
    private View.OnClickListener ediListener;

    public CarArrayAdapter(Context context, List<Car> carList, View.OnClickListener favoriteListener, View.OnClickListener editListener, View.OnClickListener deleteListener) {
        super(context, 0, carList);
        this.favoriteListener = favoriteListener;
        this.deleteListener = deleteListener;
        this.ediListener = editListener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        Car car = getItem(position);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = layoutInflater.inflate(R.layout.car_item, null);

            convertView.setTag(new CarViewHolder(convertView));
        }

        CarViewHolder viewHolder = (CarViewHolder) convertView.getTag();

        viewHolder.titleTV.setText(car.getManufacturer() + " " + car.getModel());
      //  viewHolder.manufacturerTV.setText(car.getManufacturer());
      //  viewHolder.modelTV.setText(car.getModel());
        viewHolder.dimensionsTV.setText(getContext().getString(R.string.car_item_dimension_values,Double.toString(car.getLength()),Double.toString(car.getWidth())));
        viewHolder.categoryTV.setText(car.getCategory());
        viewHolder.favoriteCarIB.setCar(car);
        viewHolder.favoriteCarIB.setOnClickListener(favoriteListener);
        if (car.isFavorite())
            setFavoriteOn(viewHolder);
        else
            setFavoriteOff(viewHolder);
        viewHolder.deleteCarIB.setCar(car);
        viewHolder.deleteCarIB.setOnClickListener(deleteListener);
        viewHolder.editCarIB.setCar(car);
        viewHolder.editCarIB.setOnClickListener(ediListener);


        return convertView;
    }

    public void setFavoriteOn(CarViewHolder viewHolder) {
        viewHolder.favoriteCarIB.setImageResource(R.drawable.ic_favorite_on);
    }

    public void setFavoriteOff(CarViewHolder viewHolder) {
        viewHolder.favoriteCarIB.setImageResource(R.drawable.ic_favorite_off);
    }

    static class CarViewHolder {

        TextView titleTV;
     //   TextView manufacturerTV;
     //   TextView modelTV;
        TextView dimensionsTV;
        FavoriteCarImageButton favoriteCarIB;
        DeleteCarImageButton deleteCarIB;
        EditCarImageButton editCarIB;
        TextView categoryTV;

        public CarViewHolder(View view) {
            titleTV = (TextView) view.
                    findViewById(R.id.carTitleTV);
            categoryTV = (TextView) view.findViewById(R.id.carCategoryTV);
       /*     manufacturerTV = (TextView) view.
                    findViewById(R.id.carManufacturerTV);
            modelTV = (TextView) view.
                    findViewById(R.id.carModelTV); */
            dimensionsTV = (TextView) view.
                    findViewById(R.id.carDimensionsTV);
            favoriteCarIB = (FavoriteCarImageButton) view.findViewById(R.id.item_car_favorite);
            deleteCarIB = (DeleteCarImageButton) view.findViewById(R.id.item_car_delete);
            editCarIB = (EditCarImageButton) view.findViewById(R.id.item_car_edit);
        }

    }
}
