package com.risata.parkshare.repository;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Ranking;

@Dao
public interface RankingDao {

    @Query("SELECT * FROM ranking WHERE userId = :userId")
    Ranking load(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Ranking ranking);

    @Query("UPDATE Ranking SET level= :level, exp= :exp WHERE userId = :userId")
    void update(int level, int exp, String userId);

    /**
     * Delete all ranking.
     */
    @Query("DELETE FROM Ranking")
    void deleteAllRankings();

}
