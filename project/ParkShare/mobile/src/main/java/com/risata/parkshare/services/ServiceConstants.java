package com.risata.parkshare.services;

public final class ServiceConstants {
    // Geocodification
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME =
            "com.google.android.gms.location.sample.locationaddress";
    public static final String ADDRESS_RECEIVER = PACKAGE_NAME + ".ADDRESS_RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";
    //Maps service and notification
    public static final String MAPS_RECEIVER=PACKAGE_NAME+".MAPS_RECEIVER";
    public static boolean COME_FROM_NOTIFICATION=false;
    public static boolean FINISH_SERVICE=true;
    public static final int MAPS_SERVICE_RESULT_CODE=2;
    public static final String NOTIFICATION_YES_RECEIVER=PACKAGE_NAME+".YES_RECEIVER";
    public static final String NOTIFICATION_NO_RECEIVER=PACKAGE_NAME+".NO_RECEIVER";
    public static final int BROADCAST_RECEIVER_RESULT_CODE=3;
    public static final int ASK_PARKING_NOTIFICATION_ID=10;
}
