package com.risata.parkshare.pattern_observer.messages;

import com.risata.parkshare.model.Filter;
import com.risata.parkshare.presenters.MainPresenter;

/**
 * Created by Marco Sartini on 21/08/2018.
 */

public class FilterFreeMessage extends FilterMessage {
    boolean free;
    public FilterFreeMessage(boolean free) {
        super();
        this.free = free;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        mainPresenter.getModel().getFilter().setFree(free);
        super.perform(mainPresenter);
    }
}
