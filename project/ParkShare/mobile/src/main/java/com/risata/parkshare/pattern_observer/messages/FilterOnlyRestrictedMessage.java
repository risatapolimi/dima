package com.risata.parkshare.pattern_observer.messages;

import com.risata.parkshare.presenters.MainPresenter;

/**
 * Created by Marco Sartini on 21/08/2018.
 */

public class FilterOnlyRestrictedMessage extends FilterMessage {
    boolean restricted;

    public FilterOnlyRestrictedMessage(boolean restricted) {
        this.restricted = restricted;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        mainPresenter.getModel().getFilter().setOnlyRestricted(restricted);
        mainPresenter.getModel().getFilter().setNoRestricted(false);
        super.perform(mainPresenter);
    }
}
