package com.risata.parkshare.presenters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.model.Marker;
import com.risata.parkshare.R;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.handlers.LocationHandler;
import com.risata.parkshare.model.Filter;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.ModelHiding;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.services.MapsReceiver;
import com.risata.parkshare.services.ServiceConstants;
import com.risata.parkshare.services.AddressResultReceiver;
import com.risata.parkshare.services.FetchAddressIntentService;
import com.risata.parkshare.services.ParkingService;
import com.risata.parkshare.views.LoginActivity;
import com.risata.parkshare.views.MainActivity_Drawer;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by tavec on 01/04/2018.
 */

public class MapHomePresenter {

    private MapHomeFragment mapHomeFragment;
    private LocationHandler locationHandler;
    private MapsReceiver mapsReceiver;

    public MapHomePresenter( LocationHandler locationHandler,MapHomeFragment mapHomeFragment) {
        this.mapHomeFragment = mapHomeFragment;
        this.locationHandler=locationHandler;
    }


    public void getDirections(Marker marker, MainPresenter mainPresenter) {

        if (marker != null) {
            double lat = marker.getPosition().latitude;
            double lon = marker.getPosition().longitude;
            this.startNavigation(lat,lon,mainPresenter);
        }
    }

    private void startNavigation(double lat,double lon,MainPresenter mainPresenter){
        Location destination = new Location("");
        destination.setLatitude(lat);
        destination.setLongitude(lon);
        // start our service in background
        startParkingService(destination, mainPresenter);
        // start google maps
        startGoogleMaps(lat, lon);
    }

    public void startGoogleMaps(double lat, double lon) {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lat + "," + lon);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        mapIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mapHomeFragment.startActivityForResult(mapIntent,100);
        //mapHomeFragment.startActivity(mapIntent);
    }

    public void redirectMaps(double lat, double lon,MainPresenter mainPresenter) {
        Context context = mapHomeFragment.getActivity().getApplicationContext();
        if (mapsReceiver != null) {
            Toast.makeText(context, "Reindirizzamento in corso: premi si", Toast.LENGTH_LONG).show();
            Location loc = new Location("");
            loc.setLatitude(lat);
            loc.setLongitude(lon);
            mapsReceiver.setDestination(new Position(loc));
            mapsReceiver.setAlreadyAskForParking(false);
            this.startGoogleMaps(lat,lon);
        }else{
            ServiceConstants.FINISH_SERVICE=true;
            this.startNavigation(lat,lon,mainPresenter);
        }
    }

    private void startParkingService(Location destination, MainPresenter mainPresenter) {
        ServiceConstants.FINISH_SERVICE=false;
        Intent intent = new Intent(this.mapHomeFragment.getContext(), ParkingService.class);
        mapsReceiver=new MapsReceiver(new Handler(),new Position(destination),mainPresenter.getModel(),mapHomeFragment.getContext());
        mapsReceiver.registerObserver(mainPresenter);
        intent.putExtra(ServiceConstants.MAPS_RECEIVER, mapsReceiver);
        this.mapHomeFragment.getContext().startService(intent);
    }


    public MapHomeFragment getMapHomeFragment() {
        return mapHomeFragment;
    }


    public LocationHandler getLocationHandler() {
        return locationHandler;
    }

    public void assignParkingAddresses(Position position,ParkingSlotPOJO parking, Context context, MainPresenter mainPresenter) {
        if (context != null) {
            AddressResultReceiver addressReceiver;//Since this is called using maphomefragment context, if it is null it means that maphomefragment is not shown and so we don't need to update addresses
            addressReceiver=new AddressResultReceiver(new Handler(),parking);
            addressReceiver.registerObserver(mainPresenter);
            this.startGeoLocationService(position,addressReceiver );

        }

    }

    public void restartApplication(String userid, String token, ModelHiding model){
        Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                mapHomeFragment.getActivity().finish();
                Bundle bundle=new Bundle();
                bundle.putString("userId",userid);
                bundle.putString("token",token);
                Intent intent = new Intent(mapHomeFragment.getActivity().getApplicationContext(),MainActivity_Drawer.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtras(bundle);
                mapHomeFragment.startActivity(intent);
            }
        });


       // mapHomeFragment.markParking(model.getParkingSlotList(),model.getUserLocation(),model.getUser().getMyRanking());
    }

    public void restartApplicationForEnding(String userid, String token, ModelHiding model){
        mapHomeFragment.getActivity().finishActivity(100);
        Bundle bundle=new Bundle();
        bundle.putString("userId",userid);
        bundle.putString("token",token);
        Intent intent = new Intent(mapHomeFragment.getActivity().getApplicationContext(),MainActivity_Drawer.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtras(bundle);
        mapHomeFragment.startActivity(intent);
        // mapHomeFragment.markParking(model.getParkingSlotList(),model.getUserLocation(),model.getUser().getMyRanking());
    }



    private void startGeoLocationService(Location location, AddressResultReceiver mResultReceiver) {
        Intent intent = new Intent(this.mapHomeFragment.getContext(), FetchAddressIntentService.class);
        intent.putExtra(ServiceConstants.ADDRESS_RECEIVER, mResultReceiver);
        intent.putExtra(ServiceConstants.LOCATION_DATA_EXTRA, location);
        this.mapHomeFragment.getContext().startService(intent);
    }


    public void manageFilter(Filter filter){
        if(mapHomeFragment.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE && mapHomeFragment.getResources().getConfiguration().isLayoutSizeAtLeast(Configuration.SCREENLAYOUT_SIZE_LARGE)) {

            mapHomeFragment.getHandyHideCB().setChecked(filter.isNoRestricted());
            mapHomeFragment.getHandyOnlyCB().setChecked(filter.isOnlyRestricted());
            mapHomeFragment.getHidePayantCB().setChecked(filter.isFree());
            switch(filter.getSize()){
                case SMALL:
                    mapHomeFragment.getSizeSeek().setInfoText(mapHomeFragment.getString(R.string.filter_seek_small),0);
                    break;
                case PERFECT:
                    mapHomeFragment.getSizeSeek().setInfoText(mapHomeFragment.getString(R.string.filter_seek_perfect),1);
                    break;
                case LARGE:
                    mapHomeFragment.getSizeSeek().setInfoText(mapHomeFragment.getString(R.string.filter_seek_large),2);
                    break;
            }
        }
        else{
            PopupMenu popupMenu = mapHomeFragment.getPopup();
            popupMenu.getMenu().findItem(R.id.filter_no_handy_item).setChecked(filter.isNoRestricted());
            popupMenu.getMenu().findItem(R.id.filter_handy_item).setChecked(filter.isOnlyRestricted());
            popupMenu.getMenu().findItem(R.id.filter_payant_item).setChecked(filter.isFree());

            switch(filter.getSize()){
                case SMALL:
                    popupMenu.getMenu().findItem(R.id.filter_size_normal).setChecked(false);
                    popupMenu.getMenu().findItem(R.id.filter_size_large).setChecked(false);
                    break;
                case PERFECT:
                    popupMenu.getMenu().findItem(R.id.filter_size_normal).setChecked(true);
                    popupMenu.getMenu().findItem(R.id.filter_size_large).setChecked(false);
                    break;
                case LARGE:
                    popupMenu.getMenu().findItem(R.id.filter_size_large).setChecked(true);
                    popupMenu.getMenu().findItem(R.id.filter_size_normal).setChecked(false);
                    break;
            }


        }
    }

}
