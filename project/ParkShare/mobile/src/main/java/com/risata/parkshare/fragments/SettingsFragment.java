package com.risata.parkshare.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;



import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.risata.parkshare.R;
import com.risata.parkshare.pattern_observer.ObservablePreferenceFragmentCompat;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.pattern_observer.messages.SettingsFilterMessage;


public class SettingsFragment extends ObservablePreferenceFragmentCompat<MyMessage> implements SharedPreferences.OnSharedPreferenceChangeListener{

    private SwitchPreferenceCompat hideSmallSP;
    private SwitchPreferenceCompat onlyLargeSP;
    private SwitchPreferenceCompat hideRestrictedSP;
    private SwitchPreferenceCompat onlyRestrictedSP;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);
        PreferenceManager preferenceManager = getPreferenceManager();

        hideSmallSP = (SwitchPreferenceCompat) preferenceManager.findPreference("pref_key_filter_hide_small");
        onlyLargeSP = (SwitchPreferenceCompat) preferenceManager.findPreference("pref_key_filter_only_large");
        hideRestrictedSP = (SwitchPreferenceCompat) preferenceManager.findPreference("pref_key_filter_no_handy");
        onlyRestrictedSP = (SwitchPreferenceCompat) preferenceManager.findPreference("pref_key_filter_only_handy");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
                view.setBackgroundColor(getResources().getColor(R.color.light_background));
        return view;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if("pref_key_filter_no_handy".equals(key)){
            if(sharedPreferences.getBoolean(key,false)) {
                onlyRestrictedSP.setChecked(false);
            }
        }
        else if("pref_key_filter_only_handy".equals(key)){
            if (sharedPreferences.getBoolean(key, false)) {
                hideRestrictedSP.setChecked(false);
            }
        }

        if("pref_key_filter_hide_small".equals(key)){
            if(sharedPreferences.getBoolean(key,false)) {
                onlyLargeSP.setChecked(false);
            }
        }
        else if("pref_key_filter_only_large".equals(key)){
            if (sharedPreferences.getBoolean(key, false)) {
                hideSmallSP.setChecked(false);
            }
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        notifyObserver(new SettingsFilterMessage());
    }
}
