package com.risata.parkshare.webservice;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

import io.appflate.restmock.RESTMockFileParser;

public class JsonFIleParse implements RESTMockFileParser {

    private Context context;

    public JsonFIleParse(Context context) {
        this.context = context;
    }

    @Override
        public String readJsonFile(String jsonFilePath) throws Exception {
            //ClassLoader classLoader = this.getClass().getClassLoader();
            //URL resource = classLoader.getResource(jsonFilePath);

            DataInputStream textFileStream = new DataInputStream(context.getAssets().open(String.format(jsonFilePath)));
            StringBuilder fileContents = new StringBuilder(textFileStream.available());
            Scanner scanner = new Scanner(textFileStream, "UTF-8");
            String lineSeparator = System.getProperty("line.separator");

            try {
                while (scanner.hasNextLine()) {
                    fileContents.append(scanner.nextLine()).append(lineSeparator);
                }
                return fileContents.toString();
            } finally {
                scanner.close();
            }
        };
    }

