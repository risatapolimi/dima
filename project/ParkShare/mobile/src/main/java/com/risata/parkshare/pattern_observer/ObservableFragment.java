package com.risata.parkshare.pattern_observer;

import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniele, Giorgio e Marco on 13/04/2018.
 */

public abstract class ObservableFragment<T> extends Fragment {

    private List<MyObserver<T>> observers;

    public ObservableFragment(){
        observers=new ArrayList<>();
    }

    public void registerObserver(MyObserver myObserver){
        this.observers.add(myObserver);
    }

    public void removeObserver(MyObserver myObserver){
        this.observers.remove(myObserver);
    }

    public void notifyObserver(T message){
        for(MyObserver<T> M:observers)
            M.update(message);
    }

    public List<MyObserver<T>> getObservers() {
        return observers;
    }

    public void setObservers(List<MyObserver<T>> observers) {
        this.observers = observers;
    }
}
