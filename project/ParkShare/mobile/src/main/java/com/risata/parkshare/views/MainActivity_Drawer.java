package com.risata.parkshare.views;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.risata.parkshare.R;
import com.risata.parkshare.callback.UserCallback;
import com.risata.parkshare.fragments.AboutFragment;
import com.risata.parkshare.fragments.HelpFragment;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.fragments.SettingsFragment;
import com.risata.parkshare.fragments.UserProfileFragment;
import com.risata.parkshare.handlers.ConnectionHandler;
import com.risata.parkshare.handlers.DatabaseHandler;
import com.risata.parkshare.handlers.LocationHandler;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Filter;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.model.Size;
import com.risata.parkshare.model.User;
import com.risata.parkshare.pattern_observer.messages.EndNavigationMessage;
import com.risata.parkshare.pattern_observer.messages.RedirectParkingMessagge;
import com.risata.parkshare.pattern_observer.messages.RemoveParkingMessage;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.repository.RememberMe;
import com.risata.parkshare.services.ParkingService;
import com.risata.parkshare.services.ServiceConstants;
import com.risata.parkshare.viewmodels.MemoryStorage;


import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.appflate.restmock.RESTMockServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity_Drawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MapHomeFragment.OnFragmentInteractionListener, UserProfileFragment.OnFragmentInteractionListener, HelpFragment.OnFragmentInteractionListener, AboutFragment.OnFragmentInteractionListener{

    private DrawerLayout mDrawerLayout;
    private FragmentTransaction ft;
    Toolbar toolbar;

    private MapHomeFragment mapHomeFragment;
    private UserProfileFragment userProfileFragment;
    private HelpFragment helpFragment;
    private AboutFragment aboutFragment;
    private SettingsFragment settingsFragment;

    private Model model;

    private MainPresenter mainPresenter;

    private User user;
    private String userid;
    private Location initLocation;
    private String token;
    private Bundle bundle;
    private ConnectionHandler connectionHandler;
    private RetrieveTask task;
    private DatabaseHandler dbHandler;
    private LocationHandler locationHandler;
    private Executor executor;
    private Bundle saveState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        saveState=savedInstanceState;
        user=null;
        bundle = getIntent().getExtras();
        userid=bundle.getString("userId");
        token=bundle.getString("token");
        initLocation=new Location("");
        initLocation.setLongitude(bundle.getDouble("lon"));
        initLocation.setLatitude(bundle.getDouble("lan"));
        if(bundle.getBoolean("mock")) {
            connectionHandler = new ConnectionHandler(RESTMockServer.getUrl(), userid, token);
        }else {
            connectionHandler = new ConnectionHandler(getString(R.string.url_server),
                    userid, token);
        }
        executor = Executors.newSingleThreadExecutor();
        //Creo DAO e lo inizializzo
        dbHandler = new DatabaseHandler(this,executor, userid);
        try {
            if (saveState != null) {
                user = dbHandler.initUser(userid);
                initStructure();
            } else {
                dbHandler.clear();
                task = new RetrieveTask();
                task.execute((Void) null);
            }
        }catch(Exception exp){
            dbHandler.clear();
            dbHandler.clearRememberme();
            task = new RetrieveTask();
            task.execute((Void) null);

        }



    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        MemoryStorage memoryStorage = ViewModelProviders.of(this).get(MemoryStorage.class);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if(memoryStorage.getActiveFragmenteTag().equals("map")){
            finish();
        }
        else {
            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE && isLandscapeLarge()){
                this.toolbar.setVisibility(View.GONE);
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                navigationView.setCheckedItem(R.id.nav_home);


                ft = getSupportFragmentManager().beginTransaction();
                ft.remove(currentFragment(memoryStorage.getActiveFragmenteTag()));
                ft.commit();


                memoryStorage.setActiveFragmenteTag("map");
                //super.onBackPressed();
            }
            else{
                this.toolbar.setVisibility(View.GONE);
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.main_frame, this.mapHomeFragment);
                ft.commit();
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                navigationView.setCheckedItem(R.id.nav_home);
                memoryStorage.setActiveFragmenteTag("map");
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            //noinspection SimplifiableIfStatement
            /*case R.id.action_settings:
                return true;*/

            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        final Intent intent;
        MemoryStorage memoryStorage = ViewModelProviders.of(this).get(MemoryStorage.class);
        int id = item.getItemId();
        //Log.d("FM_PS", getSupportFragmentManager().getFragments().toString());
        switch (id){
            case R.id.nav_home:
                ft = getSupportFragmentManager().beginTransaction();
                if(isLandscapeLarge()){
                    if(!memoryStorage.getActiveFragmenteTag().equals("map")) {
                        ft.remove(currentFragment(memoryStorage.getActiveFragmenteTag()));
                    }
                   // ft.replace(R.id.fragment_container, null );
                    //getSupportFragmentManager().popBackStack();
                    //ft.addToBackStack("map");
                }
                else {
                    ft.replace(R.id.main_frame, this.mapHomeFragment);
                   // ft.addToBackStack("map");
                }

                ft.commit();
                memoryStorage.setActiveFragmenteTag("map");
                this.toolbar.setTitle(R.string.menu_home);
                this.toolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_profile:
                ft = getSupportFragmentManager().beginTransaction();
                if(isLandscapeLarge()){
                    ft.replace(R.id.fragment_container, this.userProfileFragment);
                    //getSupportFragmentManager().popBackStack();
                    //ft.addToBackStack("profile");
                }
                else {
                    ft.replace(R.id.main_frame, this.userProfileFragment);
                    //ft.addToBackStack("profile");
                }

                ft.commit();
                memoryStorage.setActiveFragmenteTag("profile");
                this.toolbar.setTitle(R.string.menu_profile);
                this.toolbar.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_settings:
                ft = getSupportFragmentManager().beginTransaction();
                if(isLandscapeLarge()){
                    ft.replace(R.id.fragment_container, this.settingsFragment);
                    //getSupportFragmentManager().popBackStack();
                    //ft.addToBackStack("settings");
                }
                else {
                    ft.replace(R.id.main_frame, this.settingsFragment);
                   // ft.addToBackStack("settings");
                }

                ft.commit();
                memoryStorage.setActiveFragmenteTag("settings");
                this.toolbar.setTitle(R.string.menu_settings);
                this.toolbar.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_help:
                ft = getSupportFragmentManager().beginTransaction();
                if(isLandscapeLarge()){
                    ft.replace(R.id.fragment_container, this.helpFragment);
                    //getSupportFragmentManager().popBackStack();
                    //ft.addToBackStack("help");
                }
                else {
                    ft.replace(R.id.main_frame, this.helpFragment);
                    //ft.addToBackStack("help");
                }

                ft.commit();
                memoryStorage.setActiveFragmenteTag("help");
                this.toolbar.setTitle(R.string.menu_help);
                this.toolbar.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_about:
                ft = getSupportFragmentManager().beginTransaction();
                if(isLandscapeLarge()){
                    ft.replace(R.id.fragment_container, this.aboutFragment);
                    //getSupportFragmentManager().popBackStackImmediate();
                    //ft.addToBackStack("about");
                }
                else {
                    ft.replace(R.id.main_frame, this.aboutFragment);
                    //ft.addToBackStack("about");
                }

                ft.commit();
                memoryStorage.setActiveFragmenteTag("about");
                this.toolbar.setTitle(R.string.menu_about);
                this.toolbar.setVisibility(View.VISIBLE);
                break;

            case R.id.nav_logout:
                DatabaseHandler.insertRememberMe(new RememberMe(user.getEmail(),"",false,""),this.getApplicationContext());
                intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;




        }

        getSupportFragmentManager().executePendingTransactions();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        Log.d("FM_PS_fine", getSupportFragmentManager().getFragments().toString());
        return true;
    }

    private boolean isLandscapeLarge(){
        Configuration configuration = getResources().getConfiguration();
        return configuration.orientation == Configuration.ORIENTATION_LANDSCAPE && configuration.isLayoutSizeAtLeast(Configuration.SCREENLAYOUT_SIZE_LARGE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if(!isLandscapeLarge() ) {
            MemoryStorage memoryStorage = ViewModelProviders.of(this).get(MemoryStorage.class);
            ft = getSupportFragmentManager().beginTransaction();
            ft.remove(currentFragment(memoryStorage.getActiveFragmenteTag()));
            ft.commit();
        }
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void openDrawer() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public class RetrieveTask extends AsyncTask<Void, Void, Boolean>{
        @Override
        protected Boolean doInBackground(Void... voids) {
            //Ricavo i dati dell'utente dal server
            connectionHandler.getUser(userid,new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    user = new User(response.body().getUserId(),response.body().getName(), response.body().getSurname(),
                            response.body().getEmail(), response.body().getPwdHash());

                    //RECUPERO I DATI DI RANKING DELL'USER
                    connectionHandler.getRanking(userid,new Callback<Ranking>() {
                        @Override
                        public void onResponse(Call<Ranking> call, Response<Ranking> response) {
                            Ranking rank=new Ranking(response.body().getUserId(),response.body().getLevel(),
                                    response.body().getExp());
                            user.setMyRanking(rank);

                            // RECUPERO LE MACCHINE DELL'UTENTE
                            connectionHandler.getCarList(userid,new Callback<List<Car>>() {
                                @Override
                                public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {
                                    List<Car> list=response.body();
                                    if(list!=null)
                                        if(!list.isEmpty()) {
                                            user.setCarList(list);
                                            Log.d("userCars",list.toString());
                                        }
                                    //Ho recuperato tutti i dati dal server, posso proseguire
                                    processData();
                                }
                                @Override
                                public void onFailure(Call<List<Car>> call, Throwable t) {
                                    System.out.println("Errore Cars:"+t.getMessage());
                                }
                            });
                        }

                        @Override
                        public void onFailure(Call<Ranking> call, Throwable t) {
                            System.out.println("Errore ranking:"+t.getMessage());
                        }
                    });
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    System.out.println("Errore user:"+t.getMessage());
                }
            });

            return true;
        }
    }

    private void processData(){

        AsyncTask dataTask=new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                dbHandler.initUserData(user);
                return true;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                initStructure();
            }
        };
        dataTask.execute();
    }

    public void initStructure(){
        locationHandler = new LocationHandler(this,getBaseContext());
        model=new Model(userid,new Position(initLocation), dbHandler, executor);
        model.setUser(user);

        model.getFilter().setInUseCar(user.getInUseCar());

        mapHomeFragment = new MapHomeFragment();
        userProfileFragment = new UserProfileFragment();
        helpFragment = new HelpFragment();
        aboutFragment = new AboutFragment();
        settingsFragment = new SettingsFragment();


        mainPresenter = new MainPresenter(mapHomeFragment, userProfileFragment, this, model, dbHandler, connectionHandler,locationHandler);
        userProfileFragment.registerObserver(mainPresenter);
        mapHomeFragment.registerObserver(mainPresenter);
        settingsFragment.registerObserver(mainPresenter);

        model.initData(new CallbackYoYo(this,this ));
    }

    public Fragment currentFragment(String tag){
        switch (tag){
            case "map":
                return mapHomeFragment;
            case "profile":
                return userProfileFragment;
            case "about":
                return aboutFragment;
            case "settings":
                return settingsFragment;
            case "help":
                return helpFragment;
        }
        return mapHomeFragment;
    }

    public String currentMenuTitle(String tag){
        switch (tag){
            case "profile":
                return getString(R.string.menu_profile);
            case "about":
                return getString(R.string.menu_about);
            case "settings":
                return getString(R.string.menu_settings);
            case "help":
                return getString(R.string.menu_help);
        }
        return getString(R.string.app_name);
    }

    public void askForParkingFound(Position dest, Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Add the buttons
        builder.setTitle(R.string.is_there_parking);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                mapHomeFragment.notifyObserver(new RemoveParkingMessage(dest));
                stopService(new Intent(getApplicationContext(),ParkingService.class));
                EndNavigationMessage.askForOthersParking(mainPresenter);
                ServiceConstants.FINISH_SERVICE=true;
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                mapHomeFragment.notifyObserver(new RemoveParkingMessage(dest));
                //TODO Non reindirizzoooo, sceglie lui un'altra destinazione, creare stringa
                //mapHomeFragment.notifyObserver(new RedirectParkingMessagge(dest));
                Toast.makeText(context,"Scegli un'altra destinazione",Toast.LENGTH_SHORT).show();
                ServiceConstants.FINISH_SERVICE=true;
            }
        });
        // Set other dialog properties

        // Create the AlertDialog
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        if(ServiceConstants.COME_FROM_NOTIFICATION){
            Location location=new Location("");
            location.setLongitude(bundle.getDouble("destlong"));
            location.setLatitude(bundle.getDouble("destlat"));
            Position destination=new Position(location);
            Thread thread=new Thread(() -> {
                    while(mapHomeFragment==null){
                    }
                    while(!mapHomeFragment.isAdded()){
                    }
                    while(mainPresenter.getModel().getParkingSlotList().size()==0){
                    }
                askForParkingFound(destination,this);
            });
            thread.start();
            //this.notifyObserver(new RemoveParkingMessage(destination));
            ServiceConstants.COME_FROM_NOTIFICATION=false;

        }
    }



    public class CallbackYoYo{

        private AppCompatActivity activity;
        private NavigationView.OnNavigationItemSelectedListener listener;

        public CallbackYoYo(AppCompatActivity activity, NavigationView.OnNavigationItemSelectedListener listener) {
            this.activity = activity;
            this.listener = listener;
        }

        public void onTaskExecuted() {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setContentView(R.layout.activity_main);
                    toolbar = (Toolbar) findViewById(R.id.toolbar_main);
                    setSupportActionBar(toolbar);
                    ActionBar actionbar = getSupportActionBar();
                    actionbar.setDisplayHomeAsUpEnabled(true);
                    actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

                    mDrawerLayout = findViewById(R.id.drawer_layout);



     /*   ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
*/
                    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                    navigationView.setNavigationItemSelectedListener(listener);
                    View headerView = navigationView.getHeaderView(0);

                    String user_name = model.getUser().getName() + " " + model.getUser().getSurname();
                    ((TextView)headerView.findViewById(R.id.nav_header_user_name)).setText(user_name);
                    ((TextView)headerView.findViewById(R.id.nav_header_email)).setText(model.getUser().getEmail());


                    MemoryStorage memoryStorage = ViewModelProviders.of(activity).get(MemoryStorage.class);
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    //this.statusFragment.registerObserver(this);
                    if(saveState!=null){
                        String tag=memoryStorage.getActiveFragmenteTag();
                        /*Location location=new Location("");
                        location.setLatitude(memoryStorage.getLastLatitudidePosition());
                        location.setLongitude(memoryStorage.getLastLongitudePosition());
                        model.setSearchLocation(new Position(location));*/
                        if(isLandscapeLarge()){
                            if(tag.equals("map")){
                                ft.replace(R.id.main_frame, currentFragment(tag), tag);

                            }else {
                                ft.replace(R.id.fragment_container, currentFragment(tag), tag);
                                ft.replace(R.id.main_frame, currentFragment("map"), "map");
                                //getSupportFragmentManager().popBackStackImmediate();
                                //ft.addToBackStack(tag);
                            }

                            ft.commit(); //MARCO

                        }else{
                            ft.replace(R.id.main_frame, currentFragment(tag),tag);//.addToBackStack(tag);
                            ft.commit();
                        }
                        if(!tag.equals("map")) {
                            toolbar.setVisibility(View.VISIBLE);
                            toolbar.setTitle(currentMenuTitle(tag));
                        }
                        model.getFilter().setFree(memoryStorage.isFree());
                        model.getFilter().setNoRestricted(memoryStorage.isNoRestricted());
                        model.getFilter().setOnlyRestricted(memoryStorage.isOnlyRestricted());
                        model.getFilter().setSize(Size.valueOf(memoryStorage.getSize()));

                    }else{
                        memoryStorage.setLastLongitudePosition(model.getSearchLocation().getLongitude());
                        memoryStorage.setLastLatitudidePosition(model.getSearchLocation().getLatitude());
                        memoryStorage.setInSearchingMode(false);
                        memoryStorage.setParkingSelected(-1);
                        ft.add(R.id.main_frame, mapHomeFragment);
                        ft.commit();
                        memoryStorage.setActiveFragmenteTag("map");
                    }

                }
            });

        }

    }
}

