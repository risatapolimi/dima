package com.risata.parkshare.handlers;

import android.location.Location;

import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.model.Token;
import com.risata.parkshare.model.User;
import com.risata.parkshare.webservice.RESTInterface;

import java.util.List;

import io.appflate.restmock.RESTMockServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Marco Sartini on 04/05/2018.
 */

public class ConnectionHandler {

    private RESTInterface restInterface;
    private String userId;
    private String tokenAuth;

    public ConnectionHandler(String baseUrl, String userId, String pwdHash, int zero) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.restInterface = retrofit.create(RESTInterface.class);
        this.userId = userId;

    }

    public ConnectionHandler(String baseUrl, String userId, String tokenAuth) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.restInterface = retrofit.create(RESTInterface.class);
        this.userId = userId;
        this.tokenAuth = tokenAuth;
    }

    public void getUser(String userid,Callback callback){
        Call<User> call = restInterface.getUser(userid);
        call.enqueue(callback);
    }

    public void getRanking(String userid,Callback callback){
        Call<Ranking> call = restInterface.getRanking(userid);
        call.enqueue(callback);
    }

    public void addNewCar(Car newCar, Callback callback){
        Call<Car> call = restInterface.postCar(tokenAuth, newCar);
        call.enqueue(callback);
    }

    public void deleteCar(Car deletingCar, Callback callback){
        Call<Car> call = restInterface.deleteCar(tokenAuth, userId, deletingCar.getCarId());
        call.enqueue(callback);
    }

    public void editCar(Car editingCar, Callback callback){
        Call<Car> call = restInterface.putEditedCar(tokenAuth, userId, editingCar.getCarId(), editingCar);
        call.enqueue(callback);
    }

    public void signalParking(ParkingSlotPOJO parkingSignal, Callback callback){
        Call<ParkingSlotPOJO> call = restInterface.postSlot(tokenAuth, parkingSignal);
        call.enqueue(callback);
    }

    public void getParkingSlots(Location location, Callback callback){
        if(location!=null) {
            Call<List<ParkingSlotPOJO>> call = restInterface.getParkingSlots(tokenAuth,""+location.getLatitude(),""+location.getLongitude());
            call.enqueue(callback);
        }
    }

    public void getAllParkingSlots(Callback callback){
        Call<List<ParkingSlotPOJO>> call = restInterface.getAllParkingSlots(tokenAuth);
        call.enqueue(callback);
    }

    public static void serverLogin(String loginUrl, String userId, String pwdHash, Callback<Token> callback){
        Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(loginUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        RESTInterface restInterface = retrofit.create(RESTInterface.class);

        Call<Token> call = restInterface.getToken(userId, pwdHash);
        call.enqueue(callback);
    }

    private RESTInterface createRestInterface(boolean mock){
        if(mock){
            Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RESTMockServer.getUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RESTInterface restInterface = retrofit.create(RESTInterface.class);
        }
        return restInterface;
    }

    public void getCarList( String userid, Callback<List<Car>> callback) {
        Call<List<Car>> call = restInterface.getCarList(userid);
        call.enqueue(callback);
    }

    public void deleteParking(int id, Callback<ParkingSlotPOJO>callback) {
        Call<ParkingSlotPOJO> call = restInterface.deleteParking(tokenAuth,""+id);
        call.enqueue(callback);
    }

    public String getTokenAuth() {
        return tokenAuth;
    }

    public static void registerUser(String registrationUrl, String name, String surname, String email, String password, Callback<User> callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(registrationUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RESTInterface restInterface = retrofit.create(RESTInterface.class);
        Call<User> call = restInterface.postUser(new User("-1", name, surname, email, password));
        call.enqueue(callback);
    }

    public void putParking(int id, ParkingSlotPOJO park, Callback<ParkingSlotPOJO> callback) {
        Call<ParkingSlotPOJO> call = restInterface.putParking(tokenAuth,""+id,park);
        call.enqueue(callback);
    }

    public void addRankingPoint(Callback<Ranking> callback) {
        Call<Ranking> call = restInterface.addRankingPoint(userId);
        call.enqueue(callback);
    }
}
