package com.risata.parkshare.pattern_observer.messages;

import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotItem;
import com.risata.parkshare.presenters.MainPresenter;

public class InsertCardView implements MyMessage {
    @Override
    public void perform(MainPresenter mainPresenter) {
        ParkingSlotItem parkingSlot = mainPresenter.getMapHomePresenter().getMapHomeFragment().getParkingSlotAdapter().getParkingSlotList().get(0);
        mainPresenter.getMapHomePresenter().getMapHomeFragment().getParkingSlotAdapter().getParkingSlotList().add(0,parkingSlot);

        mainPresenter.getMapHomePresenter().getMapHomeFragment().getParkingSlotAdapter().notifyItemInserted(0);


    }
}
