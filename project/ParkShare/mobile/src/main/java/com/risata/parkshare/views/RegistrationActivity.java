package com.risata.parkshare.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.risata.parkshare.R;
import com.risata.parkshare.handlers.ConnectionHandler;
import com.risata.parkshare.model.Token;
import com.risata.parkshare.model.User;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {

    private EditText name_ET;
    private EditText surname_ET;
    private EditText email_ET;
    private EditText password_ET;
    private EditText passwordConf_ET;
    private Button register_B;
    private ScrollView registration_form;
    private ProgressBar registration_progress;
    private  UserRegistrationTask mAuthTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        this.setTitle("Registration");

        registration_form = findViewById(R.id.registration_form);
        registration_progress = findViewById(R.id.registration_progress);

        name_ET = (EditText) findViewById(R.id.reg_name_value);
        surname_ET = (EditText) findViewById(R.id.reg_surname_value);
        email_ET = (EditText) findViewById(R.id.reg_email_value);
        password_ET = (EditText) findViewById(R.id.reg_password_value);
        passwordConf_ET = findViewById(R.id.reg_password_confirm_value);
        register_B = (Button) findViewById(R.id.reg_button_value);

        register_B.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String name = name_ET.getText().toString();
                String surname = surname_ET.getText().toString();
                String email = email_ET.getText().toString();
                String password = password_ET.getText().toString();

                // Reset errors.
                name_ET.setError(null);
                surname_ET.setError(null);
                email_ET.setError(null);
                password_ET.setError(null);
                passwordConf_ET.setError(null);

                boolean cancel = false;
                View focusView = null;

                // Check for a valid password, if the user entered one.
                if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                    password_ET.setError(getString(R.string.error_invalid_password));
                    focusView = password_ET;
                    cancel = true;
                }

                if (!password.equals(passwordConf_ET.getText().toString())) {
                    passwordConf_ET.setError(getString(R.string.error_not_match_password));
                    focusView = passwordConf_ET;
                    cancel = true;
                }

                // Check for a valid email address.
                if (TextUtils.isEmpty(email)) {
                    email_ET.setError(getString(R.string.error_field_required));
                    focusView = email_ET;
                    cancel = true;
                } else if (!isEmailValid(email)) {
                    email_ET.setError(getString(R.string.error_invalid_email));
                    focusView = email_ET;
                    cancel = true;
                }

                //Check for non null name and surname
                if(TextUtils.isEmpty(name)){
                    name_ET.setError(getString(R.string.error_name_required));
                    focusView = name_ET;
                    cancel = true;
                }

                if(TextUtils.isEmpty(surname)){
                    surname_ET.setError(getString(R.string.error_surname_required));
                    focusView = surname_ET;
                    cancel = true;
                }


                if (cancel) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                } else {
                    // Show a progress spinner, and kick off a background task to
                    // perform the user login attempt.
                    showProgress(true);
                    mAuthTask = new RegistrationActivity.UserRegistrationTask(name, surname, email, password);
                    mAuthTask.execute((Void) null);
                }
            }

            private boolean isEmailValid(String email) {
                return email.contains("@");
            }

            private boolean isPasswordValid(String password) {
                return password.length() > 6;
            }

        });
    }

/**
 * Represents an asynchronous login/registration task used to authenticate
 * the user.
 */
public class UserRegistrationTask extends AsyncTask<Void, Void, Boolean> {

    private final String mEmail;
    private final String mPassword;
    private final String mName;
    private final String mSurname;
    //0 = non lo so, -1 = errore, 1 = ok
    UserRegistrationTask(String name, String surname, String email, String password) {
        mEmail = email;
        mPassword = password;
        mName = name;
        mSurname = surname;

    }


    @Override
    protected Boolean doInBackground(Void... params) {

        ConnectionHandler.registerUser(getString(R.string.url_server), mName, mSurname, mEmail, mPassword, new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {

                            Toast.makeText(getApplicationContext(), R.string.registration_successful, Toast.LENGTH_LONG).show();
                            finish();

                    myOnPostExecute(1);
                } else {
                    myOnPostExecute(2);
                    String error="";
                    try {
                        error = response.code() + " - " + response.message() + "\n"+ response.errorBody().string();

                    } catch (IOException e) {
                        e.printStackTrace();
                        error = getString(R.string.error_login);
                    }
                    Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.connection_failure,Toast.LENGTH_SHORT).show();

            }
        });

        return true;
    }

    private void myOnPostExecute(int resultCode){
        mAuthTask = null;
        showProgress(false);
        switch(resultCode) {
            case 1:
                finish();
                break;
            case 2:
                email_ET.setError(getString(R.string.error_already_used_email));
                email_ET.requestFocus();
                break;
        }

    }

    @Override
    protected void onPostExecute(final Boolean success) {

    }

    @Override
    protected void onCancelled() {
        mAuthTask = null;
        showProgress(false);
    }

}

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            registration_form.setVisibility(show ? View.GONE : View.VISIBLE);
            registration_form.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    registration_form.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            registration_progress.setVisibility(show ? View.VISIBLE : View.GONE);
            registration_progress.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    registration_progress.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            registration_progress.setVisibility(show ? View.VISIBLE : View.GONE);
            registration_form.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}
