package com.risata.parkshare.pattern_observer.messages;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.risata.parkshare.R;
import com.risata.parkshare.adapters.CarArrayAdapter;
import com.risata.parkshare.fragments.UserProfileFragment;
import com.risata.parkshare.handlers.ConnectionHandler;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Filter;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.ParkingSlotItem;
import com.risata.parkshare.presenters.MainPresenter;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Marco Sartini on 06/04/2018.
 */

public class FavoriteChange implements MyMessage {
    private Car newFavoriteCar;

    public FavoriteChange(Car newFavoriteCar) {
        this.newFavoriteCar = newFavoriteCar;
        this.newFavoriteCar.setFavorite(true);
    }


    @Override
    public void perform(MainPresenter mainPresenter) {
        UserProfileFragment userProfileFragment = mainPresenter.getUserProfilePresenter().getUserProfileFragment();
        Model model= mainPresenter.getModel();

        mainPresenter.getConnectionHandler().editCar(newFavoriteCar, new Callback<Car>(){

            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                if(response.isSuccessful()) {
                    mainPresenter.getDbHandler().setInUseCar(newFavoriteCar);
                    model.getUser().setCarList(mainPresenter.getDbHandler().getCarList());
                    updateFragment(userProfileFragment);

                    //aggiornamento filtro e adapter
                    Filter filter = mainPresenter.getModel().getFilter();
                    filter.setInUseCar(newFavoriteCar);
                    if(mainPresenter.getMapHomePresenter().getMapHomeFragment().isAdded()) {
                        mainPresenter.getMapHomePresenter().getMapHomeFragment().setCarSelected(newFavoriteCar.getManufacturer() + " " + newFavoriteCar.getModel());
                        mainPresenter.getMapHomePresenter().getMapHomeFragment().getParkingSlotAdapter().setInUseCar(newFavoriteCar);

                        List<ParkingSlotItem> parkingSlotItems = mainPresenter.getMapHomePresenter().getMapHomeFragment().getParkingSlotAdapter().getParkingSlotList();
                        for(ParkingSlotItem p: parkingSlotItems){
                            p.setAdvicedSize(filter.calculateParkingAdvicedDimension(p.getParkingSlot()));
                        }
                        mainPresenter.getMapHomePresenter().getMapHomeFragment().getParkingSlotAdapter().notifyDataSetChanged();
                    }
                    
                }else{
                    String error="";
                    try {
                        error =response.errorBody().string();
                        error = "error";
                    } catch (IOException e) {
                        error=userProfileFragment.getString(R.string.server_error);
                    }
                    Toast.makeText(userProfileFragment.getContext(),error,Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(userProfileFragment.getContext(), R.string.connection_failure,Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void updateFragment(UserProfileFragment fragment) {
        ((CarArrayAdapter) fragment.getCarsLV().getAdapter()).notifyDataSetChanged();
    }



}
