package com.risata.parkshare.viewmodels;

import android.arch.lifecycle.ViewModel;

public class MemoryStorage extends ViewModel {
    private String activeFragmenteTag;
    private boolean isInSearchingMode;
    private double lastLongitudePosition;
    private double lastLatitudidePosition;
    private boolean free;
    private boolean onlyRestricted;
    private boolean noRestricted;
    private String size;
    private int parkingSelected;

    public String getActiveFragmenteTag() {
        return activeFragmenteTag;
    }

    public void setActiveFragmenteTag(String activeFragmenteTag) {
        this.activeFragmenteTag = activeFragmenteTag;
    }

    public boolean isInSearchingMode() {
        return isInSearchingMode;
    }

    public void setInSearchingMode(boolean inSearchingMode) {
        isInSearchingMode = inSearchingMode;
    }

    public double getLastLongitudePosition() {
        return lastLongitudePosition;
    }

    public void setLastLongitudePosition(double lastLongitudePosition) {
        this.lastLongitudePosition = lastLongitudePosition;
    }

    public double getLastLatitudidePosition() {
        return lastLatitudidePosition;
    }

    public void setLastLatitudidePosition(double lastLatitudidePosition) {
        this.lastLatitudidePosition = lastLatitudidePosition;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public boolean isOnlyRestricted() {
        return onlyRestricted;
    }

    public void setOnlyRestricted(boolean onlyRestricted) {
        this.onlyRestricted = onlyRestricted;
    }

    public boolean isNoRestricted() {
        return noRestricted;
    }

    public void setNoRestricted(boolean noRestricted) {
        this.noRestricted = noRestricted;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getParkingSelected() {
        return parkingSelected;
    }

    public void setParkingSelected(int parkingSelected) {
        this.parkingSelected = parkingSelected;
    }
}
