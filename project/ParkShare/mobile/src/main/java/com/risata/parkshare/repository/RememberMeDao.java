package com.risata.parkshare.repository;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.risata.parkshare.model.Ranking;

@Dao
public interface RememberMeDao {

    @Query("SELECT * FROM RememberMe")
    RememberMe load();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(RememberMe rememberMe);

    /**
     * Delete all rememberme.
     * @param newRemember
     */
    @Query("DELETE FROM RememberMe WHERE email<>:newRemember")
    void deleteRememberme(String newRemember);

    @Query("DELETE FROM RememberMe")
    void deleteAll();
}
