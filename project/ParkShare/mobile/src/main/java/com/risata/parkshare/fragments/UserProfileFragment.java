package com.risata.parkshare.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;


import com.risata.parkshare.R;
import com.risata.parkshare.adapters.CarArrayAdapter;
import com.risata.parkshare.adapters.CarCursorAdapter;
import com.risata.parkshare.extendedGraphics.DeleteCarImageButton;
import com.risata.parkshare.extendedGraphics.EditCarImageButton;
import com.risata.parkshare.extendedGraphics.FavoriteCarImageButton;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.pattern_observer.MyObservable;
import com.risata.parkshare.pattern_observer.MyObserver;
import com.risata.parkshare.pattern_observer.ObservableFragment;
import com.risata.parkshare.pattern_observer.messages.DeleteCarChange;
import com.risata.parkshare.pattern_observer.messages.FavoriteChange;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.pattern_observer.messages.ReadyProfileFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserProfileFragment extends ObservableFragment<MyMessage>{

    public static final int AddCar = 123;
    public static final int EditCar = 125;


    private View v;
    private TextView nomeTV;
    private TextView rankingTV;
    private TextView radiusTV;
    private ProgressBar toNextLevelPB;
    private FloatingActionButton addCarFAB;

    private ListView carsLV;
    private View.OnClickListener favoriteListener;
    private View.OnClickListener deleteListener;
    private View.OnClickListener editListener;

    public void setNomeTV(String nome) {
        this.nomeTV.setText(nome);
    }

    public void setRankingTV(String ranking) {
        this.rankingTV.setText(ranking);
    }

    public void setRadiusTV(String radius) {
        this.radiusTV.setText(radius);
    }

    public void setToNextLevelPB(int toNextLevel) {
        this.toNextLevelPB.setProgress(toNextLevel);
    }

    public ListView getCarsLV() {
        return carsLV;
    }


    private OnFragmentInteractionListener mListener;

    public UserProfileFragment() {
        // Required empty public constructor
    }


    public static UserProfileFragment newInstance(String param1, String param2) {
        UserProfileFragment fragment = new UserProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //TODO: serve il puntatore a questo listener?
        favoriteListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyObserver(new FavoriteChange(((FavoriteCarImageButton)v.findViewById(R.id.item_car_favorite)).getCar()));
                Log.d("listener", "cliccato bottone favorite");
            }
        };

        deleteListener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder deleteDialog = new AlertDialog.Builder(getContext());
                deleteDialog.setTitle(R.string.confirm_delete_title);
                deleteDialog
                        .setMessage(R.string.confirm_delete_message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                notifyObserver(new DeleteCarChange(((DeleteCarImageButton)v.findViewById(R.id.item_car_delete)).getCar()));
                                Log.d("listener", "cliccato bottone sul dialog YES");
                            }
                        });
                deleteDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.d("listener", "cliccato bottone sul dialog NO");
                    }
                });
               // notifyObserver(new editChange(((FavoriteCarImageButton)v.findViewById(R.id.item_car_favorite)).getCar()));
                Log.d("listener", "cliccato bottone delete");
                AlertDialog dDialog = deleteDialog.create();
                dDialog.show();
            }
        };

        editListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: riutilizzare lo stesso fragment dell'add che è uguale??
                //TODO: inserire i dati della macchina selezionata per la modifica già nel dialog
                Car c = ((EditCarImageButton)v.findViewById(R.id.item_car_edit)).getCar();
                Bundle bundle = new Bundle();
                bundle.putString("userid",c.getUserId());
                bundle.putString("manufacturer", c.getManufacturer());
                bundle.putString("model", c.getModel());
                bundle.putDouble("length", c.getLength());
                bundle.putDouble("width", c.getWidth());
                bundle.putString("category", c.getCategory());
                bundle.putBoolean("isfavourite",c.isFavorite());
                bundle.putString("carId", c.getCarId());

                EditCarDialogFragment dialogFragment = new EditCarDialogFragment();
                dialogFragment.setTargetFragment(UserProfileFragment.this,EditCar);
                dialogFragment.setArguments(bundle);

                dialogFragment.show(getFragmentManager(), "editCarFrg");
            }
        };




    }

    public void setupListView(List<Car> carList){
        this.carsLV = (ListView) this.v.findViewById(R.id.userCarList);
        this.carsLV.setEmptyView(this.v.findViewById(R.id.emptyview_car));
        CarArrayAdapter carArrayAdapter = new CarArrayAdapter(getContext(), carList, favoriteListener, editListener, deleteListener);
        carsLV.setAdapter(carArrayAdapter);


    }

    public void setupListView(Cursor cursor){
        carsLV.setAdapter(new CarCursorAdapter(getContext(), cursor ,favoriteListener, editListener, deleteListener));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.v=inflater.inflate(R.layout.fragment_user_profile, container, false);

        this.nomeTV = this.v.findViewById(R.id.userNames);
        this.rankingTV = this.v.findViewById(R.id.profile_ranking);
        this.toNextLevelPB = this.v.findViewById(R.id.profile_to_next_level);
        this.radiusTV = this.v.findViewById(R.id.user_radius);
        this.addCarFAB = this.v.findViewById(R.id.addNewCar);




        this.addCarFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddCarDialogFragment dialogFragment = new AddCarDialogFragment();
                dialogFragment.setTargetFragment(UserProfileFragment.this,AddCar);

                //TODO: binding tra gli elementi fattibile anche qui, da questo fragment senza passare al dialog tutti i dati

                dialogFragment.show(getFragmentManager(), "addCarFrg");

            }
        });

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        notifyObserver(new ReadyProfileFragment());

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setMaxLevelPB(int maxLevelPB) {
        this.toNextLevelPB.setMax(maxLevelPB);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
