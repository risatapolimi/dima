package com.risata.parkshare.pattern_observer.messages;


import android.location.Location;
import android.util.Log;

import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.presenters.MainPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReturnOnMyPositionMessage extends UpdateParking {

    private MainPresenter mainPresenter;

    public ReturnOnMyPositionMessage() {
        super(null);  //Settata nel perform
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        super.loc = mainPresenter.getModel().getUserLocation();

        //mainPresenter.getMapHomePresenter().getLocationHandler().setUpUpdate();
        super.perform(mainPresenter);

    }
}
