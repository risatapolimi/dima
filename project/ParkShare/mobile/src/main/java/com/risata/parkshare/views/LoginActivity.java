package com.risata.parkshare.views;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.risata.parkshare.R;
import com.risata.parkshare.handlers.ConnectionHandler;
import com.risata.parkshare.handlers.DatabaseHandler;
import com.risata.parkshare.handlers.LocationHandler;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.model.Token;
import com.risata.parkshare.pattern_observer.MyObserver;
import com.risata.parkshare.model.User;
import com.risata.parkshare.repository.RememberMe;
import com.risata.parkshare.webservice.JsonFIleParse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.appflate.restmock.JVMFileParser;
import io.appflate.restmock.RESTMockServer;
import io.appflate.restmock.RESTMockServerStarter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;
import static io.appflate.restmock.utils.RequestMatchers.pathContains;


/**
 * A login screen that offers login via email/password, Google and Facebook account.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    private LocationHandler locationHandler;

    private static String token = "";
    private String userid;
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private TextView notRegisteredTV;
    //Facebook references.
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    //Google references.
    private GoogleSignInClient mGoogleSignInClient;
    private SignInButton signInButton;

    private CheckBox rememberCheckBox;

    //for information passing between activities
    private Bundle bundle;

    //Code for Google request login
    private static final int RC_SIGN_IN = 9000;
    private RememberMe rememberMe;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ClassLoader classLoader = this.getClass().getClassLoader();



        //RESTMockServerStarter.startSync(new JVMFileParser());
        RESTMockServerStarter.startSync(new JsonFIleParse(this));//MIO

        RESTMockServer.whenGET(pathContains("pwdHash/123456789")).thenReturnFile(200,"mockToken.json" );
        RESTMockServer.whenGET(pathContains("/users/pluto")).thenReturnFile(200, "mockuser.json");



        /*RESTMockServer.whenGET(pathContains("/users")).thenReturnString(200, "{\n" +
                "  \"Name\": \"Spider\",\n" +
                "  \"Surname\": \"Man\",\n" +
                "  \"UserID\": \"Ragnetto\",\n" +
                "  \"Email\": \"spider@man.it\",\n" +
                "  \"PwdHash\": \"spidey\"\n" +
                "}");*/


        RESTMockServer.whenPOST(pathContains("/users/marco@sartini.it/cars")).thenReturnFile(200, "mockcar.json");
        RESTMockServer.whenPUT(pathContains("/users/marco@sartini.it/cars")).thenReturnFile(200, "mockcarPunto.json");
        RESTMockServer.whenDELETE(pathContains("/users/marco@sartini.it/cars")).thenReturnFile(200, "mockcar.json");
        RESTMockServer.whenGET(pathContains("/parkings/marco@sartini.it")).thenReturnFile(200, "mockparkingslots.json");
        RESTMockServer.whenPOST(pathContains("/parkings")).thenReturnFile(200, "mockParkingAdded.json");
        RESTMockServer.whenGET(pathContains("/login")).thenReturnFile(200,"mockToken.json" );

        //AGGIORNATO
        RESTMockServer.whenGET(pathContains("/entities.user/mockUser")).thenReturnFile(200, "mockuser.json");
        RESTMockServer.whenGET(pathContains("/entities.ranking/mockUser")).thenReturnFile(200, "mockranking.json");
        RESTMockServer.whenGET(pathContains("/entities.car/all/mockUser")).thenReturnFile(200, "mockcarList.json");
        RESTMockServer.whenGET(pathContains("/entities.parkingslot/list")).thenReturnFile(200, "mockparkingslots.json");






        //TEST Retrieving hash key
        retrieveHashKey();
        this.bundle=new Bundle();
        locationHandler=new LocationHandler(this);

        setContentView(R.layout.activity_login);

        //GOOGLE LOGIN
        //specify what I need to retrieve
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        this.signInButton = findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgress(true);
                @SuppressLint("RestrictedApi") Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        //Facebook login
        callbackManager = CallbackManager.Factory.create();
        //Login button con test funzionamento tramite LOG, lascio il codice per poi essere facilmente adattato ai nostri usi
        loginButton = (LoginButton) findViewById(R.id.facebook_login_button);
        loginButton.setReadPermissions(Arrays.asList("email","public_profile"));
        loginButton.registerCallback(callbackManager, new FacebookLogin());

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();
        //mEmailView.setText("mock@gmail.com");
        //Toast.makeText(getApplicationContext(), "Hellooooo! Se vuoi accedere con il server mock, inserisci mock@gmail.com nella mail!!", Toast.LENGTH_LONG).show();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) { //if Enter is pressed
                    attemptLogin();
                    //intent.putExtras(bundle);
                   // startActivity(intent);
                  //  finish();

                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();

            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        notRegisteredTV = findViewById(R.id.notRegisteredTV);
        notRegisteredTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        rememberCheckBox=findViewById(R.id.remember_me);
        rememberMe=DatabaseHandler.getRememberMe(this.getApplicationContext());
        if(rememberMe!=null){
            mEmailView.setText(rememberMe.getEmail());
            if(rememberMe.isRememberme()){
                mPasswordView.setText(rememberMe.getPasswdHash());
                rememberCheckBox.setChecked(true);
                UserLoginTask userLoginTask=new UserLoginTask(rememberMe.getEmail(),rememberMe.getPasswdHash());
                userLoginTask.execute((Void)null);
            }
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check for existing Google Sign In account, if the user is already signed in
    // the GoogleSignInAccount will be non-null.
        @SuppressLint("RestrictedApi") GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if(account!=null) {

        }else{

        }
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }else if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationHandler.getLastSavedPosition();
            }else{
                locationHandler.setDefaultPosition();
                locationHandler.allowPermissionToast();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 6;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }



    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) { //GOOGLE LOGIN
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            @SuppressLint("RestrictedApi") Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            handleSignInResult(task);
        }else{ //FACEBOOK LOGIN
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }
    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            String id = account.getId();
            id = id.substring(0, Math.min(id.length(), 17));
            String email =  "goo_" + account.getEmail();
            String surname = account.getFamilyName();
            String name = account.getGivenName();

            Log.i("InfoGoogle","Email: " + email+ "\n" + "Name: "+name+"\n"+"Surname: "+surname+"\n"+"ID: "+id+"\n");
            // Signed in successfully, show authenticated UI.
            GoogleLogin gl = new GoogleLogin(id, name, surname, email);
            gl.execute((Void) null);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Toast.makeText(getApplicationContext(),R.string.google_login_error,Toast.LENGTH_LONG).show();
            showProgress(false);
        }
    }





    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> implements MyObserver<Position> {

        private final String mEmail;
        private final String mPassword;
       //0 = non lo so, -1 = errore, 1 = ok
        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }


        @Override
        protected Boolean doInBackground(Void... params) {
            UserLoginTask task=this;

                    ConnectionHandler.serverLogin(getString(R.string.url_server), mEmail, mPassword, new Callback<Token>() {
                        @Override
                        public void onResponse(Call<Token> call, Response<Token> response) {
                            if (response.isSuccessful()) {
                                token = response.body().getToken();
                                userid=response.body().getUserid();
                                Log.d("tokenPS", token);
                                locationHandler.registerObserver(task);
                                locationHandler.getLastSavedPosition();
                            } else {
                                LoginManager.getInstance().logOut();
                                //Per ora semplice toast che invia scrive errore
                                String error="";

                                try {
                                    error =response.errorBody().string();
                                } catch (IOException e) {
                                    error=getString(R.string.login_error);
                                }
                                if(error.equals("Mail errata,riprova")){
                                    myOnPostExecute(-2);
                                }
                                else myOnPostExecute(-1);
                            }
                        }

                        @Override
                        public void onFailure(Call<Token> call, Throwable t) {
                            LoginManager.getInstance().logOut();
                            Toast.makeText(getApplicationContext(), R.string.connection_failure,Toast.LENGTH_SHORT).show();
                            myOnPostExecute(-3);
                            //Avviare mockserver
                            if(mEmail.equals("mock@gmail.com")){
                                userid = "mockUser";
                                token = "mockToken";
                                myOnPostExecute(42);
                            }


                        }
                    });


         return true;
        }

        private void myOnPostExecute(int resultCode){
            mAuthTask = null;
            showProgress(false);
            Intent intent;
            switch (resultCode) {
                case 1:

                    //Rimuovo tutti i vecchi rememberme che hanno una mail diversa da quella attuale
                    DatabaseHandler.removeRememberme(getApplicationContext(),mEmail);

                    if(rememberCheckBox.isChecked()){
                        DatabaseHandler.insertRememberMe(new RememberMe(mEmail,mPassword,true,token),getApplicationContext());
                    }else{
                        DatabaseHandler.insertRememberMe(new RememberMe(mEmail,"",false,""),getApplicationContext());
                    }
                    bundle.putString("userId",userid);
                    bundle.putString("token",token);
                    intent = new Intent(getApplicationContext(), MainActivity_Drawer.class); //magari sbagliato
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                    break;
                case -1:
                    mPasswordView.setError(getString(R.string.error_incorrect_password));
                    mPasswordView.requestFocus();
                    break;
                case -2:
                    mEmailView.setError(getString(R.string.error_incorrect_email));
                    mEmailView.requestFocus();
                    break;
                case 42: //Mock case
                    Log.d("usertoken",userid+token);
                    bundle.putString("userId",userid);
                    bundle.putString("token",token);
                    bundle.putBoolean("mock",true);
                    intent = new Intent(getApplicationContext(), MainActivity_Drawer.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                    break;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            //la postexecute non fa nulla
          /*  mAuthTask = null;
            showProgress(false);

            if (success) {
                bundle.putString("userId",mEmail);
                bundle.putString("token",token);
                Intent intent = new Intent(getApplicationContext(), MainActivity_Drawer.class); //magari sbagliato
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }*/
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }

        @Override
        public void update(Position object) {
            bundle.putDouble("lat",object.getLatitude());
            bundle.putDouble("lon",object.getLongitude());
            myOnPostExecute(1);
        }
    }



    public class FacebookLogin extends AsyncTask<Void, Void, Boolean> implements FacebookCallback<LoginResult>{

        private String id;
        private String name;
        private String surname;
        private String email;


        @Override
        public void onSuccess(LoginResult loginResult) {
            //Se eseguito l'accesso, stampo userID e token relativo
            Log.i("Info&Token", "User ID: "+ loginResult.getAccessToken().getUserId() + "\n" +  "Auth Token: "+ loginResult.getAccessToken().getToken());

            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            // Application code
                            try {
                                id = object.get("id").toString();
                                name = object.get("first_name").toString();
                                surname = object.get("last_name").toString();
                                email = "fb_" + object.get("email").toString();

                                Log.i("Facebook","\nID: " + id+ "\n" +"\n"+"Email: "+email+"\n"+"Name: "+name+"\n");
                                String all=object.toString();
                                Log.d("AllFieldsReceived",all);

                                showProgress(true);

                                //call doinBackground
                                execute((Void) null);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            //Richiesta effettiva dei campi
            parameters.putString("fields", "id,first_name,last_name,email");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            Toast.makeText(getApplicationContext(), R.string.fb_login_aborted, Toast.LENGTH_LONG).show();
            showProgress(false);
        }

        @Override
        public void onError(FacebookException error) {
            Toast.makeText(getApplicationContext(), R.string.login_error, Toast.LENGTH_LONG).show();
            showProgress(false);
        }


        //executed once the connection to facebook has been performed in order to access the app's server
        @Override
        protected Boolean doInBackground(Void... voids) {
            registerLoginSocial(name, surname, email, id);
            return null;
        }
    }


    public class GoogleLogin extends AsyncTask<Void, Void, Boolean>{

        private String id;
        private String name;
        private String surname;
        private String email;

        public GoogleLogin(String id, String name, String surname, String email) {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.email = email;
        }

        //executed once the connection to facebook has been performed in order to access the app's server
        @Override
        protected Boolean doInBackground(Void... voids) {
            registerLoginSocial(name, surname, email, id);
            return null;
        }
    }


    public void registerLoginSocial(String name, String surname, String email, String id){

        ConnectionHandler.serverLogin(getString(R.string.url_server), email, id, new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.isSuccessful()) { //Succesful login!
                    token = response.body().getToken();
                    userid=response.body().getUserid();
                    bundle.putString("userId",userid);
                    bundle.putString("token",token);
                    Intent intent = new Intent(getApplicationContext(), MainActivity_Drawer.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    LoginManager.getInstance().logOut();
                    finish();
                } else {// The login has not been succesful, this means that the user must be registered for the first time
                    ConnectionHandler.registerUser(getString(R.string.url_server), name, surname, email, id, new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), R.string.registration_successful, Toast.LENGTH_LONG).show();
                                mAuthTask = new UserLoginTask(email,id);
                                mAuthTask.execute((Void) null);

                            } else {
                                LoginManager.getInstance().logOut();
                                String error="";
                                error = getString(R.string.login_error);
                                
                                Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
                                showProgress(false);

                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            Toast.makeText(getApplicationContext(),R.string.login_error,Toast.LENGTH_LONG).show();
                            LoginManager.getInstance().logOut();
                            showProgress(false);


                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Toast.makeText(getApplicationContext(),R.string.login_error,Toast.LENGTH_LONG).show();
                LoginManager.getInstance().logOut();
                showProgress(false);


            }
        });
    }




    public void retrieveHashKey(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (PackageManager.NameNotFoundException e) {

        }
        catch (NoSuchAlgorithmException e) {

        }

    }
}

