package com.risata.parkshare.model;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.risata.parkshare.pattern_observer.MyObservable;
import com.risata.parkshare.pattern_observer.messages.MyMessage;

/**
 * Created by Marco Sartini on 21/08/2018.
 */

public class Filter extends MyObservable<MyMessage> implements SharedPreferences.OnSharedPreferenceChangeListener {
    private boolean free;
    private boolean onlyRestricted;
    private boolean noRestricted;
    private Size size;
    private Car inUseCar;

    public Filter() {
        this.free = false;
        this.onlyRestricted = false;
        this.noRestricted = false;
        this.size = Size.SMALL;
        this.inUseCar = null;
    }

    public Filter(boolean free, boolean onlyRestricted, Size size) {
        this.free = free;
        this.onlyRestricted = onlyRestricted;
        this.size = size;
    }


    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public boolean isOnlyRestricted() {
        return onlyRestricted;
    }

    public void setOnlyRestricted(boolean onlyRestricted) {
        this.onlyRestricted = onlyRestricted;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public boolean isNoRestricted() {
        return noRestricted;
    }

    public void setNoRestricted(boolean noRestricted) {
        this.noRestricted = noRestricted;
    }

    public Car getInUseCar() {
        return inUseCar;
    }

    public void setInUseCar(Car inUseCar) {
        this.inUseCar = inUseCar;
    }

    public Size calculateParkingAdvicedDimension(ParkingSlot parkingSlot){
        Size sizeFilter;
        if(inUseCar != null) {

            Double pLength, pWidth, cLength, cWidth;
            pLength = parkingSlot.getLength();
            pWidth = parkingSlot.getWidth();
            cLength = inUseCar.getLength();
            cWidth = inUseCar.getWidth();

            if (pLength >= cLength && pWidth >= cWidth) {
                double ratioAreas = (inUseCar.getArea()/parkingSlot.getArea());
                if (ratioAreas < 0.5) {
                    sizeFilter = Size.LARGE;
                }
                else {
                    sizeFilter = Size.PERFECT;
                }
            } else {
                sizeFilter = Size.SMALL;
            }
        }
        else {
            sizeFilter = Size.NOT_DEFINED;
        }
        return sizeFilter;
    }

    private boolean isSizeCompliant(Size adviceSize){
        return adviceSize.compareTo(size) >= 0 ;
    }

    public boolean isSuitable(ParkingSlot parkingSlot,Size advicedSize){



        return
                (!free || !parkingSlot.isPayant())
                        && (!onlyRestricted || parkingSlot.isRestricted())
                        && (!noRestricted || !parkingSlot.isRestricted())
                        && isSizeCompliant(advicedSize);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(!key.contains("facebook"))
            initFilter(sharedPreferences);


        /*
        switch (key){
            case "pref_key_filter_only_free":
                this.free = !free;
                break;
            case "pref_key_filter_no_handy":
                this.noRestricted = !noRestricted;
                break;
            case "pref_key_filter_only_handy":
                this.onlyRestricted = !onlyRestricted;
                break;
            case "pref_key_filter_hide_small":
                if(sharedPreferences.getBoolean(key,false))
                    this.size = Size.PERFECT;
            case "pref_key_filter_only_large":
                if(sharedPreferences.getBoolean(key,false))
                    this.size = Size.LARGE;
            default:
                if(!sharedPreferences.getBoolean("pref_key_filter_only_large",false) && !sharedPreferences.getBoolean("pref_key_filter_hide_small",false))
                    this.size = Size.SMALL;
                break;
        }*/
    }

    public void initFilter (SharedPreferences sharedPreferences){
        this.onlyRestricted = sharedPreferences.getBoolean("pref_key_filter_only_handy", false);
        this.noRestricted =sharedPreferences.getBoolean("pref_key_filter_no_handy", false);
        this.free = sharedPreferences.getBoolean("pref_key_filter_only_free", false);

        this.size = Size.SMALL;
        if(sharedPreferences.getBoolean("pref_key_filter_hide_small", false)) {
            this.size = Size.PERFECT;
        }
        if(sharedPreferences.getBoolean("pref_key_filter_only_large", false)){
            this.size = Size.LARGE;
        }
    }
}

