package com.risata.parkshare.extendedGraphics;

import android.content.Context;
import android.util.AttributeSet;

import com.risata.parkshare.model.Car;


/**
 * Created by Marco Sartini on 06/04/2018.
 */

public class EditCarImageButton extends android.support.v7.widget.AppCompatImageButton {

    private Car car;

    public EditCarImageButton(Context context) {
        super(context);
    }

    public EditCarImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public EditCarImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}

