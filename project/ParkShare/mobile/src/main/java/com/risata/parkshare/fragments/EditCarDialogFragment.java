package com.risata.parkshare.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.risata.parkshare.R;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.pattern_observer.messages.AddCarChange;
import com.risata.parkshare.pattern_observer.messages.EditCarChange;

/**
 * Created by Marco Sartini on 10/04/2018.
 */

public class EditCarDialogFragment extends DialogFragment {

    private Spinner categorySpinner;
    private TextView manufacturerTV;
    private TextView modelTV;
    private TextView widthTV;
    private TextView lengthTV;
    private boolean cancel;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction

        View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_add_car, null);
        this.categorySpinner = (Spinner) v.findViewById(R.id.new_car_category);
        this.manufacturerTV = (TextView) v.findViewById(R.id.new_car_manufacturer);
        this.modelTV = (TextView) v.findViewById(R.id.new_car_model);
        this.lengthTV = (TextView) v.findViewById(R.id.new_car_length);
        this.widthTV = (TextView) v.findViewById(R.id.new_car_width);

        this.manufacturerTV.setText(getArguments().getCharSequence("manufacturer"));
        this.modelTV.setText(getArguments().getCharSequence("model"));
        this.lengthTV.setText(Double.toString(getArguments().getDouble("length")));
        this.widthTV.setText(Double.toString(getArguments().getDouble("width")));


        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getContext(), R.array.car_categories_list, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(spinnerAdapter);
        categorySpinner.setSelection(spinnerAdapter.getPosition(getArguments().getCharSequence("category")));

        AlertDialog alertDialog=new AlertDialog.Builder(getContext())
            .setView(v)
            .setTitle(R.string.edit_car_title)
            .setPositiveButton(R.string.edit_yes, null)
            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick (DialogInterface dialog,int id){
                    dialog.cancel();
                }
            })
            // Create the AlertDialog object and return it
            .create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        analyzeInsert(alertDialog);
                    }
                });
            }
        });

        return alertDialog;
    }

    private void analyzeInsert(AlertDialog alertDialog) {
        String manu = manufacturerTV.getText().toString();
        String mod = modelTV.getText().toString();
        String wstring = widthTV.getText().toString();
        String lstring = lengthTV.getText().toString();
        double width = 0;
        double length = 0;
        cancel = false;
        View focusView = null;
        if (manu.isEmpty()) {
            manufacturerTV.setError(getString(R.string.error_field_required));
            focusView = manufacturerTV;
            cancel = true;
        } else if (mod.isEmpty()) {
            modelTV.setError(getString(R.string.error_field_required));
            focusView = modelTV;
            cancel = true;
        }else if (lstring.isEmpty()) {
            lengthTV.setError(getString(R.string.error_field_required));
            focusView = lengthTV;
            cancel = true;
        } else if (wstring.isEmpty()) {
            widthTV.setError(getString(R.string.error_field_required));
            focusView = widthTV;
            cancel = true;
        }


        if (cancel) {
            focusView.requestFocus();
        } else {
            width = Double.parseDouble(wstring);
            length = Double.parseDouble(lstring);
            EditCarChange change = new EditCarChange(new Car(
                    getArguments().getString("userid"),
                    manu, mod, width, length,
                    categorySpinner.getSelectedItem().toString(),
                    getArguments().getBoolean("isfavourite"),
                    getArguments().getString("carId")));
            ((UserProfileFragment) getTargetFragment()).notifyObserver(change);
            alertDialog.dismiss();

        }
    }
}
