package com.risata.parkshare.pattern_observer.messages;

import com.risata.parkshare.presenters.MainPresenter;

/**
 * Created by Marco Sartini on 21/08/2018.
 */

public class FilterNoRestrictedMessage extends FilterMessage {
    boolean noRestricted;

    public FilterNoRestrictedMessage(boolean noRestricted) {
        this.noRestricted = noRestricted;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        mainPresenter.getModel().getFilter().setNoRestricted(noRestricted);
        mainPresenter.getModel().getFilter().setOnlyRestricted(false);
        super.perform(mainPresenter);
    }
}
