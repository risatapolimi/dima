package com.risata.parkshare.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;
import com.risata.parkshare.R;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotItem;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.model.Size;
import com.risata.parkshare.pattern_observer.MyObserver;
import com.risata.parkshare.pattern_observer.messages.MyMessage;

import java.util.ArrayList;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.risata.parkshare.R.drawable.border_card_selected;

public class ParkingSlotAdapter extends RecyclerView.Adapter<ParkingSlotAdapter.MyViewHolder> implements MyObserver<MyMessage>{

    private Context mContext;
    private List<ParkingSlotItem> parkingSlotList;
    public int parkingSelected;
    public MapHomeFragment mapHomeFragment;
    private Map<Integer,View > parkingSlotViewMap;
    private Map<Integer,Integer> idToPositionMap;
    private Car inUseCar;
    public static int positionToHack = -1;

    public ParkingSlotAdapter(Context context, Car car) {
        this.mContext=context;
        parkingSelected=-1;
        inUseCar = car;
    }

    public ParkingSlotAdapter(Context mContext, List<ParkingSlotItem> parkingSlotList, int oldSelectedParking, MapHomeFragment fragment) {
        this.mContext = mContext;
        this.parkingSlotList = parkingSlotList;
        this.parkingSelected=oldSelectedParking;
        this.mapHomeFragment=fragment;
        this.parkingSlotViewMap = new HashMap<>();
        this.idToPositionMap = new HashMap<>();
        for(ParkingSlotItem p:parkingSlotList){
            idToPositionMap.put(p.getParkingSlot().getId(),p.getIndex());
        }
        inUseCar = null;
    }

    public ParkingSlotAdapter(Context mContext, List<ParkingSlotItem> parkingSlotList, int oldSelectedParking, MapHomeFragment fragment, Car car) {
        this.mContext = mContext;
        this.parkingSlotList = parkingSlotList;
        this.parkingSelected=oldSelectedParking;
        this.mapHomeFragment=fragment;
        this.parkingSlotViewMap = new HashMap<>();
        inUseCar = car;
    }

    public void refreshParkingSlotCards(List<ParkingSlotItem> slotsToAdd, Position userPosition) {
        boolean alreadyThere = false; //already in the slotstoadd
        List<ParkingSlotItem> toBeDeleted = new ArrayList<>();
        for(ParkingSlotItem p : this.parkingSlotList){
            p.setDistance(p.getParkingSlot().getPosition().differenceKm(userPosition));
            for(ParkingSlotItem pnew: slotsToAdd){
                if (!p.equals(pnew)){
                    alreadyThere = alreadyThere || false;

                }else{
                    alreadyThere = alreadyThere || true;
                }
            }
            if(!alreadyThere){
                toBeDeleted.add(p);
            }
        }

        this.parkingSlotList.removeAll(toBeDeleted);
        slotsToAdd.removeAll(this.parkingSlotList);
        this.parkingSlotList.addAll(slotsToAdd);

        notifyDataSetChanged();


        /*
        // NON FUNZIONA :( parkingSlots.removeAll(this.parkingSlotList);
        boolean found = false;
        for (ParkingSlot p: slotsToAdd){
            for(ParkingSlot thisp: this.parkingSlotList){
                if (thisp.equals(p)){
                    found=true;
                }
            }
            if (!found){
                this.parkingSlotList.add(p);
                found=false;
            }
        }
        //this.parkingSlotList.addAll(parkingSlots);
        notifyDataSetChanged();
*/
    }

    @Override
    public void update(MyMessage message) {
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView parkingNumber, parkingAddress, parkingSize;
        public ImageView titleIcon, parkingPayant, parkingHandy, parkingPayantStroke;
        public ConstraintLayout cardLayout;
        public CardView cardView;
        public View view;


        public MyViewHolder(View view) {
            super(view);
            this.view = view;
            parkingNumber = (TextView) view.findViewById(R.id.parkingNumber);
            parkingAddress = (TextView) view.findViewById(R.id.parkingAddress);
            parkingSize = (TextView) view.findViewById(R.id.parkingSize);

            titleIcon = (ImageView) view.findViewById(R.id.cardIconParking);
            parkingPayant = (ImageView) view.findViewById(R.id.parkingPayant);
            parkingPayantStroke = (ImageView) view.findViewById(R.id.parkingPayantStroke);
            parkingHandy = (ImageView) view.findViewById(R.id.parkingHandy);

            cardLayout=(ConstraintLayout)view.findViewById(R.id.cardview_layout);
            cardView = (CardView) view.findViewById(R.id.cardView);

        }


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.parking_slot_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ParkingSlotItem parkingSlotItem = parkingSlotList.get(position);
        ParkingSlot parkingSlot = parkingSlotItem.getParkingSlot();
        parkingSlot.registerObserver(this);
        parkingSlotViewMap.put(parkingSlotItem.getIndex(), holder.view); //getId() prima di getIndex()

        holder.parkingNumber.setText(parkingSlotItem.getPrintableDistance());
        holder.parkingAddress.setText(parkingSlot.getAddress());

        String advicedDimension = "";

            switch (parkingSlotItem.getAdvicedSize()){
                case SMALL:
                    advicedDimension = mContext.getString(R.string.parking_dimension_small);
                    break;
                case PERFECT:
                    advicedDimension = mContext.getString(R.string.parking_dimension_perfect);
                    break;
                case LARGE:
                    advicedDimension = mContext.getString(R.string.parking_dimension_large);
                    break;
                case NOT_DEFINED:
                    advicedDimension = mContext.getString(R.string.parking_dimension_not_defined);
                    break;

             }

        holder.parkingSize.setText(advicedDimension);

        if(parkingSlot.isPayant()){
            holder.parkingPayantStroke.setVisibility(View.INVISIBLE);
            holder.parkingPayant.setAlpha((float)1);
            holder.parkingPayant.setContentDescription(mContext.getString(R.string.contentDesc_park_payant));
        }
        else{
            holder.parkingPayant.setAlpha((float)0.2);
            holder.parkingPayant.setContentDescription(mContext.getString(R.string.contentDesc_park_free));
            holder.parkingPayantStroke.setVisibility(View.VISIBLE);
            //holder.parkingFree.setImageResource(R.drawable.ic_free_symbol); //solo se necessario, altrimenti è sottinteso default
        }

        if(parkingSlot.isRestricted()) {
            holder.parkingHandy.setAlpha((float)1); //setImageResource(R.drawable.ic_accessible_black_24dp);
            holder.parkingHandy.setContentDescription(mContext.getString(R.string.contentDesc_park_handy));
            holder.titleIcon.setImageResource(R.drawable.ic_dot_parking_handy);
            holder.titleIcon.setContentDescription(mContext.getString(R.string.contentDesc_park_handy));

        }
        else {
            holder.parkingHandy.setAlpha((float)0.2);
            holder.titleIcon.setImageResource(R.drawable.ic_dot_parking_blue);
            holder.parkingHandy.setContentDescription(mContext.getString(R.string.contentDesc_park_no_handy));
            holder.titleIcon.setContentDescription(mContext.getString(R.string.contentDesc_park_no_handy));
        }

        holder.itemView.setSelected(parkingSelected == position);
        holder.itemView.setSelected(parkingSlotItem.isToBeColored());


        holder.cardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Marker marker=mapHomeFragment.checkAlreadyPresent(mapHomeFragment.getMarkerList().iterator(),parkingSlot);
                if(marker!=null){
                    mapHomeFragment.onMarkerClick(marker);
                    marker.showInfoWindow();
                    mapHomeFragment.focusMap(marker.getPosition());
                }

            }
        });

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position, List<Object> payload){
        if(!payload.isEmpty()) {
            if (payload.get(0).equals("ChangeSelection")) {
                this.parkingSelected=position;
                drawBorder(holder,true);

            }else if(payload.get(0).equals("ResetSelection")){
                drawBorder(holder,false);
            }
        }
        else {
            this.onBindViewHolder(holder,position);
            drawBorder(holder,positionToHack == position );

        }
    }

    public void drawBorder(MyViewHolder holder, boolean selected){
            holder.itemView.setSelected(selected);
    }


    @Override
    public int getItemCount() {
        return parkingSlotList.size();
    }

    public void setParkingSlotList(List<ParkingSlotItem> parkingSlotList) {
        this.parkingSlotList = parkingSlotList;
    }

    public List<ParkingSlotItem> getParkingSlotList() {
        return parkingSlotList;
    }

    public void setParkingSelected(int parkingSelected) {
        this.parkingSelected = parkingSelected;
    }

    public Map<Integer, View> getParkingSlotViewMap() {
        return parkingSlotViewMap;
    }

    public Car getInUseCar() {
        return inUseCar;
    }

    public void setInUseCar(Car inUseCar) {
        this.inUseCar = inUseCar;
    }

    public Map<Integer, Integer> getIdToPositionMap() {
        return idToPositionMap;
    }
}
