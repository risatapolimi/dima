package com.risata.parkshare.pattern_observer.messages;

import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.model.Size;

/**
 * Created by Marco Sartini on 21/08/2018.
 */

public class FilterSizeMessage extends FilterMessage {
    Size size;

    public FilterSizeMessage(Size size) {
        this.size = size;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        mainPresenter.getModel().getFilter().setSize(size);
        super.perform(mainPresenter);
    }
}
