package com.risata.parkshare.model;

import java.util.List;

/**
 * Created by Daniele on 08/03/2018.
 */

public class Map {
    //Da riguardare bene
    private List<ParkingSlot> parkingList;
    private Position myPosition;
    private int amplitude;
    //private User user


    public Map(List<ParkingSlot> parkingList, Position myPosition, int amplitude) {
        this.parkingList = parkingList;
        this.myPosition = myPosition;
        this.amplitude = amplitude;
    }

    public List<ParkingSlot> getParkingList() {
        return parkingList;
    }

    public void setParkingList(List<ParkingSlot> parkingList) {
        this.parkingList = parkingList;
    }

    public Position getMyPosition() {
        return myPosition;
    }

    public void setMyPosition(Position myPosition) {
        this.myPosition = myPosition;
    }

    public int getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(int amplitude) {
        this.amplitude = amplitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Map)) return false;

        Map map = (Map) o;

        if (getAmplitude() != map.getAmplitude()) return false;
        if (!getParkingList().equals(map.getParkingList())) return false;
        return getMyPosition().equals(map.getMyPosition());
    }

    @Override
    public int hashCode() {
        int result = getParkingList().hashCode();
        result = 31 * result + getMyPosition().hashCode();
        result = 31 * result + getAmplitude();
        return result;
    }
}
