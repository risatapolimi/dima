package com.risata.parkshare.model;

import android.location.Location;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ParkingSlotPOJO {

    @SerializedName("width")
    @Expose
    private double width;
    @SerializedName("length")
    @Expose
    private double length;
    @SerializedName("isavailable")
    @Expose
    private boolean isAvailable;
    @SerializedName("ispayant")
    @Expose
    private boolean isPayant;
    @SerializedName("isrestricted")
    @Expose
    private boolean isRestricted;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("longitude")
    @Expose
    private double longitude;
    @SerializedName("advicedDimension")
    @Expose
    private int advicedDimension;


    public ParkingSlotPOJO(double width, double length, double latitude, double longitude, boolean isAvailable, boolean isPayant, boolean isRestricted, int id) {
        this.width = width;
        this.length = length;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isAvailable = isAvailable;
        this.isPayant = isPayant;
        this.isRestricted = isRestricted;
        this.id = id;
    }
    
    public ParkingSlotPOJO(Size advicedDimension, boolean isPayant, boolean isRestricted){
        switch (advicedDimension){
            case SMALL:
                this.advicedDimension = 0;
                break;
            case PERFECT:
                this.advicedDimension = 1;
                break;
            case LARGE:
                this.advicedDimension = 2;
                break;
            default:
                this.advicedDimension = -1;
                break;
        }
        this.isPayant = isPayant;
        this.isRestricted = isRestricted;
    }

    public ParkingSlotPOJO(double width, double length, double latitude, double longitude, boolean isAvailable, boolean isPayant, boolean isRestricted, int id,String addr) {
        this.width = width;
        this.length = length;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isAvailable = isAvailable;
        this.isPayant = isPayant;
        this.isRestricted = isRestricted;
        this.id = id;
        address=addr;
    }

    public ParkingSlot convertToParkingSlot(){
        Location loc = new Location("");
        loc.setLatitude(latitude);
        loc.setLongitude(longitude);

        return new ParkingSlot(width,length,new Position(loc), isAvailable,isPayant,isRestricted, id,address);
    }

    public static List<ParkingSlot> convertList(List<ParkingSlotPOJO> list){
        List<ParkingSlot> parkingSlots=new ArrayList<>();

        for(ParkingSlotPOJO p: list){
            parkingSlots.add(p.convertToParkingSlot());
        }

        return parkingSlots;
    }

    public static List<ParkingSlot> convertListPSItem(List<ParkingSlotPOJO> list){
        List<ParkingSlot> parkingSlots=new ArrayList<>();

        for(ParkingSlotPOJO p: list){
            parkingSlots.add(p.convertToParkingSlot());
        }

        return parkingSlots;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
