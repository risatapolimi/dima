package com.risata.parkshare.pattern_observer.messages;


import com.risata.parkshare.adapters.ParkingSlotAdapter;
import com.risata.parkshare.model.Filter;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.presenters.MainPresenter;

import java.util.ArrayList;

/**
 * Created by Marco Sartini on 21/08/2018.
 */

public class FilterMessage implements MyMessage {

    public FilterMessage() {
            }

    @Override
    public void perform(MainPresenter mainPresenter) {
        Model model = mainPresenter.getModel();
        ParkingSlotAdapter adapter=mainPresenter.getMapHomePresenter().getMapHomeFragment().getParkingSlotAdapter();

        try{
            UpdateParking.setUpParking(mainPresenter.getMapHomePresenter().getMapHomeFragment().getParkingSlotAdapter(),model.getUserLocation(),model.getParkingSlotList(),mainPresenter.getMapHomePresenter().getMapHomeFragment(),new ArrayList<>(),model.getUser().getMyRanking(),model.getFilter());
        }catch (Exception exp){
            model.setParkingSlotList(new ArrayList<>());
            adapter.setParkingSelected(0);
            adapter.setParkingSlotList(new ArrayList<>());
            adapter.getIdToPositionMap().clear();
            adapter.getParkingSlotViewMap().clear();
        }

    }
}
