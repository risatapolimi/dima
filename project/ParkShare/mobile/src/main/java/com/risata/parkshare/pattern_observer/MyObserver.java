package com.risata.parkshare.pattern_observer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Daniele on 16/03/2018.
 */

public interface MyObserver<T> {

    public void update(T object);

}
