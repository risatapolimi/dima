package com.risata.parkshare.webservice;

import android.arch.lifecycle.LiveData;

import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.model.Token;
import com.risata.parkshare.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Marco Sartini on 19/04/2018.
 */

public interface RESTInterface {
    /**
     * @GET declares an HTTP GET request
     * @Path("user") annotation on the userId parameter marks it as a
     * replacement for the {user} placeholder in the @GET path
     */
  /* @GET("/users/{user}")
    Call<User> getUser(@Path("user") String userId);
*/

    @GET("users/{login}")
    LiveData<ApiResponse<User>> getUserLive(@Path("login") String login);

    @GET("entities.user/{userid}")
    Call<User> getUser(@Path("userid") String userId);

    @GET("entities.user/login/{userId}/{pwdHash}")
    Call<Token> getToken(@Path("userId") String userId, @Path("pwdHash") String pwdHash);

    @PUT("users/{login}")
    boolean putUser(@Path("login")String login, @Body User user);


    @PUT("entities.car/ ;carid={carid};userid={userid}") //oppure "users/{login}/cars/{carId}"
    Call<Car> putEditedCar(@Header("Authorization") String tokenAuth, @Path("userid")String userId, @Path("carid")String carId, @Body Car car);

    @DELETE("entities.car/ ;carid={carid};userid={userid}")
    Call<Car> deleteCar(@Header("Authorization") String tokenAuth, @Path("userid")String userId, @Path("carid")String carId);

    @POST("entities.car")
    Call<Car> postCar(@Header("Authorization") String tokenAuth, @Body Car car);

    @POST("entities.parkingslot")
    Call<ParkingSlotPOJO> postSlot(@Header("Authorization") String tokenAuth, @Body ParkingSlotPOJO newSlot);

    @GET("entities.parkingslot/list/{lat}/{lon}")
    Call<List<ParkingSlotPOJO>> getParkingSlots(@Header("Authorization") String tokenAuth, @Path("lat")String lat,@Path("lon")String lon);

    @GET("entities.parkingslot/all")
    Call<List<ParkingSlotPOJO>> getAllParkingSlots(@Header("Authorization") String tokenAuth);

    @DELETE("entities.parkingslot/{id}")
    Call<ParkingSlotPOJO> deleteParking(@Header("Authorization") String tokenAuth, @Path("id")String id);

    @GET("entities.ranking/{userid}")
    Call<Ranking> getRanking(@Path("userid") String userid);

    @GET("entities.car/all/{userid}")
    Call<List<Car>> getCarList(@Path("userid") String userid);

    @POST("entities.user/registration")
    Call<User> postUser(@Body User newUser);

    @PUT("entities.parkingslot/{id}")
    Call<ParkingSlotPOJO> putParking(@Header("Authorization") String tokenAuth, @Path("id")String id, @Body ParkingSlotPOJO park);

    @PUT("entities.ranking/{id}")
    Call<Ranking> addRankingPoint(@Path("id")String id);
}
