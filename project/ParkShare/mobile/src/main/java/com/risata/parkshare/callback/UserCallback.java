package com.risata.parkshare.callback;

import com.risata.parkshare.model.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserCallback implements Callback<User> {

    private User user;
    private static boolean endConnection=false;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        if (response.isSuccessful()) {
            User temp=response.body();
            temp.setCarList(new ArrayList<>());
            this.setUser(temp);
            System.out.println("Utente ricevuto correttamente");
            /*user=new User(temp.getName(),temp.getSurname(),temp.getEmail(),temp.getPwdHash());
            user.setUserId(temp.getUserId());
            user.setMyRanking(temp.getMyRanking());*/
            endConnection=true;
        }
        else{
            user=null;
            System.out.println("Utente ricevuto non correttamente,errore");
        }
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        System.out.println("URisposta completamente errata,errore");
    }

    public static boolean isEndConnection() {
        return endConnection;
    }
}
