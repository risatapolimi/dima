package com.risata.parkshare.pattern_observer.messages;

import android.widget.Toast;

import com.risata.parkshare.R;
import com.risata.parkshare.adapters.CarArrayAdapter;
import com.risata.parkshare.fragments.UserProfileFragment;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.presenters.MainPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Marco Sartini on 06/04/2018.
 */

public class AddCarChange implements MyMessage {
    private Car newCar;

    public AddCarChange(Car newCar) {
        this.newCar = newCar;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        UserProfileFragment userProfileFragment = mainPresenter.getUserProfilePresenter().getUserProfileFragment();
        Model model= mainPresenter.getModel();
        this.newCar.setUserId(model.getUser().getUserId());
        mainPresenter.getConnectionHandler().addNewCar(newCar, new Callback<Car>(){

            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                if(response.isSuccessful()) {
                    Car car=response.body();
                    if(car.isFavorite()){
                        userProfileFragment.notifyObserver(new FavoriteChange(car));
                    }
                    mainPresenter.getDbHandler().addNewCar(car);
                    model.getUser().setCarList(mainPresenter.getDbHandler().getCarList());
                    updateFragment(userProfileFragment);
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(userProfileFragment.getContext(), R.string.connection_failure,Toast.LENGTH_SHORT).show();
            }
        });


    }


    private void updateFragment(UserProfileFragment fragment) {
        ((CarArrayAdapter) fragment.getCarsLV().getAdapter()).notifyDataSetChanged();
    }
}
