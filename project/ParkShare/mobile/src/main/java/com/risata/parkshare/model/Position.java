package com.risata.parkshare.model;

import android.location.Location;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.presenters.MapHomePresenter;

import java.util.List;

/**
 * Created by Daniele on 08/03/2018.
 */

public class Position extends Location implements MyMessage{

    public Position(String provider) {
        super(provider);
    }

    public Position(Location l) {
        super(l);
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        Model model = mainPresenter.getModel();
        model.setUserLocation(this);
        MapHomeFragment mapHomeFragment= mainPresenter.getMapHomePresenter().getMapHomeFragment();
        if(!mapHomeFragment.isLookingAround()){
            if(!mapHomeFragment.isSearching()){
                mapHomeFragment.focusMap(new LatLng(this.getLatitude(),this.getLongitude()));
            }else{
                mapHomeFragment.focusMap(new LatLng(model.getSearchLocation().getLatitude(),model.getSearchLocation().getLongitude()));
            }
        }


    }


    public double differenceKm(Position pos2){
        double pigreco = 3.1415927;
        double lat1= pigreco * this.getLatitude() /180;
        double long1=pigreco * this.getLongitude() /180;
        double lat2= pigreco * pos2.getLatitude() /180;
        double long2=pigreco * pos2.getLongitude() /180;
        double deltalat=lat1-lat2;
        double deltalong=long1-long2;
        double distRadianti=Math.asin(
                (
                        Math.sqrt(
                                Math.pow(Math.cos(lat2)*Math.sin(deltalong),2)
                                + Math.pow(Math.cos(lat1)*Math.sin(lat2)-Math.sin(lat1)*Math.cos(lat2)*Math.cos(deltalong),2)
                        )
                )
                        /
                        (Math.sin(lat1)*Math.sin(lat2)+Math.cos(lat1)*Math.cos(lat2)*Math.cos(deltalong))
        );

                //Formula 1
                /*Math.acos(Math.sin(lat1)*Math.sin(lat2)+Math.cos(lat1)*Math.cos(lat2)*Math.cos(deltalong));*/

                //Formula 2
                /*2*Math.asin(Math.sqrt(
                Math.pow(Math.sin(deltalat/2),2)+
                Math.cos(lat1)*Math.cos(lat2)*Math.pow(Math.sin(deltalong/2),2)));*/


        return distRadianti*6371;
    }

    public ParkingSlot nearestParking(List<ParkingSlot> parkingSlotList,Filter filter,double maxDistance,Position dest){
        ParkingSlot nearest=null;
        if(parkingSlotList!=null ){
            if(parkingSlotList.size()>0){
                double minDistance=maxDistance;
                double distance;
                Size size;
                for(ParkingSlot p:parkingSlotList){
                    distance=this.differenceKm(p.getPosition());
                    size=filter.calculateParkingAdvicedDimension(p);
                    Log.i("Redirecting",""+p.getAddress()+" distance: "+distance);
                    if(distance<minDistance && !p.hasSameLocation(new LatLng(dest.getLatitude(),dest.getLongitude())) && filter.isSuitable(p,size)){
                        Log.i("Redirecting","I'm the nearest");
                        minDistance=distance;
                        nearest=p;
                    }
                }
            }
        }
        return nearest;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
