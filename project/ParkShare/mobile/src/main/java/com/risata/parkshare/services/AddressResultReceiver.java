package com.risata.parkshare.services;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.pattern_observer.ObservableReceiver;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.pattern_observer.messages.ParkingWithAddress;

public class AddressResultReceiver extends ObservableReceiver<MyMessage> {

    private ParkingSlotPOJO parkingSlot;

    public AddressResultReceiver(Handler handler, ParkingSlotPOJO p) {
        super(handler);
        this.parkingSlot=p;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        if (resultData == null) {
            return;
        }

        // Display the address string
        // or an error message sent from the intent service.
        String mAddressOutput = resultData.getString(ServiceConstants.RESULT_DATA_KEY);
        Log.d("addressesPS", mAddressOutput);
        if (mAddressOutput == null) {
            mAddressOutput = "";
        }
        this.parkingSlot.setAddress(mAddressOutput);
        this.notifyObserver(new ParkingWithAddress(parkingSlot));

        // Show a toast message if an address was found.
        /*if (resultCode == ServiceConstants.SUCCESS_RESULT) {
            Toast.makeText(context, R.string.address_found,Toast.LENGTH_SHORT).show();
        }*/

    }
}
