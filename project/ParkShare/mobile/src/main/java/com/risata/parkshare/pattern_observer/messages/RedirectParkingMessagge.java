package com.risata.parkshare.pattern_observer.messages;


import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.widget.Toast;

import com.risata.parkshare.R;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.services.ParkingService;
import com.risata.parkshare.services.ServiceConstants;
import com.risata.parkshare.views.LoginActivity;
import com.risata.parkshare.views.MainActivity_Drawer;

public class RedirectParkingMessagge implements MyMessage {

    private Position destination;

    public RedirectParkingMessagge(Position destination) {
        this.destination=destination;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        Position position=mainPresenter.getModel().getUserLocation();
        if(position==null)
            position=destination;
        ParkingSlot newParkDestination=position.nearestParking(mainPresenter.getModel().getParkingSlotList(),mainPresenter.getModel().getFilter(),
                            mainPresenter.getModel().getUser().getMyRanking().getRadiusMeters()*0.001,destination);
        if(newParkDestination!=null){
            if(newParkDestination.isInternalRanking(mainPresenter.getModel().getUserLocation(),mainPresenter.getModel().getUser().getMyRanking())){
                Position newDestination=newParkDestination.getPosition();
                double lat=newDestination.getLatitude();
                double lon=newDestination.getLongitude();
                mainPresenter.getMapHomePresenter().redirectMaps(lat,lon,mainPresenter);
            }
            else{
                this.noParkingAvailable(mainPresenter);
            }
        }
        else {
                this.noParkingAvailable(mainPresenter);
        }
    }

    private void noParkingAvailable(MainPresenter mainPresenter){
        Context context=mainPresenter.getMapHomePresenter().getMapHomeFragment().getContext();
        Toast.makeText(context, R.string.no_parkings_near,
                Toast.LENGTH_LONG).show();
        ServiceConstants.FINISH_SERVICE=true;
        context.stopService(new Intent(context,ParkingService.class));
        mainPresenter.getMapHomePresenter().restartApplication(mainPresenter.getModel().getUser().getUserId(),
                mainPresenter.getConnectionHandler().getTokenAuth(),mainPresenter.getModel());

    }
}
