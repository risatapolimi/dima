package com.risata.parkshare.pattern_observer.messages;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.risata.parkshare.R;
import com.risata.parkshare.adapters.CarArrayAdapter;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.ParkingSignal;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotItem;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.presenters.MainPresenter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Marco Sartini on 06/04/2018.
 */

public class SignalParkMsg implements MyMessage {
    private ParkingSlotPOJO newSlot;

    public SignalParkMsg(ParkingSlotPOJO newSlot) {
        this.newSlot = newSlot;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        MapHomeFragment mapHomeFragment = mainPresenter.getMapHomePresenter().getMapHomeFragment();
        Model model = mainPresenter.getModel();
        Car car=model.getUser().getInUseCar();
        if(car!=null){
            newSlot.setWidth(car.getWidth());
            newSlot.setLength(car.getLength());
        }
        Position position = model.getUserLocation();

        //se esiste la posizione dell'utente fa tutto, altrimenti no
        if (position != null) {

            newSlot.setLongitude(position.getLongitude());
            newSlot.setLatitude(position.getLatitude());
            mainPresenter.getMapHomePresenter().assignParkingAddresses(position,newSlot, mapHomeFragment.getContext(),mainPresenter);
            Toast.makeText(mapHomeFragment.getContext(), R.string.parking_adding, Toast.LENGTH_SHORT).show();

        }
        else{
            Toast.makeText(mapHomeFragment.getContext(), R.string.unavailable_position, Toast.LENGTH_SHORT).show();
        }
    }

}
