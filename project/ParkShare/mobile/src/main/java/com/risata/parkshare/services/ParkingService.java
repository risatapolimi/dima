package com.risata.parkshare.services;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import com.risata.parkshare.model.Position;


public class ParkingService extends IntentService{

    private ResultReceiver mapsReceiver;

    public ParkingService() {
        super("Keep Track of parking");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        mapsReceiver=intent.getParcelableExtra(ServiceConstants.MAPS_RECEIVER);
        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {
                Bundle bundle;
                bundle=new Bundle();
                while(!ServiceConstants.FINISH_SERVICE){
                    try {
                        Thread.sleep(1000);
                        mapsReceiver.send(ServiceConstants.MAPS_SERVICE_RESULT_CODE,bundle);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                stopSelf();
            }
        });
        t.start();


    }



}
