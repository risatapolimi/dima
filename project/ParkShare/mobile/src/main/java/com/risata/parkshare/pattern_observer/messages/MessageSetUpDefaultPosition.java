package com.risata.parkshare.pattern_observer.messages;

import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.presenters.MapHomePresenter;

/**
 * Created by tavec on 01/04/2018.
 */

public class MessageSetUpDefaultPosition implements MyMessage {

    @Override
    public void perform(MainPresenter mainPresenter) {
        MapHomePresenter mapHomePresenter = mainPresenter.getMapHomePresenter();
        mapHomePresenter.getLocationHandler().setDefaultPosition();
        mapHomePresenter.getLocationHandler().allowPermissionToast();
    }
}
