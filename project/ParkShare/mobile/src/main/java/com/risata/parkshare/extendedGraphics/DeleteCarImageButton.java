package com.risata.parkshare.extendedGraphics;

import android.content.Context;
import android.util.AttributeSet;

import com.risata.parkshare.model.Car;


/**
 * Created by Marco Sartini on 06/04/2018.
 */

public class DeleteCarImageButton extends android.support.v7.widget.AppCompatImageButton {

    private Car car;

    public DeleteCarImageButton(Context context) {
        super(context);
    }

    public DeleteCarImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public DeleteCarImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}

