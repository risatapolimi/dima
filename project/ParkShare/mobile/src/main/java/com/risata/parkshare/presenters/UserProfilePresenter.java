package com.risata.parkshare.presenters;

import android.os.Parcel;
import android.util.Log;

import com.risata.parkshare.fragments.UserProfileFragment;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.User;
import com.risata.parkshare.pattern_observer.messages.FavoriteChange;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.pattern_observer.MyObserver;
import com.risata.parkshare.pattern_observer.messages.ReadyProfileFragment;

import java.util.Comparator;

/**
 * Created by Marco Sartini il bel bambino e TeamViewer assisted  on 31/03/2018.
 */

public class UserProfilePresenter  {

    private UserProfileFragment userProfileFragment;

    public UserProfilePresenter(UserProfileFragment userProfileFragment) {
        this.userProfileFragment = userProfileFragment;
    }


    class CarComparator implements Comparator<Car> {

        @Override
        public int compare(Car o1, Car o2) {
            return o1.compareTo(o2);
        }
    }

    public UserProfileFragment getUserProfileFragment() {
        return userProfileFragment;
    }

}
