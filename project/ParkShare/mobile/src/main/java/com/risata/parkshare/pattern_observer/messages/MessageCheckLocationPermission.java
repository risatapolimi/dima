package com.risata.parkshare.pattern_observer.messages;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.fragments.UserProfileFragment;
import com.risata.parkshare.handlers.LocationHandler;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.presenters.MapHomePresenter;

/**
 * Created by tavec on 01/04/2018.
 */

public class MessageCheckLocationPermission implements MyMessage {

    @Override
    public void perform(MainPresenter mainPresenter) {
        MapHomeFragment mapHomeFragment = mainPresenter.getMapHomePresenter().getMapHomeFragment();
        LocationHandler locationHandler = mainPresenter.getMapHomePresenter().getLocationHandler();
        if(locationHandler.checkLocationPermissions(mapHomeFragment)){
            Position position = mainPresenter.getModel().getUserLocation();
            locationHandler.setUpUpdate();
            if (position != null){
                mapHomeFragment.setUpMapReady(position);
            }

        }






    }
}
