package com.risata.parkshare.model;

/**
 * Created by Daniele on 08/03/2018.
 */

public class Trip {

    private Position destination;
    private User user;
    private double distanceAccepted;

    public Trip(Position destination, User user, double distanceAccepted) {
        this.destination = destination;
        this.user = user;
        this.distanceAccepted = distanceAccepted;
    }

    public Position getDestination() {
        return destination;
    }

    public void setDestination(Position destination) {
        this.destination = destination;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getDistanceAccepted() {
        return distanceAccepted;
    }

    public void setDistanceAccepted(double distanceAccepted) {
        this.distanceAccepted = distanceAccepted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trip)) return false;

        Trip trip = (Trip) o;

        if (Double.compare(trip.getDistanceAccepted(), getDistanceAccepted()) != 0) return false;
        if (!getDestination().equals(trip.getDestination())) return false;
        return getUser().equals(trip.getUser());
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getDestination().hashCode();
        result = 31 * result + getUser().hashCode();
        temp = Double.doubleToLongBits(getDistanceAccepted());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
