package com.risata.parkshare.repository;

import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.util.StringUtil;
import android.util.Log;

import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Ranking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Marco Sartini on 21/04/2018.
 */

public class ParkShareTypeConverter {
    @TypeConverter
    public static List<Car> stringToCarList(String data) {
        if (data == null || data.length() == 0) {
            return Collections.emptyList();
        }

        List<Car> result = new ArrayList<>();
        data=data.substring(0,data.length()-1);
        StringTokenizer stCar = new StringTokenizer(data, "#");

        while(stCar.hasMoreTokens()){
            StringTokenizer stCarCar = new StringTokenizer(stCar.nextToken(), "/");
            result.add(new Car(stCarCar.nextToken(), stCarCar.nextToken(), stCarCar.nextToken(), Double.parseDouble(stCarCar.nextToken()), Double.parseDouble(stCarCar.nextToken()), stCarCar.nextToken(), Boolean.parseBoolean(stCarCar.nextToken()), stCarCar.nextToken()));
        }

        return result;
    }

    @TypeConverter
    public static String carListToString(List<Car> cars) {
        String result = "";
        if(cars==null)
            return result;

        for(Car c: cars){
            result += c.getUserId() + "/" + c.getManufacturer() + "/" + c.getModel() + "/" + c.getLength() + "/" + c.getWidth() + "/" +c.getCategory() + "/" + (c.isFavorite() ? "True" : "False") + "/" + c.getCarId() +"#";
        }
        return result;
    }

    @TypeConverter
    public static Ranking stringToRanking(String data) {
        if (data == null) {
            return null;
        }
        return new Ranking();
    }

    @TypeConverter
    public static String rankingToString(Ranking ranking) {
        String result = "";
        return result;
    }

}
