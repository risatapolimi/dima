package com.risata.parkshare.pattern_observer.messages;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.risata.parkshare.R;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.services.MyBroadcastReceiver;
import com.risata.parkshare.services.NotificationReceiver;
import com.risata.parkshare.services.ServiceConstants;
import com.risata.parkshare.views.LoginActivity;
import com.risata.parkshare.views.MainActivity_Drawer;


public class NotifyParkingFoundMessage implements MyMessage {

    private Context context;
    private final String CHANNEL_ID;
    private Position destination;
    private NotificationReceiver notificationReceiver;


    public NotifyParkingFoundMessage(Context cont, Position destination){
        this.destination=destination;
        context=cont;
        CHANNEL_ID="CHANNEL_NOTIFICATION";
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        notificationReceiver=new NotificationReceiver(new Handler(),context,destination);
        notificationReceiver.registerObserver(mainPresenter);
        notifyParkingFound(mainPresenter);
    }

    public void notifyParkingFound(MainPresenter mainPresenter){
        //Create Notification Channel
        this.createNotificationChannel();
        //Create intent dell'activity da avviare al tap della notifica
        //TODO scegliere cosa fare al click della notifica, per ora per evitare problemi NotificationActivity
        Bundle bundle=new Bundle();
        String userid=mainPresenter.getModel().getUser().getUserId();
        String token=mainPresenter.getConnectionHandler().getTokenAuth();
        bundle.putString("userId",userid);
        bundle.putString("token",token);
        bundle.putDouble("destlat",destination.getLatitude());
        bundle.putDouble("destlong",destination.getLongitude());
        Intent intent = new Intent(context, MainActivity_Drawer.class);
        intent.putExtras(bundle);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        ServiceConstants.COME_FROM_NOTIFICATION=true;



        //Create intent action of button yes
        Intent yesIntent = new Intent(context, MyBroadcastReceiver.class);
        yesIntent.setAction(context.getString(R.string.yes));
        yesIntent.putExtra(ServiceConstants.NOTIFICATION_YES_RECEIVER,notificationReceiver);
        PendingIntent yesAnswerPendingIntent =
                PendingIntent.getBroadcast(context, 0, yesIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Create intent action of button no
        Intent noIntent = new Intent(context, MyBroadcastReceiver.class);
        noIntent.setAction(context.getString(R.string.no));
        noIntent.putExtra(ServiceConstants.NOTIFICATION_NO_RECEIVER,notificationReceiver);
        PendingIntent noAnswerPendingIntent =
                PendingIntent.getBroadcast(context, 0, noIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_a)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(context.getString(R.string.is_there_parking))
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .addAction(R.drawable.ic_launcher_a,context.getString(R.string.yes), yesAnswerPendingIntent)
                .addAction(R.drawable.ic_launcher_a, context.getString(R.string.no), noAnswerPendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(ServiceConstants.ASK_PARKING_NOTIFICATION_ID, mBuilder.build());
    }


    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


}
