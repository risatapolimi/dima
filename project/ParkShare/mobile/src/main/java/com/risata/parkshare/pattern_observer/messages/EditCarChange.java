package com.risata.parkshare.pattern_observer.messages;

import android.widget.Toast;

import com.risata.parkshare.R;
import com.risata.parkshare.adapters.CarArrayAdapter;
import com.risata.parkshare.fragments.UserProfileFragment;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.presenters.MainPresenter;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Marco Sartini on 06/04/2018.
 */

public class EditCarChange implements MyMessage {
    private Car editingCar;

    public EditCarChange(Car editingCar) {
        this.editingCar = editingCar;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        UserProfileFragment userProfileFragment = mainPresenter.getUserProfilePresenter().getUserProfileFragment();
        Model model= mainPresenter.getModel();

        mainPresenter.getConnectionHandler().editCar(editingCar, new Callback<Car>(){

            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                if(response.isSuccessful()) {
                    mainPresenter.getDbHandler().editCar(response.body());
                    model.getUser().setCarList(mainPresenter.getDbHandler().getCarList());
                    updateFragment(userProfileFragment);
                }else{
                    String error="";
                    try {
                        error =response.errorBody().string();
                    } catch (IOException e) {
                        error=userProfileFragment.getString(R.string.server_error);
                    }
                    Toast.makeText(userProfileFragment.getContext(),error,Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(userProfileFragment.getContext(), R.string.connection_failure,Toast.LENGTH_SHORT).show();
            }
        });



    }


    private void updateFragment(UserProfileFragment fragment) {
        ((CarArrayAdapter) fragment.getCarsLV().getAdapter()).notifyDataSetChanged();
    }
}
