package com.risata.parkshare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Marco Sartini on 10/05/2018.
 */

public class ParkingSignal {
    public static final int LARGE = 1;
    public static final int PERFECT = 2;
    public static final int SMALL = 3;

    @SerializedName("dimension")
    @Expose
    private int dimensionEstimate;
    @SerializedName("ispayant")
    @Expose
    private boolean isPayant;
    @SerializedName("isrestricted")
    @Expose
    private boolean isRestricted;
    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("longitude")
    @Expose
    private double longitude;

    public ParkingSignal(int dimensionEstimate, boolean isPayant, boolean isRestricted) {
        this.dimensionEstimate = dimensionEstimate;
        this.isPayant = isPayant;
        this.isRestricted = isRestricted;
        latitude = 0;
        longitude = 0;
    }

    public int getDimensionEstimate() {
        return dimensionEstimate;
    }

    public boolean isPayant() {
        return isPayant;
    }

    public boolean isRestricted() {
        return isRestricted;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
