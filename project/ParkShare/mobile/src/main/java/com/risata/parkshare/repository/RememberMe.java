package com.risata.parkshare.repository;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class RememberMe {

    @PrimaryKey
    @NonNull
    private String email;
    private String passwdHash;
    private boolean rememberme;
    private String token;

    public RememberMe(@NonNull String email, String passwdHash, boolean rememberme, String token) {
        this.email = email;
        this.passwdHash = passwdHash;
        this.rememberme = rememberme;
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswdHash() {
        return passwdHash;
    }

    public void setPasswdHash(String passwdHash) {
        this.passwdHash = passwdHash;
    }

    public boolean isRememberme() {
        return rememberme;
    }

    public void setRememberme(boolean rememberme) {
        this.rememberme = rememberme;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
