package com.risata.parkshare.model;

import android.location.Location;

import com.risata.parkshare.handlers.DatabaseHandler;
import com.risata.parkshare.views.MainActivity_Drawer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * Created by DanieleIlBellissimo on 15/03/2018.
 */

public class Model implements ModelHiding {

    private DatabaseHandler dbHandler;
    private final Executor executor;
    private User user;
    private Position userLocation;
    private Position searchLocation;
    private static final String UID_KEY = "uid";
    private List<ParkingSlot> parkingSlotList;
    private Filter filter;
    private String userId;

   public Model(String id, Position location, DatabaseHandler dbHandler, Executor executor) {
       this.dbHandler = dbHandler;
       this.executor = executor;
       this.userLocation=location;
       this.userId = id;
       this.parkingSlotList = new ArrayList<>();
       this.filter = new Filter();
       initSearchLocation();
       //initParkings();
   }

   public void initData(MainActivity_Drawer.CallbackYoYo callbackYoYo){
       user = dbHandler.initUser(userId);
       callbackYoYo.onTaskExecuted(); // viene eseguito come runOnUIThread
   }

   public void initSearchLocation(){
       Location location=new Location("");
       location.setLatitude(45.464180);
       location.setLongitude(9.190006);
       searchLocation=new Position(location);
   }

   @Override
    public Position getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(Position userLocation) {
        this.userLocation = userLocation;
    }

    public User getUser(){
       return user;
    }


    private void initParkings() {
        this.parkingSlotList=new ArrayList<>();
        // Milano, L26
        Location loc=new Location("");
        loc.setLatitude(45.476018);
        loc.setLongitude(9.234362);
        parkingSlotList.add(new ParkingSlot(10,10,new Position(loc),true,true,false));
        loc.setLatitude(45.473711);
        loc.setLongitude(9.234407);
        parkingSlotList.add(new ParkingSlot(10,10,new Position(loc),true,false,true));
        loc.setLatitude(45.476485);
        loc.setLongitude(9.236647);
        parkingSlotList.add(new ParkingSlot(10,10,new Position(loc),true,true,false));
        //Lecco, Acquate
        //Sagrato
        loc.setLatitude(45.860160);
        loc.setLongitude(9.408689);
        parkingSlotList.add(new ParkingSlot(10,10,new Position(loc),true,false,true));
        //Poggi45.473711
        loc.setLatitude(45.859757);
        loc.setLongitude(9.411310);
        parkingSlotList.add(new ParkingSlot(10,10,new Position(loc),true,true,false));
        //Laga 45.859109, 9.408208
        loc.setLatitude(45.859109);
        loc.setLongitude(9.408208);
        parkingSlotList.add(new ParkingSlot(10,10,new Position(loc),true,true,true));
        //Meridiane 45.85145748305844 9.397318867973087
        loc.setLatitude(45.851457);
        loc.setLongitude(9.397318);
        parkingSlotList.add(new ParkingSlot(10,10,new Position(loc),true,true,false));

        //Via Castelletti, Ponte Lambro
        //45.827795, 9.224309
        loc.setLatitude(45.827795);
        loc.setLongitude(9.224309);
        parkingSlotList.add(new ParkingSlot(10,10,new Position(loc),true,false,true));
        //Via Monte Grappa, Ponte Lambro
        //45.828055, 9.224614
        loc.setLatitude(45.828055);
        loc.setLongitude(9.224614);
        parkingSlotList.add(new ParkingSlot(10,10,new Position(loc),true,true,false));
    }


    public List<ParkingSlot> getParkingSlotList() {
        return parkingSlotList;
    }

    public void setParkingSlotList(List<ParkingSlot> parkingSlotList) {
        this.parkingSlotList = parkingSlotList;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void addParking(ParkingSlot p) {
       this.parkingSlotList.add(p);
    }

    public void addParkingSlotList(List<ParkingSlot> parkingSlotsReceived) {
       this.parkingSlotList.addAll(parkingSlotsReceived);
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public Position getSearchLocation() {
        return searchLocation;
    }

    public void setSearchLocation(Position searchLocation) {
        this.searchLocation = searchLocation;
    }
}
