package com.risata.parkshare.pattern_observer.messages;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;
import com.risata.parkshare.R;
import com.risata.parkshare.fragments.UserProfileFragment;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.presenters.MapHomePresenter;

public class GetDirectionsMessage implements MyMessage {

    private Marker marker;
    private boolean isSeaching;

    public GetDirectionsMessage(Marker marker, boolean isS) {
        this.marker = marker;
        this.isSeaching=isS;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        MapHomePresenter mapHomePresenter = mainPresenter.getMapHomePresenter();
        mapHomePresenter.getDirections(marker,mainPresenter);
        mapHomePresenter.getMapHomeFragment().setSearching(false);
        mapHomePresenter.getMapHomeFragment().setLookingAround(false);
    }
}
