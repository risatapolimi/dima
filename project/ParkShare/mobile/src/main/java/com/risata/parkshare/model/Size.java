package com.risata.parkshare.model;

/**
 * Created by Marco Sartini on 21/08/2018.
 */
public enum Size {
    SMALL(0),
    PERFECT(1),
    LARGE(2),
    NOT_DEFINED(3),;

    private final int value;

    Size(int value){
        this.value = value;
    }

}
