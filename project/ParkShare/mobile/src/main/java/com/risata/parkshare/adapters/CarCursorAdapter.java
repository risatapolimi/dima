package com.risata.parkshare.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.risata.parkshare.R;
import com.risata.parkshare.extendedGraphics.DeleteCarImageButton;
import com.risata.parkshare.extendedGraphics.EditCarImageButton;
import com.risata.parkshare.extendedGraphics.FavoriteCarImageButton;
import com.risata.parkshare.model.Car;

import java.util.List;

/**
 * Created by Marco Sartini on 20/03/2018.
 */

public class CarCursorAdapter extends CursorAdapter {

    private View.OnClickListener favoriteListener;
    private  View.OnClickListener deleteListener;
    private View.OnClickListener ediListener;

    public CarCursorAdapter(Context context, Cursor cursor, View.OnClickListener favoriteListener, View.OnClickListener editListener, View.OnClickListener deleteListener) {
        super(context, cursor);
        this.favoriteListener = favoriteListener;
        this.deleteListener = deleteListener;
        this.ediListener = editListener;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.car_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView titleTV;
        TextView dimensionsTV;
        FavoriteCarImageButton favoriteCarIB;
        DeleteCarImageButton deleteCarIB;
        EditCarImageButton editCarIB;
        TextView categoryTV;

        titleTV = (TextView) view.
                findViewById(R.id.carTitleTV);
        categoryTV = (TextView) view.findViewById(R.id.carCategoryTV);

        dimensionsTV = (TextView) view.
                findViewById(R.id.carDimensionsTV);
        favoriteCarIB = (FavoriteCarImageButton) view.findViewById(R.id.item_car_favorite);
        deleteCarIB = (DeleteCarImageButton) view.findViewById(R.id.item_car_delete);
        editCarIB = (EditCarImageButton) view.findViewById(R.id.item_car_edit);

        titleTV.setText(cursor.getString(cursor.getColumnIndexOrThrow("manufacturer")) + " " + cursor.getString(cursor.getColumnIndexOrThrow("model")));
        //  viewHolder.manufacturerTV.setText(car.getManufacturer());
        //  viewHolder.modelTV.setText(car.getModel());
        dimensionsTV.setText(cursor.getString(cursor.getColumnIndexOrThrow("length")) + " x " + cursor.getString(cursor.getColumnIndexOrThrow("width")) + " m");
       categoryTV.setText(cursor.getString(cursor.getColumnIndexOrThrow("category")));
       // favoriteCarIB.setCar(car);
       favoriteCarIB.setOnClickListener(favoriteListener);
       /* if (car.isFavorite())
            setFavoriteOn(viewHolder);
        else
            setFavoriteOff(viewHolder);*/
       // deleteCarIB.setCar(car);
        deleteCarIB.setOnClickListener(deleteListener);
      //  editCarIB.setCar(car);
        editCarIB.setOnClickListener(ediListener);
    }

    public void setFavoriteOn(CarViewHolder viewHolder) {
        viewHolder.favoriteCarIB.setImageResource(R.drawable.ic_favorite_on);
    }

    public void setFavoriteOff(CarViewHolder viewHolder) {
        viewHolder.favoriteCarIB.setImageResource(R.drawable.ic_favorite_off);
    }

    static class CarViewHolder {

        TextView titleTV;
     //   TextView manufacturerTV;
     //   TextView modelTV;
        TextView dimensionsTV;
        FavoriteCarImageButton favoriteCarIB;
        DeleteCarImageButton deleteCarIB;
        EditCarImageButton editCarIB;
        TextView categoryTV;

        public CarViewHolder(View view) {
            titleTV = (TextView) view.
                    findViewById(R.id.carTitleTV);
            categoryTV = (TextView) view.findViewById(R.id.carCategoryTV);
       /*     manufacturerTV = (TextView) view.
                    findViewById(R.id.carManufacturerTV);
            modelTV = (TextView) view.
                    findViewById(R.id.carModelTV); */
            dimensionsTV = (TextView) view.
                    findViewById(R.id.carDimensionsTV);
            favoriteCarIB = (FavoriteCarImageButton) view.findViewById(R.id.item_car_favorite);
            deleteCarIB = (DeleteCarImageButton) view.findViewById(R.id.item_car_delete);
            editCarIB = (EditCarImageButton) view.findViewById(R.id.item_car_edit);
        }

    }
}
