package com.risata.parkshare.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by Daniele on 08/03/2018.
 */
@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "userId",
        childColumns = "userId",
        onDelete = CASCADE))
public class Ranking {

    @SerializedName("level")
    @Expose
    private int level;

    @SerializedName("exp")
    @Expose
    private int exp;

    @PrimaryKey
    @SerializedName("userid")
    @Expose
    @NonNull
    private String userId;

    public Ranking(){
        exp=0;
        level=1;
    }

    public Ranking(String userId){
        this();
        this.userId = userId;
    }

    public Ranking(String userId,int lev, int experience){
        this.userId = userId;
        this.level=lev;
        this.exp=experience;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getValue(){
        return Integer.toString(this.getLevel()) ;
    }

    public int getLevelExp(int lev){
        return decFact(lev);
    }

    public int getRadiusMeters(){
        return level * 150;
    }

    private int decFact(int value){
        if(value==0)
            return 0;
        else
            return value*10+decFact(value-1);
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getNextLevelExperience(){
        return (level)*(level+1)*10;
    }
    public int getMaxLevelExperience(){
        return level*(level-1)*10;
    }
}
