package com.risata.parkshare.pattern_observer.messages;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.risata.parkshare.R;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.fragments.SignalParkDialogFragment;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.presenters.MainPresenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EndNavigationMessage implements MyMessage {

    public static final int SignalPark = 124;
    public static final String REMOTE_ADDR="http://95.251.172.187:8080/dima_backend/webresources/entities.parkingslot";

    @Override
    public void perform(MainPresenter mainPresenter) {

        mainPresenter.getMapHomePresenter().restartApplicationForEnding(mainPresenter.getModel().getUser().getUserId(),
                mainPresenter.getConnectionHandler().getTokenAuth(),mainPresenter.getModel());
        askForOthersParking(mainPresenter);
    }

    public static void askForOthersParking(MainPresenter mainPresenter){
        Context context=mainPresenter.getMapHomePresenter().getMapHomeFragment().getActivity();
        if(context!=null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            // Add the buttons
            builder.setTitle(R.string.notify_other_parkings);
            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    mainPresenter.getMapHomePresenter().getMapHomeFragment().signalParking();

                }
            });
            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                }
            });
            // Set other dialog properties

            // Create the AlertDialog
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }



}
