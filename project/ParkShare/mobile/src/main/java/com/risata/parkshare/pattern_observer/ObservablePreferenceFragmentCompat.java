package com.risata.parkshare.pattern_observer;

import android.support.v7.preference.PreferenceFragmentCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marco Sartini on 23/08/2018.
 */

public abstract class ObservablePreferenceFragmentCompat<T> extends PreferenceFragmentCompat {
    private List<MyObserver<T>> observers;

    public ObservablePreferenceFragmentCompat(){
        observers=new ArrayList<>();
    }

    public void registerObserver(MyObserver myObserver){
        this.observers.add(myObserver);
    }

    public void removeObserver(MyObserver myObserver){
        this.observers.remove(myObserver);
    }

    public void notifyObserver(T message){
        for(MyObserver<T> M:observers)
            M.update(message);
    }

    public List<MyObserver<T>> getObservers() {
        return observers;
    }

    public void setObservers(List<MyObserver<T>> observers) {
        this.observers = observers;
    }
}

