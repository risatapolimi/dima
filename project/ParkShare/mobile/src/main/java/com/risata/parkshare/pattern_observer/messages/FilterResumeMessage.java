package com.risata.parkshare.pattern_observer.messages;

import com.risata.parkshare.presenters.MainPresenter;

public class FilterResumeMessage implements  MyMessage {

    @Override
    public void perform(MainPresenter mainPresenter) {
        mainPresenter.getMapHomePresenter().manageFilter(mainPresenter.getModel().getFilter());
    }
}
