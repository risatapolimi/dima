package com.risata.parkshare.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.risata.parkshare.R;


public class MyBroadcastReceiver extends BroadcastReceiver {

    private ResultReceiver receiver;

    @Override
    public void onReceive(Context context, Intent intent) {
        String answer=intent.getAction();
        if(answer.equals(context.getString(R.string.yes))){
            receiver=intent.getParcelableExtra(ServiceConstants.NOTIFICATION_YES_RECEIVER);
        }else{
            receiver=intent.getParcelableExtra(ServiceConstants.NOTIFICATION_NO_RECEIVER);
        }
        Bundle bundle=new Bundle();
        bundle.putString("answer",answer);
        receiver.send(ServiceConstants.BROADCAST_RECEIVER_RESULT_CODE,bundle);
    }
}
