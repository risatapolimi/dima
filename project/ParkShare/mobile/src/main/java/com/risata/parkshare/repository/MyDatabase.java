package com.risata.parkshare.repository;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.model.User;

/**
 * Created by Marco Sartini on 19/04/2018.
 */

@Database(entities = {User.class, Car.class, Ranking.class, RememberMe.class}, version = 10)
@TypeConverters({ParkShareTypeConverter.class})
public abstract class MyDatabase extends RoomDatabase {
        public abstract UserDao userDao();
        public abstract CarDao carDao();
        public abstract RankingDao rankingDao();
        public abstract RememberMeDao rememberMeDao();
    }

