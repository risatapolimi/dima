package com.risata.parkshare.repository;



        /*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.User;

import java.util.List;


/**
         * Data Access Object for the users table.
         */
        @Dao
        public interface CarDao {

                @Insert(onConflict = OnConflictStrategy.REPLACE)
                void save(Car car);
                @Query("SELECT * FROM car WHERE carId = :carId AND userId = :userId")
                Car load(String carId, String userId);

        @Query("SELECT * FROM car WHERE userId = :userId")
        List<Car> loadCars(String userId);

                @Query("UPDATE Car SET manufacturer = :manufacturer, model = :model, length = :length, width = :width, category = :category WHERE carId = :carId AND userId = :userId")
                void updateEditedCar(String manufacturer, String model, Double length, Double width, String category, String carId, String userId);

                @Query("UPDATE Car SET isFavorite = 0 WHERE userId = :userId AND isFavorite = 1")
                void clearInUseCar(String userId);

                @Query("UPDATE Car SET isFavorite = 1 WHERE carId = :carId AND userId = :userId")
                void setInUseCar(String carId, String userId);


                /**
                 * Insert a car in the database. If the car already exists, replace it.
                 *
                 * @param car the car to be inserted.
                 */
                @Insert(onConflict = OnConflictStrategy.REPLACE)
                void insert(Car car);

                /**
                 * Delete all cars.
                 */
                @Query("DELETE FROM car")
                void deleteAllCars();

        /** delete a single car
         *
         * @param carId
         */
        @Query("DELETE FROM car WHERE carId = :carId AND userId = :userId")
                void deleteCar(String carId, String userId);

        }

