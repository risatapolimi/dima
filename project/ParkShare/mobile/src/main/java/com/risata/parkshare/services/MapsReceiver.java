package com.risata.parkshare.services;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import com.risata.parkshare.model.ModelHiding;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.pattern_observer.ObservableReceiver;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.pattern_observer.messages.NotifyParkingFoundMessage;


public class MapsReceiver extends ObservableReceiver<MyMessage> {

   private Position destination;
   private ModelHiding modelHiding;
   private Context context;
   private boolean alreadyAskForParking=false;

    /**
     * Create a new ResultReceive to receive results.  Your
     * {@link #onReceiveResult} method will be called from the thread running
     * <var>handler</var> if given, or from an arbitrary thread if null.
     *
     * @param handler
     */
    public MapsReceiver(Handler handler, Position dest,ModelHiding mod, Context context) {
        super(handler);
        destination=dest;
        this.context=context;
        this.modelHiding=mod;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        if (resultData == null) {
            return;
        }
        if(resultCode==ServiceConstants.MAPS_SERVICE_RESULT_CODE){
            double distance=destination.differenceKm(modelHiding.getUserLocation());
            if(distance<0.1 && !alreadyAskForParking) {
                //Distanza raggiunta circa 40 metri, chiede se ha trovato il parcheggio
                this.notifyObserver(new NotifyParkingFoundMessage(context, destination));
                this.alreadyAskForParking = true;
            }
        }
    }

    public void setDestination(Position destination) {
        System.out.println("Old destinazione: "+this.destination.toString());
        this.destination = destination;
        System.out.println("New destinazione: "+this.destination.toString());
    }

    public void setAlreadyAskForParking(boolean alreadyAskForParking) {
        this.alreadyAskForParking = alreadyAskForParking;
    }

}




