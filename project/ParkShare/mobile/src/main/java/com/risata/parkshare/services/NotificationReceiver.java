package com.risata.parkshare.services;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.risata.parkshare.R;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.pattern_observer.ObservableReceiver;
import com.risata.parkshare.pattern_observer.messages.EndNavigationMessage;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.pattern_observer.messages.RedirectParkingMessagge;
import com.risata.parkshare.pattern_observer.messages.RemoveParkingMessage;

public class NotificationReceiver extends ObservableReceiver<MyMessage> {

    private Context context;
    private Position destination;
    /**
     * Create a new ResultReceive to receive results.  Your
     * {@link #onReceiveResult} method will be called from the thread running
     * <var>handler</var> if given, or from an arbitrary thread if null.
     *
     * @param handler
     */
    public NotificationReceiver(Handler handler, Context cont,Position destination) {
        super(handler);
        context=cont;
        this.destination=destination;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        if (resultData == null) {
            return;
        }

        NotificationManager manager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(ServiceConstants.ASK_PARKING_NOTIFICATION_ID);
        String answer=resultData.getString("answer");
        this.notifyObserver(new RemoveParkingMessage(destination));
        if(answer.equals(context.getString(R.string.yes))){
            this.notifyObserver(new EndNavigationMessage());
            context.stopService(new Intent(context,ParkingService.class));
            ServiceConstants.FINISH_SERVICE=true;
            ServiceConstants.COME_FROM_NOTIFICATION=false;
        }else if(answer.equals(context.getString(R.string.no))){
            this.notifyObserver(new RedirectParkingMessagge(destination));
            ServiceConstants.COME_FROM_NOTIFICATION=false;
        }
    }


}
