package com.risata.parkshare.pattern_observer.messages;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.presenters.MainPresenter;

public class OnSearchLocationMessage extends UpdateParking {

    public OnSearchLocationMessage(Location loc){
        super(loc);
    }

    @Override
    public void perform(MainPresenter mainPresenter) {

        //mainPresenter.getMapHomePresenter().getLocationHandler().removeUpdate();

        //set location selected as user location in order to correctly filter parking slots
        mainPresenter.getModel().setSearchLocation(new Position(loc));

        mainPresenter.getMapHomePresenter().getMapHomeFragment().setSearching(true);

        super.perform(mainPresenter);

    }


}
