package com.risata.parkshare.pattern_observer.messages;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.model.User;
import com.risata.parkshare.presenters.MainPresenter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParkingWithAddress implements MyMessage {

    private ParkingSlotPOJO parkingSlot;

    public ParkingWithAddress(ParkingSlotPOJO p) {
        parkingSlot=p;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {

        Context context=mainPresenter.getMapHomePresenter().getMapHomeFragment().getContext();
        mainPresenter.getConnectionHandler().signalParking(parkingSlot, new Callback<ParkingSlotPOJO>() {

            @Override
            public void onResponse(Call<ParkingSlotPOJO> call, Response<ParkingSlotPOJO> response) {
                if (response.isSuccessful()) {
                    ParkingSlot p = response.body().convertToParkingSlot();
                    Toast.makeText(context, "parking in "+p.getAddress()+ " added", Toast.LENGTH_SHORT).show();
                    Ranking oldRanking=mainPresenter.getModel().getUser().getMyRanking();
                    mainPresenter.getConnectionHandler().addRankingPoint(new Callback<Ranking>(){

                        @Override
                        public void onResponse(Call<Ranking> call, Response<Ranking> response) {
                            Ranking rankReceived=response.body();
                            mainPresenter.getModel().getUser().setMyRanking(rankReceived);
                            if(oldRanking.getLevel()<rankReceived.getLevel()){
                                Toast.makeText(context,"Level up: ranking "+rankReceived.getLevel()+ " reached",Toast.LENGTH_SHORT).show();
                            }
                            mainPresenter.getDbHandler().updateRanking(rankReceived.getUserId(),rankReceived.getLevel(),rankReceived.getExp());

                        }

                        @Override
                        public void onFailure(Call<Ranking> call, Throwable t) {
                        }
                    });
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(context, "Connection Failure: parking not added", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
