package com.risata.parkshare.pattern_observer.messages;

import android.location.Location;
import android.util.Log;

import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.presenters.MapHomePresenter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotifyUpdatedAddress implements MyMessage {

    public NotifyUpdatedAddress(){

    }
    @Override
    public void perform(MainPresenter mainPresenter) {

    }
}
