package com.risata.parkshare.pattern_observer;

import android.os.Handler;
import android.os.ResultReceiver;

import java.util.ArrayList;
import java.util.List;

public abstract class ObservableReceiver<T> extends ResultReceiver{

    private List<MyObserver<T>> observers;
    /**
     * Create a new ResultReceive to receive results.  Your
     * {@link #onReceiveResult} method will be called from the thread running
     * <var>handler</var> if given, or from an arbitrary thread if null.
     *
     * @param handler
     */
    public ObservableReceiver(Handler handler) {
        super(handler);
        observers=new ArrayList<>();
    }

    public void registerObserver(MyObserver myObserver){
        this.observers.add(myObserver);
    }

    public void removeObserver(MyObserver myObserver){
        this.observers.remove(myObserver);
    }

    public void notifyObserver(T message){
        for(MyObserver<T> myObserver:observers)
            myObserver.update(message);
    }
}
