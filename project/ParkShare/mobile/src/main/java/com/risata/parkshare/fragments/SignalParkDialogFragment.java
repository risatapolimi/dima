package com.risata.parkshare.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.risata.parkshare.R;
import com.risata.parkshare.model.ParkingSignal;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Size;
import com.risata.parkshare.pattern_observer.MyObservable;
import com.risata.parkshare.pattern_observer.MyObserver;
import com.risata.parkshare.pattern_observer.messages.SignalParkMsg;
import com.risata.parkshare.presenters.MainPresenter;

import java.util.ArrayList;

/**
 * Created by Marco Sartini on 10/04/2018.
 */

public class SignalParkDialogFragment extends DialogFragment  {
    

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction


        View v = getActivity().getLayoutInflater().inflate(R.layout.signal_park, null);
        RadioGroup fitOpinionRG = v.findViewById(R.id.fit_opinion_group);
        CheckBox handicapCB = v.findViewById(R.id.handicap_park);
        CheckBox payantCB = v.findViewById(R.id.payant_park);


        return new AlertDialog.Builder(getContext())
        .setView(v)

        .setTitle(R.string.signal_park_title)
                .setPositiveButton(R.string.signal_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int estimation = fitOpinionRG.getCheckedRadioButtonId();
                        RadioButton radio = (RadioButton) v.findViewById(estimation);
                        int radioSelected=fitOpinionRG.indexOfChild(radio);
                        Size dimension = Size.NOT_DEFINED;
                        switch (radioSelected){
                            case 0:
                                dimension = Size.LARGE;
                                break;
                            case 1:
                                dimension = Size.PERFECT;
                                break;
                            case 2:
                                dimension = Size.SMALL;
                                break;
                        }
                        ((MapHomeFragment) getTargetFragment()).notifyObserver(new SignalParkMsg(new ParkingSlotPOJO(dimension , payantCB.isChecked(), handicapCB.isChecked())));
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })

        // Create the AlertDialog object and return it
        .create();


    }

}
