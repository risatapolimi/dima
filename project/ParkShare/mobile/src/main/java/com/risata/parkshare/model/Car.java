package com.risata.parkshare.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by Daniele on 08/03/2018.
 */
@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "userId",
        childColumns = "userId",
        onDelete = CASCADE))
public class Car implements Comparable{

    public static final String NO_ID = "NO_ID";

    @SerializedName("manufacturer")
    @Expose
    private String manufacturer;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("width")
    @Expose
    private double width;
    @SerializedName("length")
    @Expose
    private double length;
    @SerializedName("isfavourite")
    @Expose
    private boolean isFavorite;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("carid")
    @Expose
    @PrimaryKey
    @NonNull
    private String carId;
    @SerializedName("userid")
    @Expose
    private String userId;

    @Ignore
    public Car(String userId, String manufacturer, String model, double width, double length, String carId) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.width = width;
        this.length = length;
        this.carId = carId;
        this.userId = userId;
    }

    @Ignore
    public Car(String userId, String manufacturer, String model, double width, double length, String category, boolean isFavorite) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.width = width;
        this.length = length;
        this.category = category;
        this.isFavorite = isFavorite;
        this.carId = NO_ID;
        this.userId = userId;
    }

    @Ignore
    public Car(String manufacturer, String model, double width, double length, String category, String carId) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.width = width;
        this.length = length;
        this.category = category;
        this.carId = carId;

    }

    public Car(String userId, String manufacturer, String model, double width, double length, String category, boolean isFavorite, String carId) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.width = width;
        this.length = length;
        this.category = category;
        this.isFavorite = isFavorite;
        this.carId = carId;
        this.userId = userId;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }


    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (Double.compare(car.width, width) != 0) return false;
        if (Double.compare(car.length, length) != 0) return false;
        if (!manufacturer.equals(car.manufacturer)) return false;
        return model.equals(car.model);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = manufacturer.hashCode();
        result = 31 * result + model.hashCode();
        temp = Double.doubleToLongBits(width);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(length);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        if(this.isFavorite()){
            return 1;
        }
        else return -1;
    }

    public double getArea() {
        return length*width;
    }
}
