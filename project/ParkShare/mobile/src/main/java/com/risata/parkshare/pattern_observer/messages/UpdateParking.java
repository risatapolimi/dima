package com.risata.parkshare.pattern_observer.messages;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.Marker;
import com.risata.parkshare.adapters.ParkingSlotAdapter;
import com.risata.parkshare.fragments.MapHomeFragment;
import com.risata.parkshare.model.Car;
import com.risata.parkshare.model.Filter;
import com.risata.parkshare.model.Model;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotItem;
import com.risata.parkshare.model.ParkingSlotPOJO;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.model.Size;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.presenters.MapHomePresenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateParking implements MyMessage {

    protected Location loc;

    public UpdateParking(Location location) {
        this.loc = location;
    }



    @Override
    public void perform(MainPresenter mainPresenter) {
        Model model = mainPresenter.getModel();
        MapHomePresenter mapHomePresenter = mainPresenter.getMapHomePresenter();
        MapHomeFragment mapHomeFragment = mapHomePresenter.getMapHomeFragment();

        if(mapHomeFragment.isSearching())
            loc=model.getSearchLocation();


        mainPresenter.getConnectionHandler().getParkingSlots(loc, new Callback<List<ParkingSlotPOJO>>(){

            @Override
            public void onResponse(Call<List<ParkingSlotPOJO>> call, Response<List<ParkingSlotPOJO>> response) {
                if(response.isSuccessful()) {


                    List<ParkingSlot> parkingSlotsReceived=ParkingSlotPOJO.convertList(response.body());

                    List<ParkingSlot> modelOldParkings = model.getParkingSlotList();

                    Map<Integer,Integer> idToPositionMap = mainPresenter.getMapHomePresenter().getMapHomeFragment().getParkingSlotAdapter().getIdToPositionMap();
                    ParkingSlotAdapter adapter =mainPresenter.getMapHomePresenter().getMapHomeFragment().getParkingSlotAdapter();

                    ParkingSlotItem slotItem=null;
                    int parkingSelected=mapHomeFragment.getParkingSelected();
                    if(parkingSelected>=0 && parkingSelected<adapter.getParkingSlotList().size()){
                        slotItem=adapter.getParkingSlotList().get(parkingSelected);
                    }

                    List<ParkingSlot> listToBeDeleted = new ArrayList<>();
                    if(modelOldParkings!=null) {

                        for(ParkingSlot pCheck: modelOldParkings){
                            if(!parkingSlotsReceived.contains(pCheck)){
                                listToBeDeleted.add(pCheck);
                            }
                        }

                        int delindex=0;
                        for(ParkingSlot p : listToBeDeleted){

                            ParkingSlotItem itemToDelete=null;
                            for(ParkingSlotItem item:adapter.getParkingSlotList()){
                                if(item.getParkingSlot().getId()==p.getId())
                                    itemToDelete=item;
                            }
                            Integer index = idToPositionMap.get(p.getId());
                            if(itemToDelete!=null){
                                index=index-delindex;
                                adapter.getParkingSlotList().remove(itemToDelete);
                                adapter.notifyItemRemoved(index);
                                adapter.getIdToPositionMap().remove(itemToDelete.getParkingSlot().getId());
                                delindex++;
                            }

                        }
                        int ind=0;
                        for(ParkingSlotItem p : adapter.getParkingSlotList()){
                            ind=adapter.getParkingSlotList().indexOf(p);
                            p.setIndex(ind);
                            adapter.getIdToPositionMap().put(p.getParkingSlot().getId(),ind);
                            //adapter.notifyItemChanged(ind); Rimosso per evitare sfarfallii
                        }

                        //update to new position of item selected
                        int newParkingSelected;
                        if(slotItem!=null){
                            newParkingSelected=adapter.getParkingSlotList().indexOf(slotItem);
                            if(newParkingSelected!=-1){
                                mapHomeFragment.setParkingSelected(newParkingSelected);
                                ParkingSlotAdapter.positionToHack = newParkingSelected;
                                adapter.notifyItemChanged(newParkingSelected, "ChangeSelection");
                                if(parkingSelected>=0 && parkingSelected<adapter.getParkingSlotList().size())
                                    adapter.notifyItemChanged(parkingSelected, "ResetSelection");
                            }

                        }


                        //rimuove dalla lista nel modello i parcheggi diversi da quelli ricevuti
                         modelOldParkings.removeAll(listToBeDeleted);

                        //rimuovo i vecchi parcheggi per non resettare gli indirizzi
                        parkingSlotsReceived.removeAll(modelOldParkings);

                    }
                    //filtraggio dei parcheggi
                    mainPresenter.getModel().addParkingSlotList(parkingSlotsReceived);
                    try {
                        setUpParking(adapter, loc, mainPresenter.getModel().getParkingSlotList(), mapHomeFragment,
                                listToBeDeleted, model.getUser().getMyRanking(), model.getFilter());
                    }catch (Exception exp){
                        Log.d("Exception1","update");
                        model.setParkingSlotList(new ArrayList<>());
                        adapter.setParkingSelected(0);
                        adapter.setParkingSlotList(new ArrayList<>());
                        adapter.getIdToPositionMap().clear();
                        adapter.getParkingSlotViewMap().clear();
                    }

                }
            }


            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("updateparking","we faaaaaaaail");
            }
        });
    }

    public static void setUpParking(ParkingSlotAdapter adapter,Location loc, List<ParkingSlot> parkingSlotList, MapHomeFragment mapHomeFragment,
                              List<ParkingSlot> listToDelete,Ranking userRanking, Filter filter){

        if (loc != null && mapHomeFragment.isAdded() && mapHomeFragment.getmMap() != null) {

            //Check all item selected
            ParkingSlotItem slotItem=null;
            int parkingSelected=mapHomeFragment.getParkingSelected();
            if(parkingSelected>=0 && parkingSelected<adapter.getParkingSlotList().size()){
                slotItem=adapter.getParkingSlotList().get(parkingSelected);
            }

            List<ParkingSlotItem> filterList = new ArrayList<>();
            int index = 0;
            int delindex=0;
            Position userPosition = new Position(loc);
            mapHomeFragment.removeMarkerParking(listToDelete);
            mapHomeFragment.drawRankingCircle(loc,userRanking);
            //Log.i("userPosition ", "" + userPosition.getLatitude() + " " + userPosition.getLongitude());
            double distance = 0;
            Size size = Size.NOT_DEFINED;
            for (ParkingSlot ps : parkingSlotList) {
                distance = ps.getPosition().differenceKm(userPosition);
                size = filter.calculateParkingAdvicedDimension(ps);
                boolean visible = ps.isInternalRanking(userPosition, userRanking) && filter.isSuitable(ps, size);
                ParkingSlotItem psItem = new ParkingSlotItem(ps);
                psItem.setDistance(distance);
                psItem.setAdvicedSize(size);
                psItem.setVisible(visible);
                if (visible) {
                    filterList.add(psItem);
                    //Log.i("Parking", "" + ps.getAddress() + " index: " + psItem.getIndex());
                } else {
                    if (adapter.getParkingSlotList().contains(psItem)) {
                        //RecyclerView Update
                        Integer position = adapter.getIdToPositionMap().get(ps.getId());
                        if (position != null) {
                            position = position - delindex;
                            adapter.getParkingSlotList().remove(psItem);
                            adapter.notifyItemRemoved(position);
                            adapter.getIdToPositionMap().remove(ps.getId());
                            delindex++;
                            //Log.i("ParkRemove", "" + ps.getAddress() + " positionToDelete: " + position);
                        }
                    }
                    mapHomeFragment.markParking(ps, visible, psItem);
                }
            }



            //Riordinamento filterlist
            Collections.sort(filterList, new Comparator<ParkingSlotItem>() {
                @Override
                public int compare(ParkingSlotItem p1, ParkingSlotItem p2) {
                    return Double.compare(p1.getDistance(),p2.getDistance());
                }
            });

            for(ParkingSlotItem p: filterList){
                Log.d("Ordine", p.getDistance().toString());
            }

            //todo gestire il primo riavvio

                //inserimento recyclerview
                for(ParkingSlotItem p: filterList){
                    if(!adapter.getParkingSlotList().contains(p) && adapter.getParkingSlotList().size()>0){
                        int indexToBeInserted = adapter.getParkingSlotList().size();
                        for(int i=0; i<adapter.getParkingSlotList().size(); i++){
                            ParkingSlotItem adapterP = adapter.getParkingSlotList().get(i);
                            if(p.getDistance() < adapterP.getDistance()){
                                indexToBeInserted = i;
                                i = adapter.getParkingSlotList().size();
                            }
                        }
                        //AGGIUNTO
                        p.setIndex(indexToBeInserted);
                        adapter.getIdToPositionMap().put(p.getParkingSlot().getId(),indexToBeInserted);
                        //
                        adapter.getParkingSlotList().add(indexToBeInserted,p);
                        adapter.notifyItemInserted(indexToBeInserted);


                    }else if (adapter.getParkingSlotList().size() == 0){
                        adapter.getParkingSlotList().add(p);
                        adapter.getIdToPositionMap().put(p.getParkingSlot().getId(),0);
                        adapter.notifyItemInserted(0);
                    }else if (adapter.getParkingSlotList().contains(p)){
                        //Aggiornamento card
                        int j = adapter.getParkingSlotList().indexOf(p);
                        int z = filterList.indexOf(p);
                        //AGGIUNTO dopo riordine
                        if (j!=z){
                            ParkingSlotItem p1 = adapter.getParkingSlotList().remove(j);
                            adapter.notifyItemRemoved(j);
                            adapter.getParkingSlotList().add(z,p1);
                            adapter.getIdToPositionMap().put(p1.getParkingSlot().getId(),z);
                            adapter.notifyItemInserted(z);
                            j=z;
                        }
                        adapter.getParkingSlotList().get(j).setDistance(p.getDistance());
                        adapter.getParkingSlotList().get(j).setAdvicedSize(p.getAdvicedSize());
                        adapter.getParkingSlotList().get(j).setVisible(p.isVisible());
                    }

                }
                //aggiornamento indici e redraw
                for(ParkingSlotItem p : adapter.getParkingSlotList()){
                    index=adapter.getParkingSlotList().indexOf(p);

                    //AGGIUNTO
                    if(p.getIndex()!=index){
                        p.setIndex(index);
                        adapter.getIdToPositionMap().put(p.getParkingSlot().getId(),index);
                        adapter.notifyItemChanged(index);
                    }
                    mapHomeFragment.markParking(p.getParkingSlot(),p.isVisible(),p);
                }

            //update to new position of item selected
            int newParkingSelected;
            if(slotItem!=null){
                newParkingSelected=adapter.getParkingSlotList().indexOf(slotItem);
                if(newParkingSelected!=-1 && newParkingSelected!=parkingSelected){
                    mapHomeFragment.setParkingSelected(newParkingSelected);
                    /*ParkingSlotAdapter.positionToHack = newParkingSelected;
                    adapter.notifyItemChanged(newParkingSelected, "ChangeSelection");
                    if(parkingSelected>=0 && parkingSelected<adapter.getParkingSlotList().size())
                        adapter.notifyItemChanged(parkingSelected, "ResetSelection");*/
                    markNewSelection(mapHomeFragment,newParkingSelected,adapter);
                }

            }

            //update item selected if change the orientation and marker selection
            if(mapHomeFragment.isFirstInizialization()){
                //Log.i("firstTime:","selected: "+parkingSelected+ " size: "+adapter.getParkingSlotList().size());
                if(parkingSelected>=0 && parkingSelected<adapter.getParkingSlotList().size()){
                    Log.i("firstTime", " become false");
                    mapHomeFragment.setFirstInizialization(false);
                    markNewSelection(mapHomeFragment,parkingSelected,adapter);
                }

            }

        }
    }

    private static void markNewSelection(MapHomeFragment mapHomeFragment, int indParking,ParkingSlotAdapter adapter){
        ParkingSlot parkOldConf=adapter.getParkingSlotList().get(indParking).getParkingSlot();
        Marker marker=mapHomeFragment.checkAlreadyPresent(mapHomeFragment.getMarkerList().iterator(),parkOldConf);
        if(marker!=null){
            mapHomeFragment.onMarkerClick(marker);
            marker.showInfoWindow();
            mapHomeFragment.focusMap(marker.getPosition());
        }
    }




}
