package com.risata.parkshare.pattern_observer.messages;

import android.os.Bundle;

import com.risata.parkshare.model.Filter;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.model.Size;
import com.risata.parkshare.presenters.MainPresenter;
import com.risata.parkshare.viewmodels.MemoryStorage;

public class SaveInstanceStateMessage implements MyMessage {

    private MemoryStorage memoryStorage;

    public SaveInstanceStateMessage(MemoryStorage m) {
        memoryStorage=m;
    }

    @Override
    public void perform(MainPresenter mainPresenter) {
        Position searchPos = mainPresenter.getModel().getSearchLocation();
        memoryStorage.setLastLatitudidePosition(searchPos.getLatitude());
        memoryStorage.setLastLongitudePosition(searchPos.getLongitude());
        Filter filter=mainPresenter.getModel().getFilter();
        memoryStorage.setFree(filter.isFree());
        memoryStorage.setOnlyRestricted(filter.isOnlyRestricted());
        memoryStorage.setNoRestricted(filter.isNoRestricted());
        Size size=filter.getSize();
        String size_dim;
        switch (size){
            case SMALL:
                size_dim="SMALL";
                break;
            case PERFECT:
                size_dim="PERFECT";
                break;
            case LARGE:
                size_dim="LARGE";
                break;
            default:
                size_dim="NOT_DEFINED";
                break;
        }

        memoryStorage.setSize(size_dim);
    }
}
