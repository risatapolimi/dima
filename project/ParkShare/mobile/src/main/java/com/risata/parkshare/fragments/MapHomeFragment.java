package com.risata.parkshare.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.risata.parkshare.R;
import com.risata.parkshare.adapters.ParkingSlotAdapter;
import com.risata.parkshare.handlers.WrapContentLinearLayoutManager;
import com.risata.parkshare.model.ParkingSlot;
import com.risata.parkshare.model.ParkingSlotItem;
import com.risata.parkshare.model.Position;
import com.risata.parkshare.model.Ranking;
import com.risata.parkshare.model.Filter;
import com.risata.parkshare.model.Size;
import com.risata.parkshare.pattern_observer.ObservableFragment;
import com.risata.parkshare.pattern_observer.messages.FilterFreeMessage;
import com.risata.parkshare.pattern_observer.messages.FilterNoRestrictedMessage;
import com.risata.parkshare.pattern_observer.messages.FilterOnlyRestrictedMessage;
import com.risata.parkshare.pattern_observer.messages.FilterResumeMessage;
import com.risata.parkshare.pattern_observer.messages.FilterSizeMessage;
import com.risata.parkshare.pattern_observer.messages.GetDirectionsMessage;
import com.risata.parkshare.pattern_observer.messages.InsertCardView;
import com.risata.parkshare.pattern_observer.messages.OnSearchLocationMessage;
import com.risata.parkshare.pattern_observer.messages.ReturnOnMyPositionMessage;
import com.risata.parkshare.pattern_observer.messages.SaveInstanceStateMessage;
import com.risata.parkshare.pattern_observer.messages.UpdateParking;
import com.risata.parkshare.pattern_observer.messages.MessageCheckLocationPermission;
import com.risata.parkshare.pattern_observer.messages.MessageSetUpDefaultPosition;
import com.risata.parkshare.pattern_observer.messages.MyMessage;
import com.risata.parkshare.pattern_observer.messages.ReadyMapHomeFragment;
import com.risata.parkshare.extendedGraphics.RecyclerViewEmptySupport;
import com.risata.parkshare.services.ServiceConstants;
import com.risata.parkshare.viewmodels.MemoryStorage;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import vn.n2m.dynamic_seekbar.DynamicSeekBarView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapHomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapHomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapHomeFragment extends ObservableFragment<MyMessage> implements GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener, OnMapReadyCallback ,GoogleMap.OnMarkerClickListener,  PopupMenu.OnMenuItemClickListener,
        GoogleMap.OnCameraMoveStartedListener{


    public static final int SignalPark = 124;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private GoogleMap mMap;
    private ImageButton buttonVoice;
    private ImageButton buttonMenu;
    private SupportPlaceAutocompleteFragment autocompleteFragment;
    private SupportMapFragment mapFragment;
    private static final int SPEECH_REQUEST_CODE = 0;
    private DrawerLayout mDrawerLayout;
    protected GeoDataClient mGeoDataClient;
    protected PlaceDetectionClient mPlaceDetectionClient;
    private Marker roadToMarker;
    private List<Marker> markerList;
    private Marker lastClickedMarker;
    private Marker searchMarker;


    private TextView carLabelTV;
    private Button userRankingB;
    private FloatingActionButton signalFAB;
    private FloatingActionButton getDirectionsFAB;
    private FloatingActionButton myLocationFAB;
    private ImageView filterIcon;
    private PopupMenu popup;
    private CheckBox hidePayantCB;
    private CheckBox handyOnlyCB;
    private CheckBox handyHideCB;
    private DynamicSeekBarView sizeSeek;
    private int contatoreIngresso = 0;


    private View v;

    //For testing animation
    private Button bigButton;
    private View animatedView;
    private boolean isUp;

    private View statusLayout;
    private RecyclerViewEmptySupport recyclerView;
    private int parkingSelected;
    private boolean isSearching;
    private boolean isLookingAround;


    private OnFragmentInteractionListener mListener;
    private ParkingSlotAdapter parkingSlotAdapter;
    private Circle circle;

    private boolean isLandscape;
    private Bitmap bitmap,bitmapHandy;
    private boolean isFirstTime;
    private boolean isFirstInizialization;


    public MapHomeFragment() {
        // Required empty public constructor
    }

    public static MapHomeFragment newInstance() {
        MapHomeFragment fragment = new MapHomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.isSearching = false;
        this.isFirstTime=true;
        this.isFirstInizialization=true;
        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.ic_parking_marker);
        bitmap = this.getBitmapFromVectorDrawable((VectorDrawable) drawable);
        Drawable drawableHandy = res.getDrawable(R.drawable.ic_parking_marker_handy);
        bitmapHandy = this.getBitmapFromVectorDrawable((VectorDrawable) drawableHandy);

        contatoreIngresso = 0;
        final View rootView = getActivity().getWindow().getDecorView().getRootView();
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener(){

                    @Override
                    public void onGlobalLayout() {
                        if(isAdded()){
                            if(contatoreIngresso>100){
                                rootView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                            else {
                                notifyObserver(new ReadyMapHomeFragment());
                                isFirstTime = false;
                                contatoreIngresso++;
                            }
                        }

                        //by now all views will be displayed with correct values
                    }
                });

        this.markerList = new ArrayList<>();
        this.isLookingAround=false;
        // Inflate the layout for this fragment
        if (v == null) {

            this.v = inflater.inflate(R.layout.fragment_map_home, container, false);

            this.buttonVoice = (ImageButton) v.findViewById(R.id.button_voice);
            buttonVoice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    // Start the activity, the intent will be populated with the speech text
                    startActivityForResult(intent, SPEECH_REQUEST_CODE);
                }
            });

            this.buttonMenu = (ImageButton) v.findViewById(R.id.button_menu);
            buttonMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.openDrawer();

                }
            });
            this.carLabelTV = v.findViewById(R.id.carLabel);
            this.userRankingB = v.findViewById(R.id.userLevelButton);
            this.signalFAB = v.findViewById(R.id.fab_add_parking);
            signalFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signalParking();
                }
            });
            this.getDirectionsFAB = v.findViewById(R.id.fab_get_dir);
            getDirectionsFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.navigation);
                    // Add the buttons
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            notifyObserver(new GetDirectionsMessage(roadToMarker,isSearching));
                            getDirectionsFAB.setVisibility(View.GONE);
                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });

            this.myLocationFAB = v.findViewById(R.id.fab_my_location);

            filterIcon = v.findViewById(R.id.filter_icon);
            popup = new PopupMenu(this.getContext(), filterIcon, Gravity.BOTTOM);

            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE && getResources().getConfiguration().isLayoutSizeAtLeast(Configuration.SCREENLAYOUT_SIZE_LARGE)) {
                hidePayantCB = v.findViewById(R.id.hide_payant);
                handyOnlyCB = v.findViewById(R.id.handy_checkBox);
                handyHideCB = v.findViewById(R.id.handy_hyde);
                sizeSeek = v.findViewById(R.id.size_seek);

                hidePayantCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        notifyObserver(new FilterFreeMessage(isChecked));
                    }
                });

                handyOnlyCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked) {
                            handyHideCB.setChecked(false);
                        }
                        notifyObserver(new FilterOnlyRestrictedMessage(isChecked));
                    }
                });

                handyHideCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked) {
                            handyOnlyCB.setChecked(false);
                        }
                        notifyObserver(new FilterNoRestrictedMessage(isChecked));

                    }
                });

                sizeSeek.setSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            switch (progress) {
                                case 0:
                                    notifyObserver(new FilterSizeMessage(Size.SMALL));
                                    sizeSeek.setInfoText(getString(R.string.filter_seek_small),0);
                                    break;
                                case 1:
                                    notifyObserver(new FilterSizeMessage(Size.PERFECT));
                                    sizeSeek.setInfoText(getString(R.string.filter_seek_perfect),1);
                                    break;
                                case 2:
                                    notifyObserver(new FilterSizeMessage(Size.LARGE));
                                    sizeSeek.setInfoText(getString(R.string.filter_seek_large),2);
                                    break;
                            }

                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
            }
            else{
                filterIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v4) {
                        popup.show();

                    }
                });
            }

            popup.setOnMenuItemClickListener(this);
            popup.inflate(R.menu.menu_filter);

            //FOR TESTING ANIMATION
            bigButton = (Button) v.findViewById(R.id.bigButton);
            bigButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBigButtonClick(view);
                }
            });
            animatedView = v.findViewById(R.id.slidingLayout); //prima era la TestView
            //animatedView.setVisibility(View.INVISIBLE);

            statusLayout = v.findViewById(R.id.statusLayout);
            isUp = false;
            statusLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBigButtonClick(bigButton);
                }
            });

            parkingSlotAdapter = new ParkingSlotAdapter(this.getContext(), new ArrayList(), this.parkingSelected, this);
        }
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        onViewStateRestored(savedInstanceState);
        FragmentManager fm = getChildFragmentManager();

        //Autocomplete fragment
        this.autocompleteFragment = (SupportPlaceAutocompleteFragment) fm.findFragmentByTag("placeAutocompleteFragment");
        if (autocompleteFragment == null) {
            autocompleteFragment = new SupportPlaceAutocompleteFragment();
            fm.beginTransaction().add(R.id.place_autocomplete_frame, autocompleteFragment, "placeAutocompleteFragment").commit();
            fm.executePendingTransactions();
        }

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                onSearchLocation(place);
                view.findViewById(R.id.place_autocomplete_clear_button)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // example : way to access view from PlaceAutoCompleteFragment
                                // ((EditText) autocompleteFragment.getView()
                                // .findViewById(R.id.place_autocomplete_search_input)).setText("");
                                autocompleteFragment.setText("");
                                view.setVisibility(View.GONE);
                                getDirectionsFAB.setVisibility(View.GONE);
                                if(searchMarker != null) {
                                    searchMarker.remove();
                                    searchMarker = null;
                                }

                            }
                        });
                getDirectionsFAB.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(Status status) {
                autocompleteFragment.setText("Error...");
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        //this.mapFragment = (SupportMapFragment) fm.findFragmentByTag("mapFragment");
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
        }
        fm.executePendingTransactions();
        if (!mapFragment.isAdded()) {
            fm.beginTransaction().add(R.id.map_frame, mapFragment, "mapFragment").commit();
        }
        fm.executePendingTransactions();

        //this.notifyObserver(new ReadyMapHomeFragment());
        this.setupRecyclerView();
        //Inner Fragment
        //this.statusFragment.getCarSelected().setText("Test riuscito");
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onResume() {
        ServiceConstants.FINISH_SERVICE=true;
        super.onResume();
    }

    public void signalParking() {
        SignalParkDialogFragment signalDialogFragment = new SignalParkDialogFragment();
        signalDialogFragment.setTargetFragment(MapHomeFragment.this, SignalPark);
        signalDialogFragment.show(getFragmentManager(), "signalParkFrg");
    }

    private void onSearchLocation(Place place) {
        if (searchMarker != null)
            searchMarker.remove();
        autocompleteFragment.setText(place.getAddress());
        searchMarker = mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()));
        focusMap(place.getLatLng());
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(),16),5000,null);
        LatLng latLng = place.getLatLng();
        Location l=new Location("");
        l.setLatitude(latLng.latitude);
        l.setLongitude(latLng.longitude);
        moveToLocation(l);
    }

    private void moveToLocation(Location l){
        this.notifyObserver(new OnSearchLocationMessage(l));
        roadToMarker = searchMarker;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void focusMap(LatLng location) {
        if(mMap!=null){
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 16), 30, null);
            //isLookingAround=false;
            Log.i("ZOOM","sto zommando");
        }
    }


    @Override
    public void onMyLocationClick(@NonNull Location location) {
        if (isSearching) {
            this.notifyObserver(new ReturnOnMyPositionMessage());
            isSearching = false;
            if(searchMarker !=null) {
                searchMarker.remove();
                searchMarker = null;
            }
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {


        isLookingAround=false;
        if(!isUp)
            getDirectionsFAB.setVisibility(View.GONE);
        if (isSearching) {
            isSearching = false;
            this.notifyObserver(new ReturnOnMyPositionMessage());
            if(searchMarker !=null) {
                searchMarker.remove();
                searchMarker = null;
            }
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Permission was granted
                    if (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        notifyObserver(new MessageCheckLocationPermission());
                        //torna true e applica il metodo
                    }
                } else { //Permission closed or refused
                    notifyObserver(new MessageSetUpDefaultPosition());
                }
                return;
            }

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if(searchMarker==null)
                    getDirectionsFAB.setVisibility(View.GONE);
                if (isUp)
                    onBigButtonClick(bigButton);
            }
        });

        this.mMap.setOnMarkerClickListener(this);
        this.mMap.setOnCameraMoveStartedListener(this);
        notifyObserver(new MessageCheckLocationPermission());
        //notifyObserver(new UpdateParking());
    }

    @SuppressLint("MissingPermission") //Check already done in previous method
    public void setUpMapReady(Position userLocation) {
        mMap.setMyLocationEnabled(true);


        int uno = 1;
        int due = 2;
        View locationButton = ((View) mapFragment.getView().findViewById(uno).getParent()).findViewById(due);
        locationButton.setVisibility(View.GONE);

        myLocationFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationButton.performClick();
            }
        });

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);

        MemoryStorage memoryStorage = ViewModelProviders.of(getActivity()).get(MemoryStorage.class);
        isSearching=memoryStorage.isInSearchingMode();
        if(isSearching){
            Location loc=new Location("");
            loc.setLongitude(memoryStorage.getLastLongitudePosition());
            loc.setLatitude(memoryStorage.getLastLatitudidePosition());
            searchMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(loc.getLatitude(),loc.getLongitude())));
            this.moveToLocation(loc);
            userLocation=new Position(loc);
        }


        focusMap(new LatLng(userLocation.getLatitude(), userLocation.getLongitude()));
    }



    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.i("ZOOM","STOP");
        isLookingAround=true;
        if(marker!=null) {
            ParkingSlotItem p = (ParkingSlotItem) marker.getTag();
            if (p != null) {
                if (!isUp) {
                    this.onBigButtonClick(bigButton);
                }
                int oldparkingselected=this.parkingSelected;
                this.parkingSelected = p.getIndex();
                if (parkingSelected >= 0 && parkingSelected < parkingSlotAdapter.getParkingSlotList().size()) {
                    if (lastClickedMarker == null) {
                        if(oldparkingselected>=0 && oldparkingselected<parkingSlotAdapter.getParkingSlotList().size())
                            recyclerView.getAdapter().notifyItemChanged(oldparkingselected, "ResetSelection");
                        recyclerView.getAdapter().notifyItemChanged(0, "ResetSelection");
                        recyclerView.getAdapter().notifyItemChanged(parkingSelected, "ChangeSelection");
                        recyclerView.smoothScrollToPosition(this.parkingSelected);
                        ParkingSlotAdapter.positionToHack = parkingSelected;
                    } else {
                        ParkingSlotItem oldParking = (ParkingSlotItem) this.lastClickedMarker.getTag();
                        recyclerView.smoothScrollToPosition(this.parkingSelected);
                        ParkingSlotAdapter.positionToHack = parkingSelected;
                        if (oldParking != null) {
                            if (parkingSelected != oldParking.getIndex()) {
                                recyclerView.getAdapter().notifyItemChanged(oldParking.getIndex(), "ResetSelection");
                            }
                        }
                        recyclerView.getAdapter().notifyItemChanged(this.parkingSelected, "ChangeSelection");
                    }
                }
                this.lastClickedMarker = marker;
            }
            this.roadToMarker = marker;
            this.getDirectionsFAB.setVisibility(View.VISIBLE);
        }
        return false;
    }


    public boolean isLookingAround() {
        return isLookingAround;
    }


    @Override
    public void onCameraMoveStarted(int i) {
       if(i==1){
            isLookingAround=true;
            Log.i("ZOOM","STOP");
        }
        
    }


    public interface OnFragmentInteractionListener {
        void openDrawer();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
            //Cercare
            this.autocompleteFragment.setText(spokenText);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void setCarSelected(String carLabel) {

        this.carLabelTV.setText(carLabel);

    }

    public void setUserLevel(String userLevel) {
        this.userRankingB.setText(userLevel);
    }

    private void saveState(){
        MemoryStorage memoryStorage = ViewModelProviders.of(getActivity()).get(MemoryStorage.class);
        memoryStorage.setInSearchingMode(isSearching);
        memoryStorage.setParkingSelected(parkingSelected);
        notifyObserver(new SaveInstanceStateMessage(memoryStorage));
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        saveState();
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onPause() {
        saveState();
        super.onPause();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    public void markParking(ParkingSlot ps, boolean visible, ParkingSlotItem p) {
        Marker markerAlreadyPresent = this.checkAlreadyPresent(markerList.iterator(), ps);
        if (markerAlreadyPresent == null) {
                Marker marker = this.mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(ps.getPosition().getLatitude(), ps.getPosition().getLongitude()))
                        .title("Parking Free")
                        .icon(BitmapDescriptorFactory.fromBitmap(ps.isRestricted() ? bitmapHandy : bitmap)) //cambia icona se per disabili
                        .visible(visible));
            marker.setTag(p);
            markerList.add(marker);
            markerAlreadyPresent = marker;
            markerAlreadyPresent.setTitle(p.getPrintableDistance());
        } else {
            markerAlreadyPresent.setVisible(visible);
            markerAlreadyPresent.setTag(p);
            markerAlreadyPresent.setTitle(p.getPrintableDistance());
        }
    }

    public void removeMarkerParking(List<ParkingSlot> listToDelete) {

        for (Iterator<Marker> iterator = markerList.iterator(); iterator.hasNext();) {
            Marker marker = iterator.next();
            ParkingSlotItem p = (ParkingSlotItem) marker.getTag();
            ParkingSlot parking = p.getParkingSlot();
            if (listToDelete.contains(parking)) {
                marker.remove();
                iterator.remove();
            }
        }
    }

    public void drawRankingCircle(Location userLocation, Ranking userRanking) {
        LatLng pos = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
        if (circle != null) {
            circle.setVisible(false);
            circle.remove();
        }
        circle = this.mMap.addCircle(new CircleOptions()
                .center(pos)
                .radius(userRanking.getRadiusMeters())
                .strokeColor(Color.YELLOW) //circonferenza
                .fillColor(Color.argb(50, 246, 252, 48)));
    }

    public Marker checkAlreadyPresent(Iterator<Marker> iter, ParkingSlot p) {
        if (iter.hasNext()) {
            Marker m = iter.next();
            if (p.hasSameLocation(m.getPosition()))
                return m;
            else return this.checkAlreadyPresent(iter, p);
        } else return null;
    }


    private Bitmap getBitmapFromVectorDrawable(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }



    public void moveUp(View view) {
       /* FrameLayout.LayoutParams params=(FrameLayout.LayoutParams)view.getLayoutParams();
        params.bottomMargin=0;
        view.setLayoutParams(params);*/
        recyclerView.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                recyclerView.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }


    public void moveDown(View view) {

        recyclerView.setVisibility(View.GONE);

    }


    public void onBigButtonClick(View view) {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) { //blocca lo slide se siamo in orizzontale
            return;
        }
        if (isUp) {
            //slideDown(animatedView);
            moveDown(animatedView); //era statusLayout

            //moveDown(signalFAB);
            //moveDown(getDirectionsFAB);
            //setMove(animatedView,0);
            //slideDown(animatedView);
            //slideDown(signalFAB);
            //slideDown(getDirectionsFAB);
            bigButton.setText("TU MI PORTI SUUUUU");
        } else {
            //moveUp(signalFAB);
            //moveUp(getDirectionsFAB);
            moveUp(animatedView);
            //slideUp(animatedView);
            //setMove(statusLayout,recyclerView.getHeight());
            //slideUp(statusLayout);
            //slideUp(signalFAB);
            //slideUp(getDirectionsFAB);
            bigButton.setText("E POI MI LASCI CADEREE");
        }
        isUp = !isUp;
    }

    public void setupRecyclerView() {
        this.recyclerView = (RecyclerViewEmptySupport) this.v.findViewById(R.id.recycler_view);
        int orientation = getResources().getConfiguration().orientation;
        isLandscape = orientation == Configuration.ORIENTATION_LANDSCAPE;

        /*
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), isLandscape ? LinearLayoutManager.VERTICAL : LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);*/

        WrapContentLinearLayoutManager layoutManager = new WrapContentLinearLayoutManager(getContext(), isLandscape ? LinearLayoutManager.VERTICAL : LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        MemoryStorage memoryStorage = ViewModelProviders.of(getActivity()).get(MemoryStorage.class);
        this.parkingSelected = memoryStorage.getParkingSelected();
        recyclerView.setEmptyView(v.findViewById(R.id.emptyview_parking));
        recyclerView.setAdapter(parkingSlotAdapter);

    }

    public List<Marker> getMarkerList() {
        return markerList;
    }

    public ParkingSlotAdapter getParkingSlotAdapter() {
        return parkingSlotAdapter;
    }

    public GoogleMap getmMap() {
        return mMap;
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter_payant_item:
                item.setChecked(!item.isChecked());
                notifyObserver(new FilterFreeMessage(item.isChecked()));
                return true;
            case R.id.filter_handy_item:
                item.setChecked(!item.isChecked());
                popup.getMenu().findItem(R.id.filter_no_handy_item).setChecked(false);
                notifyObserver(new FilterOnlyRestrictedMessage(item.isChecked()));
                return true;
            case R.id.filter_no_handy_item:
                item.setChecked(!item.isChecked());
                popup.getMenu().findItem(R.id.filter_handy_item).setChecked(false);
                notifyObserver(new FilterNoRestrictedMessage(item.isChecked()));
                return true;
           /* case R.id.filter_size_small:
                item.setChecked(true);
                notifyObserver(new FilterSizeMessage(Size.SMALL));
                popup.getMenu().findItem(R.id.filter_size_normal).setChecked(false);
                popup.getMenu().findItem(R.id.filter_size_large).setChecked(false);
                return true;*/
            case R.id.filter_size_normal:
                /*item.setChecked(true);
                notifyObserver(new FilterSizeMessage(Size.PERFECT));
                popup.getMenu().findItem(R.id.filter_size_small).setChecked(false);
                popup.getMenu().findItem(R.id.filter_size_large).setChecked(false);
                return true;
                */
                item.setChecked(!item.isChecked());
                if(item.isChecked()){
                    notifyObserver(new FilterSizeMessage(Size.PERFECT));
                    popup.getMenu().findItem(R.id.filter_size_large).setChecked(false);
                }
                else if(!popup.getMenu().findItem(R.id.filter_size_large).isChecked()){
                    notifyObserver(new FilterSizeMessage(Size.SMALL));
                }
                return true;

            case R.id.filter_size_large:
                /*item.setChecked(true);
                notifyObserver(new FilterSizeMessage(Size.LARGE));
                popup.getMenu().findItem(R.id.filter_size_normal).setChecked(false);
                popup.getMenu().findItem(R.id.filter_size_small).setChecked(false);
                return true;*/
                item.setChecked(!item.isChecked());
                if(item.isChecked()){
                    notifyObserver(new FilterSizeMessage(Size.LARGE));
                    popup.getMenu().findItem(R.id.filter_size_normal).setChecked(false);
                }
                else if(!popup.getMenu().findItem(R.id.filter_size_normal).isChecked()){
                    notifyObserver(new FilterSizeMessage(Size.SMALL));
                }
                return true;
            default:
                return false;
        }
    }

    public PopupMenu getPopup() {
        return popup;
    }

    public CheckBox getHidePayantCB() {
        return hidePayantCB;
    }

    public CheckBox getHandyOnlyCB() {
        return handyOnlyCB;
    }

    public CheckBox getHandyHideCB() {
        return handyHideCB;
    }

    public DynamicSeekBarView getSizeSeek() {
        return sizeSeek;
    }

    public int getParkingSelected() {
        return parkingSelected;
    }

    public void setParkingSelected(int parkingSelected) {
        this.parkingSelected = parkingSelected;
    }

    public RecyclerViewEmptySupport getRecyclerView() {
        return recyclerView;
    }

    public boolean isFirstInizialization() {
        return isFirstInizialization;
    }

    public void setFirstInizialization(boolean firstInizialization) {
        isFirstInizialization = firstInizialization;
    }

    public void setLastClickedMarker(Marker lastClickedMarker) {
        this.lastClickedMarker = lastClickedMarker;
    }

    public boolean isSearching() {
        return isSearching;
    }

    public void setSearching(boolean searching) {
        isSearching = searching;
    }

    public void setLookingAround(boolean lookingAround) {
        isLookingAround = lookingAround;
    }

    public boolean isFirstTime() {
        return isFirstTime;
    }
}