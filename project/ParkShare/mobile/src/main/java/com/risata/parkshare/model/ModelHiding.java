package com.risata.parkshare.model;

import java.util.List;

public interface ModelHiding {
    Position getUserLocation();
    List<ParkingSlot> getParkingSlotList();
    User getUser();
}
