package com.risata.parkshare.pattern_observer.messages;

import com.risata.parkshare.presenters.MainPresenter;

/**
 * Created by Marco Sartini on 23/08/2018.
 */

public class SettingsFilterMessage implements MyMessage {
    @Override
    public void perform(MainPresenter mainPresenter) {
        if(mainPresenter.getMapHomePresenter().getMapHomeFragment().isAdded())
            mainPresenter.getMapHomePresenter().manageFilter(mainPresenter.getModel().getFilter());
    }
}
