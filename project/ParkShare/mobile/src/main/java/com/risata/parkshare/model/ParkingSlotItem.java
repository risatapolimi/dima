package com.risata.parkshare.model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Marco Sartini on 26/05/2018.
 */

public class ParkingSlotItem {
    private ParkingSlot parkingSlot;
    private int index;
    private Double distance;
    private boolean toBeColored;
    private Size advicedSize;
    private boolean isVisible;

    public ParkingSlotItem(ParkingSlot parkingSlot){
        this.parkingSlot = parkingSlot;
        this.index = -1;
        this.distance = new Double(Double.POSITIVE_INFINITY);
        this.toBeColored = false;
        this.advicedSize = Size.NOT_DEFINED;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Double getDistance() {
        return distance;
    }

    public boolean isToBeColored() {
        return toBeColored;
    }

    public void setToBeColored(boolean toBeColored) {
        this.toBeColored = toBeColored;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getPrintableDistance() {
        if(distance < 1){
            return Integer.toString((int)(distance * 1000)) + " m";
        }
        else {
            return new DecimalFormat("#.#").format(distance) + " km";
        }
    }

    public ParkingSlot getParkingSlot() {
        return parkingSlot;
    }

    public Size getAdvicedSize() {
        return advicedSize;
    }

    public void setAdvicedSize(Size advicedSize) {
        this.advicedSize = advicedSize;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public static List<ParkingSlotItem> convertList(List<ParkingSlot> parkingSlotList) {

        List<ParkingSlotItem> newList = new ArrayList<>();
        for (ParkingSlot p: parkingSlotList){
            newList.add(new ParkingSlotItem(p));
        }
        return newList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingSlotItem that = (ParkingSlotItem) o;
        return Objects.equals(parkingSlot, that.parkingSlot);
    }


}
